<?php

include '_exportSQL.php';

 $server = '135.239.16.60:1445';

// Next line is commented because the script was switched to centralized mssql authentication
				//$conn = mssql_connect($server, 'dsava', 'sq1p3wds');
//  mssql centralized authentication
				$app_name="ean_dashboard";			  
				include '/srv/data203386/sftp/jail/ftp/gnoc/mssql_auth/mssql_config.php';
				$conn  = $connection_string;

$view = $_GET['view'];
$ym = $_GET['date'];
$region = $_GET['region'];

require_once "PHPExcel/Classes/PHPExcel/IOFactory.php";



$objTpl = PHPExcel_IOFactory::load("market_template.xlsm");
$objTpl->setActiveSheetIndex(0);

if($view == 'Executive Dashboard')
{
	$sql=" 
SET ANSI_NULLS ON 
SET QUOTED_IDENTIFIER ON
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET ANSI_PADDING ON

DECLARE @calculateValues nvarchar(4000)
DECLARE @convertValues nvarchar(4000)
DECLARE @showExecutive nvarchar(4000)
DECLARE @rawValues nvarchar(4000)

SET @calculateValues= 
		'
			SELECT * into ##tempGov FROM(
				SELECT
				CASE
							WHEN [BUSINESS_LINE] LIKE ''%multi%'' THEN ''MULTI BL''
							ELSE BUSINESS_LINE
						END AS [BUSINESS_LINE] 
				,case when PMO not like  ''Y'' then PTA_BASED_ACTUAL_DOCUMENT else PTA_BASED_ON_BC end PTA_SIGNED
				, '''' PROJ_32
				, [COST_BASELINE]
				,cast([COST_BASELINE] as nvarchar)+ ''/'' + cast([REVISED_CBL] as nvarchar) COST_1
				,(CAST( ROUND(([TOTAL_COST] /[PLANED_COST] * 100), 2) as nvarchar)) as COST_2
				,(CAST( ROUND(([REVISED_CBL] / [ESTIMATED_COST] * 100), 2) as nvarchar)) as COST_3
				, [PROJECT_START_DATE] as TIME_1
				, [PROJECT_END_DATE] as TIME_2
				, (CAST(([PROJECT_PLANED_END_DATE] / [PROJECT_ESTIMATED_COMPLETITION_DATE] * 100) as nvarchar)) as TIME_3
				, CASE WHEN [BUSINESS_LINE] LIKE ''MS'' THEN ''100'' ELSE CASE WHEN [BUSINESS_LINE] LIKE ''SI'' THEN '''' ELSE '''' END END as TIME_4	
				, [NR_CHANGES_REQUEST_APPROVED] SCOPE_1
				, [COMPETENCE_ADHERENCE] SCOPE_2
				, (CAST(([TOTAL_WORK_PACKAGES_HAS_TO_WORK] / [TOTAL_WORK_PACKAGES_ACTUAL_WORKED] * 100)as nvarchar)) SCOPE_3
				, [PMO] as QUALITY_1
				, [SOLUTION_DOCUMENT] as QUALITY_2
				, [VIRTUAL_ZERO] as QUALITY_3
				, [CUSTOMER_TEAM_SURVEY] QUALITY_4
				, ''-'' OVR_STS
				FROM ReportingDBProd.[gsd].[RAW_DATA] 
				WHERE BUSINESS_LINE IS NOT NULL
				AND YEAR_MONTH LIKE ''$ym''
			)f'

SET @convertValues = '
			select 
				 BUSINESS_LINE
				,PTA_SIGNED
				,PROJ_32
				,[COST_BASELINE]
				,CASE WHEN COST_1 IS NOT NULL THEN 0 ELSE 2 END COST_1
				,CASE WHEN CAST(COST_2 as float) >= 90 AND CAST(COST_2 as float) <= 110 
						THEN 0 
				  ELSE 
						CASE WHEN (CAST(COST_2 as float) >= 80 AND CAST(COST_2 as float) < 90) OR (CAST(COST_2 as float) >= 111 AND CAST(COST_2 as float) <= 120)
						THEN 1
						ELSE
							2
						END
				  END COST_2
				,CASE WHEN CAST(COST_3 as float) > 90 AND CAST(COST_3 as float) < 110 
					THEN 0
				ELSE
					CASE WHEN (CAST(COST_3 as float) >= 80 AND CAST(COST_3 as float) <= 89) OR (CAST(COST_3 as float) >= 111 AND CAST(COST_3 as float) <= 120)
					THEN 1
					ELSE 
						2
					END
				END COST_3
			   ,CASE WHEN TIME_1 IS NOT NULL THEN 0 ELSE 2 END TIME_1
			   ,CASE WHEN TIME_2 IS NOT NULL THEN 0 ELSE 2 END TIME_2
			   ,CASE WHEN CAST(TIME_3 as float) > 90 AND CAST(TIME_3 as float) < 110 
					THEN 0
				ELSE
					CASE WHEN (CAST(TIME_3 as float) >= 80 AND CAST(TIME_3 as float) <= 89) OR (CAST(TIME_3 as float) >= 111 AND CAST(TIME_3 as float) <= 120)
					THEN 1
					ELSE 
						2
					END
				END TIME_3
				,CASE WHEN CAST(TIME_4 as float) > 90 AND CAST(TIME_4 as float) < 110 
					THEN 0
				ELSE
					CASE WHEN (CAST(TIME_4 as float) >= 80 AND CAST(TIME_4 as float) <= 89) OR (CAST(TIME_4 as float) >= 111 AND CAST(TIME_4 as float) <= 120)
					THEN 1
					ELSE 
						2
					END
				END TIME_4
				,CASE WHEN SCOPE_1 = 1 THEN 0 ELSE 2 END SCOPE_1
				,CASE WHEN SCOPE_2 IS NOT NULL THEN 0 ELSE 2 END SCOPE_2
				,CASE WHEN CAST(SCOPE_3 as float) > 90 AND CAST(SCOPE_3 as float) < 110 
					THEN 0
				ELSE
					CASE WHEN CAST(SCOPE_3 as float) > 80 AND CAST(SCOPE_3 as float) < 89
					THEN 1
					ELSE 2
					END
				END SCOPE_3
				,CASE WHEN QUALITY_1 IS NOT NULL THEN 0 ELSE 2 END QUALITY_1
				,CASE WHEN QUALITY_2 IS NOT NULL THEN 0 ELSE 2 END QUALITY_2
				,CASE WHEN CAST(QUALITY_3 as float) > 93 
					THEN 0
				ELSE
					CASE WHEN (CAST(QUALITY_3 as float) >= 83.7 AND CAST(QUALITY_3 as float) <= 93) 
					THEN 1
					ELSE 
						2
					END
				END QUALITY_3
				,CASE WHEN CAST(QUALITY_4 as float) > 8.2 
					THEN 0
				ELSE
					CASE WHEN (CAST(QUALITY_4 as float) >= 7.38 AND CAST(QUALITY_4 as float) <= 8.2) 
					THEN 1
					ELSE 
						2
					END
				END QUALITY_4
			into ##tempGovRaw
			from ##tempGov	
		'

SET @rawValues = '
	SELECT 
		 BUSINESS_LINE
		,PTA_SIGNED
		,PROJ_32
		, case when CAST(COST_BASELINE AS FLOAT) < 100 then ''100'' else 
			case when CAST(COST_BASELINE AS FLOAT) <= 500 AND CAST(COST_BASELINE AS FLOAT) > 100 then ''1_3'' else
				case when CAST(COST_BASELINE AS FLOAT) <= 1000 AND CAST(COST_BASELINE AS FLOAT) > 500 then ''3_5'' else 
					case when CAST(COST_BASELINE AS FLOAT) > 1000 then ''500'' else ''Wrong'' END
				END
			END
		END GDC_COST
		, CASE WHEN COST_1 = 0 AND COST_2 = 0 AND COST_3 = 0 THEN ''GREEN'' 
			ELSE CASE WHEN COST_1 = 1 OR COST_2 = 0 OR COST_3 = 1 THEN ''AMBER''
			ELSE ''RED''
			END
		END COST_COLOR
		, CASE WHEN TIME_1 = 2 OR TIME_2 = 2 OR TIME_3 = 2 OR TIME_4 = 2 THEN ''RED'' 
			ELSE 
				CASE WHEN TIME_1 = 1 OR TIME_2 = 1 OR TIME_3 = 1 OR TIME_4 = 1 THEN ''AMBER''
				ELSE ''GREEN'' 
				END
			END TIME_COLOR
		, CASE WHEN SCOPE_1 = 2 OR SCOPE_2 = 2 OR SCOPE_3 = 2 THEN ''RED'' 
			ELSE 
				CASE WHEN SCOPE_1 = 1 OR SCOPE_2 = 1 OR SCOPE_3 = 1 THEN ''AMBER''
				ELSE ''GREEN'' 
				END
			END SCOPE_COLOR
		, CASE WHEN QUALITY_1 = 2 OR QUALITY_2 = 2 OR QUALITY_3 = 2 OR QUALITY_4 = 2 THEN ''RED'' 
			ELSE 
				CASE WHEN QUALITY_1 = 1 OR QUALITY_2 = 1 OR QUALITY_3 = 1 OR QUALITY_4 = 1 THEN ''AMBER''
				ELSE ''GREEN'' 
				END
			END QUALITY_COLOR
	INTO ##finalRaw
	FROM ##tempGovRaw

'
SET @showExecutive = 
		'
		SELECT 
			BUSINESS_LINE
			,COUNT(BUSINESS_LINE)PROJ_EXCL_CARE
			,SUM(CASE WHEN PTA_SIGNED LIKE ''Y'' THEN 1 ELSE 0 END)PTA_SIGNED
			,PROJ_32
			,SUM(CASE WHEN GDC_COST LIKE ''100'' THEN 1 ELSE 0 END)GDC_100
			,SUM(CASE WHEN GDC_COST LIKE ''1_3'' THEN 1 ELSE 0 END)GDC_1_3
			,SUM(CASE WHEN GDC_COST LIKE ''3_5'' THEN 1 ELSE 0 END)GDC_3_5
			,SUM(CASE WHEN GDC_COST LIKE ''500'' THEN 1 ELSE 0 END)GDC_500
			,SUM(CASE WHEN COST_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)COST_GREEN
			,SUM(CASE WHEN COST_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)COST_AMBER
			,SUM(CASE WHEN COST_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)COST_RED
			,SUM(CASE WHEN TIME_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)TIME_GREEN
			,SUM(CASE WHEN TIME_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)TIME_AMBER
			,SUM(CASE WHEN TIME_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)TIME_RED
			,SUM(CASE WHEN SCOPE_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)SCOPE_GREEN
			,SUM(CASE WHEN SCOPE_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)SCOPE_AMBER
			,SUM(CASE WHEN SCOPE_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)SCOPE_RED
			,SUM(CASE WHEN QUALITY_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)QUALITY_GREEN
			,SUM(CASE WHEN QUALITY_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)QUALITY_AMBER
			,SUM(CASE WHEN QUALITY_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)QUALITY_RED
		FROM
		##finalRaw
		GROUP BY BUSINESS_LINE, PROJ_32

		DROP TABLE ##finalRaw
		drop table ##tempGov
		drop table ##tempGovRaw

		'

EXECUTE (@calculateValues + @convertValues + @rawValues + @showExecutive) ";


$rs=mssql_query($sql);

  if (!$rs){
	exit('MSSQL error: ' . mssql_get_last_message());
  }	
  $i=7;
  while ($row=mssql_fetch_array($rs))
	{
		if($i<13)
		{
			$objTpl->getActiveSheet()->setCellValue('C'.$i,$row['PROJ_EXCL_CARE']);
			$objTpl->getActiveSheet()->setCellValue('D'.$i,$row['PTA_SIGNED']);
			$objTpl->getActiveSheet()->setCellValue('E'.$i,$row['PROJ_32']);
			$objTpl->getActiveSheet()->setCellValue('F'.$i,$row['GDC_100']);
			$objTpl->getActiveSheet()->setCellValue('G'.$i,$row['GDC_1_3']);
			$objTpl->getActiveSheet()->setCellValue('H'.$i,$row['GDC_3_5']);
			$objTpl->getActiveSheet()->setCellValue('I'.$i,$row['GDC_500']);
			$objTpl->getActiveSheet()->setCellValue('J'.$i,$row['COST_RED']);
			$objTpl->getActiveSheet()->setCellValue('K'.$i,$row['COST_AMBER']);
			$objTpl->getActiveSheet()->setCellValue('L'.$i,$row['COST_GREEN']);
			$objTpl->getActiveSheet()->setCellValue('M'.$i,$row['TIME_RED']);
			$objTpl->getActiveSheet()->setCellValue('N'.$i,$row['TIME_AMBER']);
			$objTpl->getActiveSheet()->setCellValue('O'.$i,$row['TIME_GREEN']);
			$objTpl->getActiveSheet()->setCellValue('P'.$i,$row['SCOPE_RED']);
			$objTpl->getActiveSheet()->setCellValue('Q'.$i,$row['SCOPE_AMBER']);
			$objTpl->getActiveSheet()->setCellValue('R'.$i,$row['SCOPE_GREEN']);
			$objTpl->getActiveSheet()->setCellValue('S'.$i,$row['QUALITY_RED']);
			$objTpl->getActiveSheet()->setCellValue('T'.$i,$row['QUALITY_AMBER']);
			$objTpl->getActiveSheet()->setCellValue('U'.$i,$row['QUALITY_GREEN']);
			$i++;
		}
	//$nr_supp_ev=$nr_supp_ev+1;
  }
}


if($view == 'RAW DATA')
{
	$sql=" 
SET ANSI_NULLS ON 
SET QUOTED_IDENTIFIER ON
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET ANSI_PADDING ON

DECLARE @calculateValues nvarchar(4000)
DECLARE @convertValues nvarchar(4000)
DECLARE @showExecutive nvarchar(4000)
DECLARE @rawValues nvarchar(4000)

SET @calculateValues= 
		'
			SELECT * into ##tempGov FROM(
				SELECT
				 [BUSINESS_LINE] 
				,case when PMO not like  ''Y'' then PTA_BASED_ACTUAL_DOCUMENT else PTA_BASED_ON_BC end PTA_SIGNED
				, '''' PROJ_32
				, [COST_BASELINE]
				,cast([COST_BASELINE] as nvarchar)+ ''/'' + cast([REVISED_CBL] as nvarchar) COST_1
				,(CAST( ROUND(([TOTAL_COST] /[PLANED_COST] * 100), 2) as nvarchar)) as COST_2
				,(CAST( ROUND(([REVISED_CBL] / [ESTIMATED_COST] * 100), 2) as nvarchar)) as COST_3
				, [PROJECT_START_DATE] as TIME_1
				, [PROJECT_END_DATE] as TIME_2
				, (CAST(([PROJECT_PLANED_END_DATE] / [PROJECT_ESTIMATED_COMPLETITION_DATE] * 100) as nvarchar)) as TIME_3
				, CASE WHEN [BUSINESS_LINE] LIKE ''MS'' THEN ''100'' ELSE CASE WHEN [BUSINESS_LINE] LIKE ''SI'' THEN '''' ELSE '''' END END as TIME_4	
				, [NR_CHANGES_REQUEST_APPROVED] SCOPE_1
				, [COMPETENCE_ADHERENCE] SCOPE_2
				, (CAST(([TOTAL_WORK_PACKAGES_HAS_TO_WORK] / [TOTAL_WORK_PACKAGES_ACTUAL_WORKED] * 100)as nvarchar)) SCOPE_3
				, [PMO] as QUALITY_1
				, [SOLUTION_DOCUMENT] as QUALITY_2
				, [VIRTUAL_ZERO] as QUALITY_3
				, [CUSTOMER_TEAM_SURVEY] QUALITY_4
				, ''-'' OVR_STS
				FROM ReportingDBProd.[gsd].[RAW_DATA] 
				WHERE BUSINESS_LINE IS NOT NULL
				AND YEAR_MONTH LIKE ''$ym''
			)f'

SET @convertValues = '
			select 
				 BUSINESS_LINE
				,PTA_SIGNED
				,PROJ_32
				,[COST_BASELINE]
				,CASE WHEN COST_1 IS NOT NULL THEN 0 ELSE 2 END COST_1
				,CASE WHEN CAST(COST_2 as float) >= 90 AND CAST(COST_2 as float) <= 110 
						THEN 0 
				  ELSE 
						CASE WHEN (CAST(COST_2 as float) >= 80 AND CAST(COST_2 as float) < 90) OR (CAST(COST_2 as float) >= 111 AND CAST(COST_2 as float) <= 120)
						THEN 1
						ELSE
							2
						END
				  END COST_2
				,CASE WHEN CAST(COST_3 as float) > 90 AND CAST(COST_3 as float) < 110 
					THEN 0
				ELSE
					CASE WHEN (CAST(COST_3 as float) >= 80 AND CAST(COST_3 as float) <= 89) OR (CAST(COST_3 as float) >= 111 AND CAST(COST_3 as float) <= 120)
					THEN 1
					ELSE 
						2
					END
				END COST_3
			   ,CASE WHEN TIME_1 IS NOT NULL THEN 0 ELSE 2 END TIME_1
			   ,CASE WHEN TIME_2 IS NOT NULL THEN 0 ELSE 2 END TIME_2
			   ,CASE WHEN CAST(TIME_3 as float) > 90 AND CAST(TIME_3 as float) < 110 
					THEN 0
				ELSE
					CASE WHEN (CAST(TIME_3 as float) >= 80 AND CAST(TIME_3 as float) <= 89) OR (CAST(TIME_3 as float) >= 111 AND CAST(TIME_3 as float) <= 120)
					THEN 1
					ELSE 
						2
					END
				END TIME_3
				,CASE WHEN CAST(TIME_4 as float) > 90 AND CAST(TIME_4 as float) < 110 
					THEN 0
				ELSE
					CASE WHEN (CAST(TIME_4 as float) >= 80 AND CAST(TIME_4 as float) <= 89) OR (CAST(TIME_4 as float) >= 111 AND CAST(TIME_4 as float) <= 120)
					THEN 1
					ELSE 
						2
					END
				END TIME_4
				,CASE WHEN SCOPE_1 = 1 THEN 0 ELSE 2 END SCOPE_1
				,CASE WHEN SCOPE_2 IS NOT NULL THEN 0 ELSE 2 END SCOPE_2
				,CASE WHEN CAST(SCOPE_3 as float) > 90 AND CAST(SCOPE_3 as float) < 110 
					THEN 0
				ELSE
					CASE WHEN CAST(SCOPE_3 as float) > 80 AND CAST(SCOPE_3 as float) < 89
					THEN 1
					ELSE 2
					END
				END SCOPE_3
				,CASE WHEN QUALITY_1 IS NOT NULL THEN 0 ELSE 2 END QUALITY_1
				,CASE WHEN QUALITY_2 IS NOT NULL THEN 0 ELSE 2 END QUALITY_2
				,CASE WHEN CAST(QUALITY_3 as float) > 93 
					THEN 0
				ELSE
					CASE WHEN (CAST(QUALITY_3 as float) >= 83.7 AND CAST(QUALITY_3 as float) <= 93) 
					THEN 1
					ELSE 
						2
					END
				END QUALITY_3
				,CASE WHEN CAST(QUALITY_4 as float) > 8.2 
					THEN 0
				ELSE
					CASE WHEN (CAST(QUALITY_4 as float) >= 7.38 AND CAST(QUALITY_4 as float) <= 8.2) 
					THEN 1
					ELSE 
						2
					END
				END QUALITY_4
			into ##tempGovRaw
			from ##tempGov	
		'

SET @rawValues = '
	SELECT 
		 BUSINESS_LINE
		,PTA_SIGNED
		,PROJ_32
		, case when CAST(COST_BASELINE AS FLOAT) < 100 then ''100'' else 
			case when CAST(COST_BASELINE AS FLOAT) <= 500 AND CAST(COST_BASELINE AS FLOAT) > 100 then ''1_3'' else
				case when CAST(COST_BASELINE AS FLOAT) <= 1000 AND CAST(COST_BASELINE AS FLOAT) > 500 then ''3_5'' else 
					case when CAST(COST_BASELINE AS FLOAT) > 1000 then ''500'' else ''Wrong'' END
				END
			END
		END GDC_COST
		, CASE WHEN COST_1 = 0 AND COST_2 = 0 AND COST_3 = 0 THEN ''GREEN'' 
			ELSE CASE WHEN COST_1 = 1 OR COST_2 = 0 OR COST_3 = 1 THEN ''AMBER''
			ELSE ''RED''
			END
		END COST_COLOR
		, CASE WHEN TIME_1 = 2 OR TIME_2 = 2 OR TIME_3 = 2 OR TIME_4 = 2 THEN ''RED'' 
			ELSE 
				CASE WHEN TIME_1 = 1 OR TIME_2 = 1 OR TIME_3 = 1 OR TIME_4 = 1 THEN ''AMBER''
				ELSE ''GREEN'' 
				END
			END TIME_COLOR
		, CASE WHEN SCOPE_1 = 2 OR SCOPE_2 = 2 OR SCOPE_3 = 2 THEN ''RED'' 
			ELSE 
				CASE WHEN SCOPE_1 = 1 OR SCOPE_2 = 1 OR SCOPE_3 = 1 THEN ''AMBER''
				ELSE ''GREEN'' 
				END
			END SCOPE_COLOR
		, CASE WHEN QUALITY_1 = 2 OR QUALITY_2 = 2 OR QUALITY_3 = 2 OR QUALITY_4 = 2 THEN ''RED'' 
			ELSE 
				CASE WHEN QUALITY_1 = 1 OR QUALITY_2 = 1 OR QUALITY_3 = 1 OR QUALITY_4 = 1 THEN ''AMBER''
				ELSE ''GREEN'' 
				END
			END QUALITY_COLOR
	INTO ##finalRaw
	FROM ##tempGovRaw

'
SET @showExecutive = 
		'
		SELECT 
			BUSINESS_LINE
			,COUNT(BUSINESS_LINE)PROJ_EXCL_CARE
			,SUM(CASE WHEN PTA_SIGNED LIKE ''Y'' THEN 1 ELSE 0 END)PTA_SIGNED
			,PROJ_32
			,SUM(CASE WHEN GDC_COST LIKE ''100'' THEN 1 ELSE 0 END)GDC_100
			,SUM(CASE WHEN GDC_COST LIKE ''1_3'' THEN 1 ELSE 0 END)GDC_1_3
			,SUM(CASE WHEN GDC_COST LIKE ''3_5'' THEN 1 ELSE 0 END)GDC_3_5
			,SUM(CASE WHEN GDC_COST LIKE ''500'' THEN 1 ELSE 0 END)GDC_500
			,SUM(CASE WHEN COST_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)COST_GREEN
			,SUM(CASE WHEN COST_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)COST_AMBER
			,SUM(CASE WHEN COST_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)COST_RED
			,SUM(CASE WHEN TIME_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)TIME_GREEN
			,SUM(CASE WHEN TIME_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)TIME_AMBER
			,SUM(CASE WHEN TIME_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)TIME_RED
			,SUM(CASE WHEN SCOPE_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)SCOPE_GREEN
			,SUM(CASE WHEN SCOPE_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)SCOPE_AMBER
			,SUM(CASE WHEN SCOPE_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)SCOPE_RED
			,SUM(CASE WHEN QUALITY_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)QUALITY_GREEN
			,SUM(CASE WHEN QUALITY_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)QUALITY_AMBER
			,SUM(CASE WHEN QUALITY_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)QUALITY_RED
		FROM
		##finalRaw
		GROUP BY BUSINESS_LINE, PROJ_32

		DROP TABLE ##finalRaw
		drop table ##tempGov
		drop table ##tempGovRaw

		'

EXECUTE (@calculateValues + @convertValues + @rawValues + @showExecutive) ";

//die($sql);

$rs=mssql_query($sql);

  if (!$rs){
	exit('MSSQL error: ' . mssql_get_last_message());
  }	
  $i=7;
  while ($row=mssql_fetch_array($rs))
	{
		if($i<13)
		{
			$objTpl->getActiveSheet()->setCellValue('C'.$i,$row['PROJ_EXCL']);
			$objTpl->getActiveSheet()->setCellValue('D'.$i,$row['PTA_SIGNED']);
			$objTpl->getActiveSheet()->setCellValue('E'.$i,$row['PROJ_T']);
			$objTpl->getActiveSheet()->setCellValue('J'.$i,$row['GDC_COST_1_R']);
			$objTpl->getActiveSheet()->setCellValue('K'.$i,$row['GDC_COST_1_O']);
			$objTpl->getActiveSheet()->setCellValue('L'.$i,$row['GDC_COST_1_G']);
			$objTpl->getActiveSheet()->setCellValue('M'.$i,$row['GDC_COST_2_R']);
			$objTpl->getActiveSheet()->setCellValue('N'.$i,$row['GDC_COST_2_O']);
			$objTpl->getActiveSheet()->setCellValue('O'.$i,$row['GDC_COST_2_G']);
			$objTpl->getActiveSheet()->setCellValue('P'.$i,$row['GDC_COST_3_R']);
			$objTpl->getActiveSheet()->setCellValue('Q'.$i,$row['GDC_COST_3_O']);
			$objTpl->getActiveSheet()->setCellValue('R'.$i,$row['GDC_COST_3_G']);
			$objTpl->getActiveSheet()->setCellValue('S'.$i,$row['GDC_COST_4_R']);
			$objTpl->getActiveSheet()->setCellValue('T'.$i,$row['GDC_COST_4_O']);
			$objTpl->getActiveSheet()->setCellValue('U'.$i,$row['GDC_COST_4_G']);
			$i++;
		}
	//$nr_supp_ev=$nr_supp_ev+1;
  }
}
	//echo $sql;die();
//exportSQL($sql, "EAN Export", "EAN Export");
$filename='EAN_export.xls'; //just some random filename
// header('Content-Type: application/vnd.ms-excel; charset: utf-8');
// header('Content-Disposition: attachment;filename="exp.xls"');
// header('Cache-Control: max-age=0');
 
$objWriter = PHPExcel_IOFactory::createWriter($objTpl, 'Excel5');
header('Content-Type: application/vnd.ms-excel; charset: utf-8');
header('Content-Disposition: attachment;filename="export-market-executive.xls"');
header('Cache-Control: max-age=0');
$objWriter->save('php://output');
//$objWriter->save('test2.xls');  //send it to user, of course you can save it to disk also!
 
exit; //done.. exiting!
?>