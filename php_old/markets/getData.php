<?php


	include '/srv/data203386/sftp/jail/ftp/gnoc/mssql_auth/mssql_config.php';
	include 'functionExecuteData.php';
	include 'getConsolidatedView.php';
	session_start();

	$upi = $_SESSION['GOV_UPI'];
	$ym = $_POST["ym"];
	$over1000 = $_POST["over1000"];
	$only32 = $_POST["only32"];

	$GDM = trim($_POST["GDM"]);

	$region = $_POST["region"];
	if ($region != 'ALL'){
		$arrBefore=executeData($upi,$ym,$region,$over1000,$only32, $GDM);
		$mustHave = array(" ","  ","TOTAL", "MS", "NI", "NPO", "SI", "MULTI BL");

		$arr=$arrBefore[0];
		$arrNames=$arrBefore[1];

		

		$queryCountAll = "SELECT TOTAL, CARE_PROJ FROM ReportingDBProd.gsd.TOTAL2MARKET
						WHERE YEAR_MONTH like '$ym'
							AND MARKET LIKE '$region'";
		// die ($queryCountAll);
		$runCount = mssql_query($queryCountAll);

		while($rowTwo = mssql_fetch_row($runCount)){
			$countAll = $rowTwo[0];
			$countCareProj = $rowTwo[1];
		}

		$queryCountProj = "USE ReportingDBProd SELECT CASE WHEN COUNT(CO)=0 THEN NULL ELSE COUNT(CO) END AS CO FROM (SELECT CASE WHEN TOP32PROJECT=''Y'' THEN 1 ELSE NULL END AS CO FROM [gsd].[RAW_DATA] WHERE MARKET=''$region'' AND YEAR_MONTH=''$ym'' AND BUSINESS_LINE IN (SELECT DISTINCT BUSINESS_LINE FROM [gsd].[RAW_DATA] WHERE BUSINESS_LINE!='''' AND BUSINESS_LINE IS NOT NULL) ) AS FINAL";	

							// die($queryCountProj);
		$runProj = mssql_query($queryCountProj);

		while($rowThree = mssql_fetch_row($runProj)){
			$countPro = $rowThree[0];
		}
		// echo $countPro;
		
		foreach($mustHave as $key){
			$flag = 0;

			foreach($arrNames as $keyTwo){
				if($key ==  $keyTwo){
			 		$flag = 1;				
					break;
				}
			}
			
			if($flag == 0){
				$arr[] = array( 
						    "BUSINESS_LINE" => $key, 
						    "PROJ_EXCL_CARE" => ' ', 
						    "PTA_SIGNED" => ' ',
						    "PROJ_32" => ' ',
						    "GDC_100" => ' ',
						    "GDC_1_3" => ' ',
						    "GDC_3_5" => ' ',
						    "GDC_500" => ' ',
						    "COST_GREEN" => ' ',
						    "COST_AMBER" => ' ',
						    "COST_RED" => ' ',
						    "TIME_GREEN" => ' ',
						    "TIME_AMBER" => ' ',
						    "TIME_RED" => ' ',
						    "SCOPE_GREEN" => ' ',
						    "SCOPE_AMBER" => ' ',
						    "SCOPE_RED" => ' ',
						    "QUALITY_GREEN" => ' ',
						    "QUALITY_AMBER" => ' ',
						    "QUALITY_RED" => ' '
						); 
			}		
		}
			for($i = 0; $i < count($arr) ; $i++) {
				if($arr[$i]['BUSINESS_LINE'] == 'Total'){				
					$countPro = $arr[$i]['PROJ_32'];
				}
			}
		
		//print_r($arr);
		for($i = 0; $i < count($arr) ; $i++) {

			if($i == 0){
				$arrFinal[0] = $arr[$i];
				// print_r($arrFinal);
				$arrFinal[0]["BUSINESS_LINE"] = 'Total:'.$countAll. ' Care:'.$countCareProj;

				$arrFinal[0]["PROJ_32"] = $countPro;
			}else if($arr[$i]["BUSINESS_LINE"] == '999'){
				$arrFinal[1] = $arr[$i];
			}else if($arr[$i]["BUSINESS_LINE"] == 'MS'){
				$arrFinal[2] = $arr[$i];
			}else if($arr[$i]["BUSINESS_LINE"] == 'NI'){
				$arrFinal[3] = $arr[$i];
			}else if($arr[$i]["BUSINESS_LINE"] == 'SI'){
				$arrFinal[4] = $arr[$i];
			}else if($arr[$i]["BUSINESS_LINE"] == 'NPO'){
				$arrFinal[5] = $arr[$i];
			}else if( (strtoupper($arr[$i]["BUSINESS_LINE"]) == 'MULTI BL') || ($arr[$i]["BUSINESS_LINE"] == 'MultiBL') ){
				$arrFinal[6] = $arr[$i];
			}else if($arr[$i]["BUSINESS_LINE"] == 'Total'){
				$arrFinal[7] = $arr[$i];
			}
		}


	} else {
		// echo 'merge';
		// die();
		$arrBefore=executeConsolidatedData($ym,$over1000,$only32, $GDM);
		$mustHave = array(" ","  ","TOTAL", "APJ", "EUR", "GCHN", "IND", "LAT", "MEA","NAM");
		$arr=$arrBefore[0];
		$arrNames=$arrBefore[1];

		
		$queryCountAll = "SELECT COUNT(TOTAL) AS TOTAL FROM ReportingDBProd.gsd.TOTAL2MARKET
						WHERE YEAR_MONTH like '$ym'
							";

		$runCount = mssql_query($queryCountAll);

		while($rowTwo = mssql_fetch_row($runCount)){
			$countAll = $rowTwo[0];
		}

		$queryCountProj = "USE ReportingDBProd SELECT CASE WHEN COUNT(CO)=0 THEN NULL ELSE COUNT(CO) END AS CO FROM (SELECT CASE WHEN TOP32PROJECT=''Y'' THEN 1 ELSE NULL END AS CO FROM [gsd].[RAW_DATA] WHERE MARKET=''$region'' AND YEAR_MONTH=''$ym'' AND BUSINESS_LINE IN (SELECT DISTINCT BUSINESS_LINE FROM [gsd].[RAW_DATA] WHERE BUSINESS_LINE!='''' AND BUSINESS_LINE IS NOT NULL) ) AS FINAL";	

							// die($queryCountProj);
		$runProj = mssql_query($queryCountProj);
		while($rowThree = mssql_fetch_row($runProj)){
			$countPro = $rowThree[0];
		}
		// echo $countPro;

		foreach($mustHave as $key){
			$flag = 0;

			foreach($arrNames as $keyTwo){
				if($key ==  $keyTwo){
			 		$flag = 1;				
					break;
				}
			}
			
			if($flag == 0){
				$arr[] = array( 
						    "BUSINESS_LINE" => $key, 
						    "PROJ_EXCL_CARE" => ' ', 
						    "PTA_SIGNED" => ' ',
						    "PROJ_32" => ' ',
						    "GDC_100" => ' ',
						    "GDC_1_3" => ' ',
						    "GDC_3_5" => ' ',
						    "GDC_500" => ' ',
						    "COST_GREEN" => ' ',
						    "COST_AMBER" => ' ',
						    "COST_RED" => ' ',
						    "TIME_GREEN" => ' ',
						    "TIME_AMBER" => ' ',
						    "TIME_RED" => ' ',
						    "SCOPE_GREEN" => ' ',
						    "SCOPE_AMBER" => ' ',
						    "SCOPE_RED" => ' ',
						    "QUALITY_GREEN" => ' ',
						    "QUALITY_AMBER" => ' ',
						    "QUALITY_RED" => ' '
						); 
			}		
		}
			for($i = 0; $i < count($arr) ; $i++) {
				if($arr[$i]['BUSINESS_LINE'] == 'Total'){				
					$countPro = $arr[$i]['PROJ_32'];
				}
			}
		//print_r($arr);
		for($i = 0; $i < count($arr) ; $i++) {

			if($i == 0){
				$arrFinal[0] = $arr[$i];
				// print_r($arrFinal);
				$arrFinal[0]["BUSINESS_LINE"] = $countAll;
				$arrFinal[0]["PROJ_32"] = $countPro;
			}else if($arr[$i]["BUSINESS_LINE"] == '999'){
				$arrFinal[1] = $arr[$i];
			}else if($arr[$i]["BUSINESS_LINE"] == 'APJ'){
				$arrFinal[2] = $arr[$i];
			}else if($arr[$i]["BUSINESS_LINE"] == 'EUR'){
				$arrFinal[3] = $arr[$i];
			}else if($arr[$i]["BUSINESS_LINE"] == 'GCHN'){
				$arrFinal[4] = $arr[$i];
			}else if($arr[$i]["BUSINESS_LINE"] == 'IND'){
				$arrFinal[5] = $arr[$i];
			}else if($arr[$i]["BUSINESS_LINE"] == 'LAT'){
				$arrFinal[6] = $arr[$i];
			}else if($arr[$i]["BUSINESS_LINE"] == 'MEA'){
				$arrFinal[7] = $arr[$i];
			}else if($arr[$i]["BUSINESS_LINE"] == 'NAM'){
				$arrFinal[8] = $arr[$i];
			}else if($arr[$i]["BUSINESS_LINE"] == 'Total'){
				$arrFinal[9] = $arr[$i];
			}
		}
		//TOTAL", "", "EUR", "GCHN", "IND", "LAT", "MEA","NAM
		/*print_r($arrFinal);
		die();*/
		// $str = "asd";
		// ksort($arrFinal);
		// print_r($arrFinal);
		// echo json_encode($arrFinal);
		// print_r($arrFinal);
	}
	

		
		if ($arrFinal[0]["BUSINESS_LINE"] == "Total: Care:" || strlen($arrFinal[0]["BUSINESS_LINE"]) < 5) {
			//print_r($arrFinal[0]["BUSINESS_LINE"]);
			$arrFinal[0]["BUSINESS_LINE"] = 'Total:' . $arrFinal[0]["PROJ_EXCL_CARE"] . ' Care:0';
		}
		ksort($arrFinal);
		echo json_encode($arrFinal);
?>