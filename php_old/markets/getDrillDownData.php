<?php

	include '/srv/data203386/sftp/jail/ftp/gnoc/mssql_auth/mssql_config.php';

	session_start();

	$upi = $_SESSION['GOV_UPI'];
	$ym = $_POST["ym"];
	$bl = $_POST["bLine"];
	$over1000 = $_POST["over1000"];
	$only32 = $_POST["only32"];
	$colCode = $_POST["colCode"];
	$market = $_POST["market"];
	$GDM = $_POST["GDM"];
	
	//echo $bl;die();

	if(strpos($bl, 'Total') !== false || is_numeric($bl) || empty($bl)){
		$bl = '';
	}else if($bl == 'MULTI BL'){
		$bl = 'MULTI BL';
	}

	if($colCode == 'GDC_100'){
		$param = "WHERE CAST(COST_BASELINE AS FLOAT)<100";
	}else if($colCode == 'GDC_1_3'){
		$param = "WHERE (CAST(COST_BASELINE AS FLOAT)>=100 AND CAST(COST_BASELINE AS FLOAT)<500)";
	}else if($colCode == 'GDC_3_5'){
		$param = "WHERE (CAST(COST_BASELINE AS FLOAT)>=500 AND CAST(COST_BASELINE AS FLOAT)<1000)";
	}else if($colCode == 'GDC_500'){
		$param = "WHERE CAST(COST_BASELINE AS FLOAT)>=1000";
	}

	if($colCode == 'COST_RED'){
		$param = "WHERE ([COST_CBL_REVISED-COLOR]=''RED'' OR [COST_PCA_ACTUAL-COLOR]=''RED'' OR [COST_PCA_EAC-COLOR]=''RED'')";
	}else if($colCode == 'COST_AMBER'){
		$param = "WHERE ([COST_CBL_REVISED-COLOR]=''AMBER'' OR [COST_PCA_ACTUAL-COLOR]=''AMBER'' OR [COST_PCA_EAC-COLOR]=''AMBER'') AND ([COST_CBL_REVISED-COLOR] <> ''RED'' AND [COST_PCA_ACTUAL-COLOR] <> ''RED'' AND [COST_PCA_EAC-COLOR] <> ''RED'')";
	}else if($colCode == 'COST_GREEN'){
		$param = "WHERE ( [COST_CBL_REVISED-COLOR]=''GREEN'' AND [COST_PCA_ACTUAL-COLOR]=''GREEN'' AND [COST_PCA_EAC-COLOR]=''GREEN'')";		
	}

	if($colCode == 'TIME_RED'){
		$param = "WHERE ([TIME_SCHEDULE-COLOR]=''RED'' OR [TIME_PROJECT-COLOR]=''RED'')";
	}else if($colCode == 'TIME_AMBER'){
		$param = "WHERE ([TIME_SCHEDULE-COLOR]=''AMBER'' OR [TIME_PROJECT-COLOR]=''AMBER'') AND ([TIME_SCHEDULE-COLOR] <> ''RED'' AND [TIME_PROJECT-COLOR] <> ''RED'')";
	}else if($colCode == 'TIME_GREEN'){
		$param = "WHERE ([TIME_SCHEDULE-COLOR]=''GREEN'' AND [TIME_PROJECT-COLOR]=''GREEN'')";
	}

	if($colCode == 'SCOPE_RED'){
		$param = "WHERE [SCOPE_REMOTE-COLOR]=''RED''";
	}else if($colCode == 'SCOPE_AMBER'){
		$param = "WHERE [SCOPE_REMOTE-COLOR]=''AMBER''";
	}else if($colCode == 'SCOPE_GREEN'){
		$param = "WHERE [SCOPE_REMOTE-COLOR]=''GREEN''";
	}	

	if($colCode == 'QUALITY_RED'){
		$param = "WHERE ([QUALITY_SSD-COLOR]=''RED'' OR [QUALITY_VIRTUAL-COLOR]=''RED'')";
	}else if($colCode == 'QUALITY_AMBER'){
		$param = "WHERE ([QUALITY_SSD-COLOR]=''AMBER'' OR [QUALITY_VIRTUAL-COLOR]=''AMBER'') AND ([QUALITY_SSD-COLOR] <> ''RED'' AND [QUALITY_VIRTUAL-COLOR] <> ''RED'')";
	}else if($colCode == 'QUALITY_GREEN'){
		$param = "WHERE ([QUALITY_SSD-COLOR]=''GREEN'' OR [QUALITY_VIRTUAL-COLOR]=''GREEN'') AND ([QUALITY_SSD-COLOR] <> ''RED'' AND [QUALITY_VIRTUAL-COLOR] <> ''RED'') AND ([QUALITY_SSD-COLOR]<>''AMBER'' OR [QUALITY_VIRTUAL-COLOR]<>''AMBER'')";
	}		

	if($colCode == 'PTA_SIGNED'){
		$ptaComm = '';
		$param = "WHERE PTA_BASED_ACTUAL_DOCUMENT LIKE ''Y''";
		$ptaComm2 = '--';
	}else{
		$ptaComm = '--';
		$ptaComm2 = '';
	}

	include "getProjDataMarketFunction.php";
	
    $arr=getProjDataMerketFunction($ym,$market,$projectName,$bl,'dbl',$param,$over1000,$only32, $GDM);


	if( json_encode($arr) != 'null'){
		echo json_encode($arr);
	}else{
		echo '
			[  
			   {  
			      "IDX":1,
			      "PROJECT_NAME":"No data found"
			   }
			]
		';
	}
	//echo $bl;die();
	
	// $colsToSel = "
	// 	b.[IDX],
	// 	b.[CUSTOMER_NAME], 
	// 	b.[PROJECT_NAME], 
	// 	b.[COST_BASELINE], 
	// 	b.[REVISED_CBL], 
	// 	b.[ESTIMATED_COST], 
	// 	b.[TOTAL_COST], 
	// 	b.[PLANED_COST], 
	// 	b.[PROJECT_START_DATE],
	// 	b.[ACTUAL_START_DATE],
	// 	b.[PROJECT_END_DATE],
	// 	b.[ESTIMATED_COMPLETITION_DATE],
	// 	b.[PROJECT_PLANED_END_DATE], 
	// 	b.[PROJECT_ESTIMATED_COMPLETITION_DATE], 
	// 	b.[PLANNED_SITE_TILL_P12], 
	// 	b.[ACTUAL_SITE_TILL_P12], 
	// 	b.[PLANNED_MILESTONES], 
	// 	b.[ACTUAL_MILESTONES], 
	// 	b.[BUSINESS_LINE], 
	// 	b.[NR_CHANGES_REQUEST_APPROVED], 
	// 	b.[COMPETENCE_ADHERENCE], 
	// 	b.[TOTAL_WORK_PACKAGES_HAS_TO_WORK], 
	// 	b.[TOTAL_WORK_PACKAGES_ACTUAL_WORKED], 
	// 	b.[PTA_BASED_ON_BC], 
	// 	b.[PTA_BASED_ACTUAL_DOCUMENT], 
	// 	b.[SOLUTION_DOCUMENT], 
	// 	b.[VIRTUAL_ZERO], 
	// 	b.[CUSTOMER_TEAM_SURVEY], 
	// 	b.[IDX_GIC],
	// 	b.[GDN_NAME], 
	// 	b.[PROJECT_MANAGER], 
	// 	b.[DELIVERY_CENTER], 
	// 	b.[BUSINESSS_CASE], 
	// 	b.[PROJECT_DURATION], 
	// 	b.[PMO], 
	// 	b.[REMARKS], 
	// 	b.[MARKET],
	// 	PTA_BASED_ACTUAL_DOCUMENT PTA_SIGNED
	// ";

	// if($colCode == 'PROJ_EXCL_CARE'){
	// 	$getRelevantProjects = "USE ReportingDBProd 
	// 								SELECT DISTINCT $colsToSel INTO #tempGSDRaw 	
	// 									 FROM [GSD].[DRILL_DOWN_DATA] a
	// 								JOIN ReportingDBProd.[gsd].[RAW_DATA] b 
	// 									ON a.PROJECT_NAME = b.IDX --and a.CUSTOMER_NAME = B.CUSTOMER_NAME
	// 								WHERE a.BUSINESS_LINE LIKE '$bl'
	// 									AND a.upi LIKE '$upi'
	// 									AND b.YEAR_MONTH LIKE '$ym'
	// 							ORDER BY b.IDX
	// 							";
	// }else{
	// 	$getRelevantProjects = "USE ReportingDBProd 
	// 							$ptaComm SELECT *  INTO #tempGSDRaw FROM (
	// 							SELECT DISTINCT $colsToSel $ptaComm2 INTO #tempGSDRaw 	
	// 								 FROM [GSD].[DRILL_DOWN_DATA] a
	// 							JOIN ReportingDBProd.[gsd].[RAW_DATA] b 
	// 								ON a.PROJECT_NAME = b.IDX --and a.CUSTOMER_NAME = B.CUSTOMER_NAME
	// 							WHERE a.BUSINESS_LINE LIKE '$bl'
	// 								$param
	// 								AND a.upi LIKE '$upi'
	// 								AND b.YEAR_MONTH LIKE '$ym'
	// 							$ptaComm )final WHERE PTA_SIGNED LIKE 'Y'									
	// 							ORDER BY IDX
	// 							";	
	// }

	// //die($getRelevantProjects);

	// $calculateQuery = "
	// 		SET ANSI_NULLS ON 
	// 		SET QUOTED_IDENTIFIER ON
	// 		SET CONCAT_NULL_YIELDS_NULL ON
	// 		SET ANSI_WARNINGS ON
	// 		SET ANSI_PADDING ON
	// 		USE ReportingDBProd

	// 		SELECT DISTINCT [IDX]
	// 	      ,[PROJECT_NAME]
	// 	       ,cast(ROUND(CAST(COST_BASELINE AS FLOAT),2) as nvarchar)+ '/' +CASE WHEN (REVISED_CBL IS NULL OR REVISED_CBL='-100') THEN cast(ISNULL(ROUND(CAST(COST_BASELINE AS FLOAT),2),ROUND(CAST(COST_BASELINE AS FLOAT),2)) as nvarchar)
	// 	       ELSE cast(ISNULL(ROUND(CAST(REVISED_CBL AS FLOAT),2),ROUND(CAST(COST_BASELINE AS FLOAT),2)) as nvarchar) END as COST_CBL_REVISED 
	// 			,CASE
	// 		        WHEN TOTAL_COST IS NULL AND PLANED_COST IS NULL THEN NULL
	// 		        WHEN TOTAL_COST IS NULL OR PLANED_COST IS NULL THEN 'NA'
	// 		        WHEN PLANED_COST=0 THEN 'NA'
	// 		        WHEN PLANED_COST='-100' OR TOTAL_COST='-100' THEN 'NA'
	// 		        ELSE CAST(CAST(CAST(TOTAL_COST AS FLOAT)/PLANED_COST *100 AS DECIMAL(15,2)) AS VARCHAR)
	// 		      END AS COST_PCA_ACTUAL
	// 			,CASE
	// 	        WHEN ESTIMATED_COST IS NULL AND (REVISED_CBL IS NULL AND COST_BASELINE IS NULL) THEN NULL
	// 	        WHEN ESTIMATED_COST IS NULL THEN 'NA'
	// 	        WHEN REVISED_CBL='-100' OR REVISED_CBL IS NULL OR REVISED_CBL=0 THEN 
	// 	          CASE
	// 	            WHEN CAST(COST_BASELINE AS FLOAT)=-100 OR COST_BASELINE IS NULL OR CAST(COST_BASELINE AS FLOAT)=0 THEN 'NA'
	// 	            ELSE CAST(CAST(CAST(ESTIMATED_COST AS FLOAT)/COST_BASELINE *100 AS DECIMAL(15,2)) AS VARCHAR)+' %' 
	// 	          END
	// 	        ELSE CAST(CAST(CAST(ESTIMATED_COST AS FLOAT)/REVISED_CBL *100 AS DECIMAL(15,2)) AS VARCHAR)+' %' 
	// 	      END as COST_PCA_EAC	
	// 	      ,[ACTUAL_START_DATE] as TIME_START
	// 	      ,[PROJECT_END_DATE] as TIME_END
	// 	       ,CASE
	// 	        WHEN [ACTUAL_START_DATE] IS NULL AND ESTIMATED_COMPLETITION_DATE IS NULL AND [PROJECT_END_DATE] IS NULL THEN NULL
	// 	        WHEN DATEDIFF(DD,[ACTUAL_START_DATE], [PROJECT_END_DATE] )=0 THEN NULL
	// 	        ELSE CAST(CAST(CAST(DATEDIFF(DD,[ACTUAL_START_DATE],[ESTIMATED_COMPLETITION_DATE]) AS FLOAT)/DATEDIFF(DD,[ACTUAL_START_DATE], [PROJECT_END_DATE] ) *100 AS DECIMAL(15,2)) AS VARCHAR)
 //      			END as TIME_SCHEDULE
	// 	       ,CASE
	// 	        WHEN BUSINESS_LINE='MS' THEN '100 %'
	// 	        --WHEN BUSINESS_LINE='SI' THEN 
	// 	          --CASE
	// 	            --WHEN [ACTUAL_MILESTONES] IS NULL AND PLANNED_MILESTONES IS NULL THEN NULL
	// 	      			--WHEN cast([ACTUAL_MILESTONES] as float)='-100' AND cast(PLANNED_MILESTONES as float)='-100' THEN 'NA'
	// 	                 -- WHEN cast(PLANNED_MILESTONES as float)=0 THEN 'NA'
	// 	                  --WHEN [ACTUAL_MILESTONES]='-100' AND PLANNED_MILESTONES='-100' THEN 'NA'
	// 	                  --WHEN PLANNED_MILESTONES=0 THEN 'NA'
	// 	                  --ELSE CAST(CAST(CAST(ACTUAL_MILESTONES AS FLOAT)/PLANNED_MILESTONES *100 AS DECIMAL(15,2)) AS VARCHAR)+' %' 
	// 	                --END
	// 	        ELSE
	// 	          CASE
	// 	            WHEN [ACTUAL_SITE_TILL_P12] IS NULL AND PLANNED_SITE_TILL_P12 IS NULL THEN NULL
	// 	            WHEN [ACTUAL_SITE_TILL_P12]='-100' OR PLANNED_SITE_TILL_P12='-100' THEN 'NA'
	// 	            WHEN CAST([PLANNED_SITE_TILL_P12] AS FLOAT)=0 THEN 'NA'
	// 	            ELSE 
	// 	              CASE WHEN CAST(CAST(CAST(ACTUAL_SITE_TILL_P12 AS FLOAT)/PLANNED_SITE_TILL_P12 *100 AS DECIMAL(15,2)) AS VARCHAR) LIKE '100.00' THEN '100'
	// 	                ELSE CAST(CAST(CAST(ACTUAL_SITE_TILL_P12 AS FLOAT)/PLANNED_SITE_TILL_P12 *100 AS DECIMAL(15,2)) AS VARCHAR)
	// 	              END
	// 	          END
	// 	      END  AS TIME_PROJECT
	// 		  ,[BUSINESS_LINE] as SCOPE_BLS
	// 		  ,[NR_CHANGES_REQUEST_APPROVED] as SCOPE_CHANGE
	// 		  ,[COMPETENCE_ADHERENCE] as SCOPE_COMPETENCE
	// 		  ,CASE
	// 	        WHEN TOTAL_WORK_PACKAGES_HAS_TO_WORK IS NULL AND TOTAL_WORK_PACKAGES_ACTUAL_WORKED IS NULL THEN NULL
	// 	        WHEN TOTAL_WORK_PACKAGES_HAS_TO_WORK IS NULL OR TOTAL_WORK_PACKAGES_ACTUAL_WORKED IS NULL THEN '-100'
	// 	        WHEN TOTAL_WORK_PACKAGES_HAS_TO_WORK='-100' OR TOTAL_WORK_PACKAGES_ACTUAL_WORKED='-100' THEN '-100'
	// 	        WHEN TOTAL_WORK_PACKAGES_ACTUAL_WORKED=0 THEN '-100'
	// 	        ELSE ROUND(CAST(TOTAL_WORK_PACKAGES_ACTUAL_WORKED AS FLOAT)/TOTAL_WORK_PACKAGES_HAS_TO_WORK * 100,2)
	// 	      END AS SCOPE_REMOTE
	// 	      ,PTA_BASED_ACTUAL_DOCUMENT AS QUALITY_PTA
	// 		  ,CASE WHEN [SOLUTION_DOCUMENT]='Y' THEN 'Y' ELSE 'N' END as QUALITY_SSD
	// 		  ,CASE WHEN VIRTUAL_ZERO IS NULL THEN NULL
	// 		  		WHEN VIRTUAL_ZERO LIKE '-100' THEN '-100'
	// 		  		ELSE [VIRTUAL_ZERO]  END as QUALITY_VIRTUAL
	// 		  ,[CUSTOMER_TEAM_SURVEY] as QUALITY_CUSTOMER
	// 		  , '' as OVERALL_PROJ_STS
	// 		  ,C.[WBS] as  WBS_CODE
	// 		  ,D.SVO as SWO_CODE
	// 	      ,[IDX_GIC] as GIC_CODE
	// 		  ,[GDN_NAME] as NAME_OF_GDM
	// 	      ,[PROJECT_MANAGER] as NAME_OF_PM
	// 	      ,[DELIVERY_CENTER] as DELIVERY_CENTER
	// 	  	FROM #tempGSDRaw A
	// 	    LEFT JOIN (SELECT DISTINCT r.IDX_COUNTRY_CUSTOMER,
	// 		 STUFF((SELECT distinct ','+ a.COUNTRY
	// 	               FROM [ReportingDBProd].[gsd].[MAPPING_COUNTRY] a
	// 	             WHERE r.IDX_COUNTRY_CUSTOMER = a.IDX_COUNTRY_CUSTOMER
	// 	            FOR XML PATH(''), TYPE).value('.','VARCHAR(max)'), 1, 1, '')  AS [COUNTRY_OF_CUSTOMER]
	// 			FROM ReportingDBProd.[gsd].[MAPPING_COUNTRY] r) B 
	// 			ON A.IDX=B.IDX_COUNTRY_CUSTOMER
	// 		LEFT JOIN (SELECT DISTINCT r.IDX_WBS,
	// 		 STUFF((SELECT distinct ','+ a.WBS
	// 	               FROM [ReportingDBProd].[gsd].[MAPPING_WBS] a
	// 	             WHERE r.IDX_WBS = a.IDX_WBS
	// 	            FOR XML PATH(''), TYPE).value('.','VARCHAR(max)'), 1, 1, '')  AS [WBS]
	// 			FROM ReportingDBProd.[gsd].[MAPPING_WBS] r) C
	// 			ON A.IDX=C.IDX_WBS
	// 		LEFT JOIN (SELECT DISTINCT r.IDX_SVO,
	// 		 STUFF((SELECT distinct ','+ a.SVO
	// 	               FROM [ReportingDBProd].[gsd].[MAPPING_SVO] a
	// 	             WHERE r.IDX_SVO = a.IDX_SVO
	// 	            FOR XML PATH(''), TYPE).value('.','VARCHAR(max)'), 1, 1, '')  AS [SVO]
	// 			FROM ReportingDBProd.[gsd].[MAPPING_SVO] r) D
	// 			ON A.IDX=D.IDX_SVO	
	// 		--WHERE a.BUSINESS_LINE LIKE '$bl'
	// 		ORDER BY IDX
	// 		drop table #tempGSDRaw 
	// ";

	// /*echo $getRelevantProjects;
	// die($calculateQuery);*/
	// $runOne = mssql_query($getRelevantProjects);
	// $runTwo = mssql_query($calculateQuery);

	// while($row = mssql_fetch_assoc($runTwo)){
	// 	$arr[] = $row;
	// }

	// if( json_encode($arr) != 'null'){
 //    	echo json_encode($arr);
 // 	}else{
	// 	echo '
	//       [  
	//          {  
	//             "IDX":1,
	//             "PROJECT_NAME":"No data found"
	//          }
	//       ]
	//     ';
	// }
?>