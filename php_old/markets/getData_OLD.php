<?php

	include '/srv/data203386/sftp/jail/ftp/gnoc/mssql_auth/mssql_config.php';

	session_start();

	$upi = $_SESSION['GOV_UPI'];
	$ym = $_POST["ym"];
	$region = $_POST["region"];

	$mustHave = array(" ","  ","Total", "MS", "NI", "NPO", "SI", "MULTI BL");

	$sql = "use ReportingDBProd
		SET ANSI_NULLS ON 
		SET QUOTED_IDENTIFIER ON
		SET CONCAT_NULL_YIELDS_NULL ON
		SET ANSI_WARNINGS ON
		SET ANSI_PADDING ON

		DECLARE @calculateValues nvarchar(4000)
		DECLARE @convertValues nvarchar(4000)
		DECLARE @showExecutive1 nvarchar(4000)
		DECLARE @showExecutive2 nvarchar(4000)
		DECLARE @showExecutive3 nvarchar(4000)
		DECLARE @rawValues nvarchar(4000)

		SET @calculateValues= 
				'
					SELECT * into #tempGov FROM(
						SELECT
						PROJECT_NAME
						,CUSTOMER_NAME
						,[BUSINESS_LINE] 
						,case when PMO not like  ''Y'' then PTA_BASED_ACTUAL_DOCUMENT else PTA_BASED_ON_BC end PTA_SIGNED
						, '''' PROJ_32
						,COST_BASELINE
						,cast([COST_BASELINE] as nvarchar)+ ''/'' + cast([REVISED_CBL] as nvarchar) COST_1
						,(SELECT CASE WHEN PLANED_COST NOT LIKE ''0'' THEN (CAST( ROUND(([TOTAL_COST] / [PLANED_COST] * 100), 2) as nvarchar)) ELSE NULL END )as COST_2
						,(SELECT CASE WHEN ESTIMATED_COST NOT LIKE ''0'' THEN (CAST( ROUND(([REVISED_CBL] / [ESTIMATED_COST] * 100), 2) as nvarchar)) ELSE NULL END)as COST_3
						, [PROJECT_START_DATE] as TIME_1
						, [PROJECT_END_DATE] as TIME_2
						, CASE WHEN BUSINESS_LINE LIKE ''MS'' THEN ''100''
							WHEN DATEDIFF(DD,[ACTUAL_START_DATE], [PROJECT_END_DATE] )=0 THEN NULL
							ELSE
							(CAST((DATEDIFF(dd, [ACTUAL_START_DATE], [ESTIMATED_COMPLETITION_DATE]) * 1.00 / DATEDIFF(dd, [ACTUAL_START_DATE], [PROJECT_END_DATE]) * 100) as nvarchar)) 
							END
						as TIME_3
		       		  ,CASE
				        WHEN BUSINESS_LINE=''MS'' THEN ''100''
				        WHEN BUSINESS_LINE=''SI'' THEN 
				          CASE
				            WHEN [ACTUAL_MILESTONES] IS NULL AND PLANNED_MILESTONES IS NULL THEN NULL
				            WHEN [ACTUAL_MILESTONES]=''-100'' AND PLANNED_MILESTONES=''-100'' THEN NULL
				            WHEN PLANNED_MILESTONES=0 THEN NULL
				            ELSE CAST(CAST(CAST(ACTUAL_MILESTONES AS FLOAT)/PLANNED_MILESTONES *100 AS DECIMAL(15,2)) AS VARCHAR)
				          END
				        ELSE
				          CASE
				            WHEN [ACTUAL_SITE_TILL_P12] IS NULL AND PLANNED_SITE_TILL_P12 IS NULL THEN NULL
				            WHEN [ACTUAL_SITE_TILL_P12]=''-100'' AND PLANNED_SITE_TILL_P12=''-100'' THEN NULL
				            WHEN PLANNED_SITE_TILL_P12=0 THEN NULL
				            ELSE CAST(CAST(CAST(ACTUAL_SITE_TILL_P12 AS FLOAT)/PLANNED_SITE_TILL_P12 *100 AS DECIMAL(15,2)) AS VARCHAR)
				          END
				      END AS TIME_4	
						, [NR_CHANGES_REQUEST_APPROVED] SCOPE_1
						, [COMPETENCE_ADHERENCE] SCOPE_2
						, (CAST(([TOTAL_WORK_PACKAGES_HAS_TO_WORK] / [TOTAL_WORK_PACKAGES_ACTUAL_WORKED] * 100)as nvarchar)) SCOPE_3
						, case when PMO not like  ''Y'' then PTA_BASED_ACTUAL_DOCUMENT else PTA_BASED_ON_BC end as QUALITY_1
						, [SOLUTION_DOCUMENT] as QUALITY_2
						, [VIRTUAL_ZERO] as QUALITY_3
						, CASE WHEN [CUSTOMER_TEAM_SURVEY] LIKE ''-100'' THEN ''NA'' ELSE NULL END QUALITY_4 
						, ''-'' OVR_STS
						FROM ReportingDBProd.[gsd].[RAW_DATA] 
						WHERE BUSINESS_LINE IS NOT NULL
						AND YEAR_MONTH LIKE ''$ym''
						AND MARKET LIKE ''$region''
						--AND COST_BASELINE IS NOT NULL
						--AND TOTAL_COST IS NOT NULL
						--AND TOTAL_WORK_PACKAGES_HAS_TO_WORK IS NOT NULL
						--AND PTA_BASED_ON_BC IS NOT NULL
						--AND VIRTUAL_ZERO IS NOT NULL
					)f
					
					'

		SET @convertValues = '
					select 
						 PROJECT_NAME
						,CUSTOMER_NAME
						,BUSINESS_LINE
						,PTA_SIGNED
						,PROJ_32
						,[COST_BASELINE]
						,CASE WHEN COST_1 IS NOT NULL THEN 0 ELSE 2 END COST_1
						,CASE WHEN COST_2 IS NOT NULL THEN
							 CASE WHEN CAST(COST_2 as float) >= 90 AND CAST(COST_2 as float) <= 110 
									THEN 0 
							  ELSE 
									CASE WHEN (CAST(COST_2 as float) >= 80 AND CAST(COST_2 as float) < 90) OR (CAST(COST_2 as float) >= 111 AND CAST(COST_2 as float) <= 120)
									THEN 1
									ELSE
										2
									END
							  END 
						  ELSE NULL END
						  COST_2
						,
						CASE WHEN COST_3 IS NOT NULL THEN
							CASE WHEN CAST(COST_3 as float) > 90 AND CAST(COST_3 as float) < 110 
								THEN 0
							ELSE
								CASE WHEN (CAST(COST_3 as float) >= 80 AND CAST(COST_3 as float) <= 90) OR (CAST(COST_3 as float) >= 111 AND CAST(COST_3 as float) <= 120)
								THEN 1
								ELSE 
									2
								END
							END 
						ELSE NULL END
						COST_3
					   ,CASE WHEN TIME_1 IS NOT NULL THEN 0 ELSE 2 END TIME_1
					   ,CASE WHEN TIME_2 IS NOT NULL THEN 0 ELSE 2 END TIME_2
					   ,CASE WHEN TIME_3 IS NOT NULL THEN
						   CASE WHEN CAST(TIME_3 as float) > 90 AND CAST(TIME_3 as float) < 110 
								THEN 0
							ELSE
								CASE WHEN (CAST(TIME_3 as float) >= 80 AND CAST(TIME_3 as float) <= 90) OR (CAST(TIME_3 as float) >= 111 AND CAST(TIME_3 as float) <= 120)
								THEN 1
								ELSE 
									2
								END
							END 
						ELSE ''2'' END
						TIME_3
						,
						CASE WHEN TIME_4 IS NOT NULL THEN
							CASE WHEN CAST(TIME_4 as float) > 90 AND CAST(TIME_4 as float) < 110 
								THEN 0
							ELSE
								CASE WHEN (CAST(TIME_4 as float) >= 80 AND CAST(TIME_4 as float) <= 90) OR (CAST(TIME_4 as float) >= 111 AND CAST(TIME_4 as float) <= 120)
								THEN 1
								ELSE 
									2
								END
							END
						ELSE ''2'' END 
						TIME_4
						,CASE WHEN SCOPE_1 > 0 THEN 0 ELSE CASE WHEN SCOPE_1 = 0 THEN 2 ELSE NULL END END SCOPE_1
						,CASE WHEN SCOPE_2 LIKE ''Y'' THEN 0 ELSE CASE WHEN SCOPE_2 LIKE ''N'' THEN 2 ELSE NULL END END SCOPE_2
						,
						CASE WHEN SCOPE_3 IS NOT NULL THEN
							CASE WHEN CAST(SCOPE_3 as float) > 90 AND CAST(SCOPE_3 as float) < 110 
								THEN 0
							ELSE
								CASE WHEN CAST(SCOPE_3 as float) > 80 AND CAST(SCOPE_3 as float) <= 90
								THEN 1
								ELSE 2
								END
							END 
						ELSE NULL END
						SCOPE_3
						,CASE WHEN QUALITY_1 LIKE ''Y'' THEN 0 ELSE CASE WHEN QUALITY_1 LIKE ''N'' THEN 2 ELSE NULL END END QUALITY_1
						,CASE WHEN QUALITY_2 LIKE ''Y'' THEN 0 ELSE CASE WHEN QUALITY_2 LIKE ''N'' THEN 2 ELSE NULL END END QUALITY_2
						,
						CASE WHEN QUALITY_3 IS NOT NULL THEN
							CASE WHEN CAST(QUALITY_3 as float) > 93 
								THEN 0
							ELSE
								CASE WHEN (CAST(QUALITY_3 as float) >= 83.7 AND CAST(QUALITY_3 as float) <= 93) 
								THEN 1
								ELSE 
									2
								END
							END 
						ELSE NULL END
						QUALITY_3
						,
						CASE WHEN QUALITY_4 IS NOT NULL THEN
						  CASE WHEN QUALITY_4 LIKE ''NA'' THEN ''0'' ELSE
							CASE WHEN CAST(QUALITY_4 as float) > 8.2 
								THEN 0
							ELSE
								CASE WHEN (CAST(QUALITY_4 as float) >= 7.38 AND CAST(QUALITY_4 as float) <= 8.2) 
								THEN 1
								ELSE 
									2
								END
							END 
						 END
						ELSE NULL END
						QUALITY_4
					into #tempGovRaw
					from #tempGov	

				'

		SET @rawValues = '
			SELECT PROJECT_NAME
				,CUSTOMER_NAME
				,BUSINESS_LINE
				,PTA_SIGNED
				,PROJ_32
				, case when CAST(COST_BASELINE AS FLOAT) < 100 then ''100'' else 
					case when CAST(COST_BASELINE AS FLOAT) <= 300 AND CAST(COST_BASELINE AS FLOAT) > 100 then ''1_3'' else
						case when CAST(COST_BASELINE AS FLOAT) <= 500 AND CAST(COST_BASELINE AS FLOAT) > 300 then ''3_5'' else 
							case when CAST(COST_BASELINE AS FLOAT) > 500 then ''500'' else ''Wrong'' END
						END
					END
				END GDC_COST
				,CASE WHEN COST_1 IS NOT NULL OR COST_2 IS NOT NULL OR COST_3 IS NOT NULL THEN
					CASE WHEN COST_1 = 2 OR COST_2 = 2 OR COST_3 = 2 THEN ''RED'' 
						ELSE CASE WHEN COST_1 = 1 OR COST_2 = 1 OR COST_3 = 1 THEN ''AMBER''
						ELSE ''GREEN''
					END
				END
				ELSE '''' 
				END
				COST_COLOR
				,CASE WHEN TIME_1 IS NOT NULL OR TIME_2 IS NOT NULL OR TIME_3 IS NOT NULL OR TIME_4 IS NOT NULL THEN
				  CASE WHEN TIME_1 = 2 OR TIME_2 = 2 OR TIME_3 = 2 OR TIME_4 = 2 THEN ''RED'' 
					ELSE 
						CASE WHEN TIME_1 = 1 OR TIME_2 = 1 OR TIME_3 = 1 OR TIME_4 = 1 THEN ''AMBER''
						ELSE ''GREEN'' 
						END
					END 
				ELSE '''' END
				TIME_COLOR
				,CASE WHEN SCOPE_1 IS NOT NULL OR SCOPE_2 IS NOT NULL OR SCOPE_3 IS NOT NULL THEN
				  CASE WHEN SCOPE_1 = 2 OR SCOPE_2 = 2 OR SCOPE_3 = 2 THEN ''RED'' 
					ELSE 
						CASE WHEN SCOPE_1 = 1 OR SCOPE_2 = 1 OR SCOPE_3 = 1 THEN ''AMBER''
						ELSE ''GREEN'' 
						END
					END 
				 ELSE ''RED'' END
				SCOPE_COLOR
				,CASE WHEN QUALITY_1 IS NOT NULL OR QUALITY_2 IS NOT NULL OR QUALITY_3 IS NOT NULL OR QUALITY_4 IS NOT NULL THEN
				  CASE WHEN QUALITY_1 = 2 OR QUALITY_2 = 2 OR QUALITY_3 = 2 OR QUALITY_4 = 2 THEN ''RED'' 
					ELSE 
						CASE WHEN QUALITY_1 = 1 OR QUALITY_2 = 1 OR QUALITY_3 = 1 OR QUALITY_4 = 1 THEN ''AMBER''
						ELSE ''GREEN'' 
						END
					END 
				 ELSE ''RED'' END
				QUALITY_COLOR
				,(SELECT ''$upi'') AS UPI
			INTO #finalRaw
			FROM #tempGovRaw

			DELETE FROM [ReportingDBProd].[GSD].[DRILL_DOWN_DATA] WHERE UPI LIKE ''$upi''
			INSERT INTO [ReportingDBProd].[GSD].[DRILL_DOWN_DATA] SELECT * FROM #finalRaw
					
		'
		SET @showExecutive1 = 
				'
				SELECT 
					''  ''
					as BUSINESS_LINE,
					SUM(PROJ_EXCL_CARE)PROJ_EXCL_CARE,
					SUM(PTA_SIGNED)PTA_SIGNED,
					''''PROJ_32, --SUM(PROJ_32)PROJ_32,
					SUM(GDC_100)GDC_100,
					SUM(GDC_1_3)GDC_1_3,
					SUM(GDC_3_5)GDC_3_5,
					SUM(GDC_500)GDC_500,
					SUM(COST_GREEN)COST_GREEN,
					SUM(COST_AMBER)COST_AMBER,
					SUM(COST_RED)COST_RED,
					SUM(TIME_GREEN)TIME_GREEN,
					SUM(TIME_AMBER)TIME_AMBER,
					SUM(TIME_RED)TIME_RED,
					SUM(SCOPE_GREEN)SCOPE_GREEN,
					SUM(SCOPE_AMBER)SCOPE_AMBER,
					SUM(SCOPE_RED)SCOPE_RED,
					SUM(QUALITY_GREEN)QUALITY_GREEN,
					SUM(QUALITY_AMBER)QUALITY_AMBER,
					SUM(QUALITY_RED) QUALITY_RED
					FROM (
							SELECT
								COUNT(BUSINESS_LINE)PROJ_EXCL_CARE
								,SUM(CASE WHEN PTA_SIGNED LIKE ''Y'' THEN 1 ELSE 0 END)PTA_SIGNED
								,PROJ_32
								,SUM(CASE WHEN GDC_COST LIKE ''100'' THEN 1 ELSE 0 END)GDC_100
								,SUM(CASE WHEN GDC_COST LIKE ''1_3'' THEN 1 ELSE 0 END)GDC_1_3
								,SUM(CASE WHEN GDC_COST LIKE ''3_5'' THEN 1 ELSE 0 END)GDC_3_5
								,SUM(CASE WHEN GDC_COST LIKE ''500'' THEN 1 ELSE 0 END)GDC_500
								,SUM(CASE WHEN COST_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)COST_GREEN
								,SUM(CASE WHEN COST_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)COST_AMBER
								,SUM(CASE WHEN COST_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)COST_RED
								,SUM(CASE WHEN TIME_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)TIME_GREEN
								,SUM(CASE WHEN TIME_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)TIME_AMBER
								,SUM(CASE WHEN TIME_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)TIME_RED
								,SUM(CASE WHEN SCOPE_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)SCOPE_GREEN
								,SUM(CASE WHEN SCOPE_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)SCOPE_AMBER
								,SUM(CASE WHEN SCOPE_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)SCOPE_RED
								,SUM(CASE WHEN QUALITY_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)QUALITY_GREEN
								,SUM(CASE WHEN QUALITY_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)QUALITY_AMBER
								,SUM(CASE WHEN QUALITY_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)QUALITY_RED
							FROM
							#finalRaw
							GROUP BY BUSINESS_LINE, PROJ_32
					)fff
				UNION
		'

		SET @showExecutive3 ='
				SELECT
				''999'',999,999,999,999,999,999,999,999,999,999,999,999,999,999,999,999,999,999,999
				UNION
		'

		SET @showExecutive2 = '
				SELECT 
					BUSINESS_LINE
					,COUNT(BUSINESS_LINE)PROJ_EXCL_CARE
					,SUM(CASE WHEN PTA_SIGNED LIKE ''Y'' THEN 1 ELSE 0 END)PTA_SIGNED
					,PROJ_32
					,SUM(CASE WHEN GDC_COST LIKE ''100'' THEN 1 ELSE 0 END)GDC_100
					,SUM(CASE WHEN GDC_COST LIKE ''1_3'' THEN 1 ELSE 0 END)GDC_1_3
					,SUM(CASE WHEN GDC_COST LIKE ''3_5'' THEN 1 ELSE 0 END)GDC_3_5
					,SUM(CASE WHEN GDC_COST LIKE ''500'' THEN 1 ELSE 0 END)GDC_500
					,SUM(CASE WHEN COST_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)COST_GREEN
					,SUM(CASE WHEN COST_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)COST_AMBER
					,SUM(CASE WHEN COST_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)COST_RED
					,SUM(CASE WHEN TIME_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)TIME_GREEN
					,SUM(CASE WHEN TIME_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)TIME_AMBER
					,SUM(CASE WHEN TIME_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)TIME_RED
					,SUM(CASE WHEN SCOPE_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)SCOPE_GREEN
					,SUM(CASE WHEN SCOPE_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)SCOPE_AMBER
					,SUM(CASE WHEN SCOPE_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)SCOPE_RED
					,SUM(CASE WHEN QUALITY_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)QUALITY_GREEN
					,SUM(CASE WHEN QUALITY_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)QUALITY_AMBER
					,SUM(CASE WHEN QUALITY_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)QUALITY_RED
				FROM
				#finalRaw
				GROUP BY BUSINESS_LINE, PROJ_32
				UNION 
				SELECT 
					''Total'',
					SUM(PROJ_EXCL_CARE),
					SUM(PTA_SIGNED),
					'''', --SUM(PROJ_32),
					SUM(GDC_100),
					SUM(GDC_1_3),
					SUM(GDC_3_5),
					SUM(GDC_500),
					SUM(COST_GREEN),
					SUM(COST_AMBER),
					SUM(COST_RED),
					SUM(TIME_GREEN),
					SUM(TIME_AMBER),
					SUM(TIME_RED),
					SUM(SCOPE_GREEN),
					SUM(SCOPE_AMBER),
					SUM(SCOPE_RED),
					SUM(QUALITY_GREEN),
					SUM(QUALITY_AMBER),
					SUM(QUALITY_RED) 
					FROM (
							SELECT
								COUNT(BUSINESS_LINE)PROJ_EXCL_CARE
								,SUM(CASE WHEN PTA_SIGNED LIKE ''Y'' THEN 1 ELSE 0 END)PTA_SIGNED
								,PROJ_32
								,SUM(CASE WHEN GDC_COST LIKE ''100'' THEN 1 ELSE 0 END)GDC_100
								,SUM(CASE WHEN GDC_COST LIKE ''1_3'' THEN 1 ELSE 0 END)GDC_1_3
								,SUM(CASE WHEN GDC_COST LIKE ''3_5'' THEN 1 ELSE 0 END)GDC_3_5
								,SUM(CASE WHEN GDC_COST LIKE ''500'' THEN 1 ELSE 0 END)GDC_500
								,SUM(CASE WHEN COST_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)COST_GREEN
								,SUM(CASE WHEN COST_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)COST_AMBER
								,SUM(CASE WHEN COST_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)COST_RED
								,SUM(CASE WHEN TIME_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)TIME_GREEN
								,SUM(CASE WHEN TIME_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)TIME_AMBER
								,SUM(CASE WHEN TIME_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)TIME_RED
								,SUM(CASE WHEN SCOPE_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)SCOPE_GREEN
								,SUM(CASE WHEN SCOPE_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)SCOPE_AMBER
								,SUM(CASE WHEN SCOPE_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)SCOPE_RED
								,SUM(CASE WHEN QUALITY_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)QUALITY_GREEN
								,SUM(CASE WHEN QUALITY_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)QUALITY_AMBER
								,SUM(CASE WHEN QUALITY_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)QUALITY_RED
							FROM
							#finalRaw
							GROUP BY BUSINESS_LINE, PROJ_32
					)ff

				DROP TABLE #finalRaw
				drop table #tempGov
				drop table #tempGovRaw

				'

		execute (@calculateValues + @convertValues + @rawValues + @showExecutive1 + @showExecutive3 +@showExecutive2)
	
	
	
	";

	//die($sql);
	$rs = mssql_query($sql);

	while ($row = mssql_fetch_assoc($rs)){	
		$arr[] = $row;

		$arrNames[] = $row["BUSINESS_LINE"];
	}

	$queryCountAll = "SELECT COUNT(PROJECT_NAME) FROM ReportingDBProd.gsd.RAW_DATA
					WHERE YEAR_MONTH like '$ym'";

	$runCount = mssql_query($queryCountAll);

	while($rowTwo = mssql_fetch_row($runCount)){
		$countAll = $rowTwo[0];
	}

	foreach($mustHave as $key){
		$flag = 0;

		foreach($arrNames as $keyTwo){
			if($key ==  $keyTwo){
		 		$flag = 1;				
				break;
			}
		}
		
		if($flag == 0){
			$arr[] = array( 
					    "BUSINESS_LINE" => $key, 
					    "PROJ_EXCL_CARE" => ' ', 
					    "PTA_SIGNED" => ' ',
					    "PROJ_32" => ' ',
					    "GDC_100" => ' ',
					    "GDC_1_3" => ' ',
					    "GDC_3_5" => ' ',
					    "GDC_500" => ' ',
					    "COST_GREEN" => ' ',
					    "COST_AMBER" => ' ',
					    "COST_RED" => ' ',
					    "TIME_GREEN" => ' ',
					    "TIME_AMBER" => ' ',
					    "TIME_RED" => ' ',
					    "SCOPE_GREEN" => ' ',
					    "SCOPE_AMBER" => ' ',
					    "SCOPE_RED" => ' ',
					    "QUALITY_GREEN" => ' ',
					    "QUALITY_AMBER" => ' ',
					    "QUALITY_RED" => ' '
					); 
		}		
	}
	

	for($i = 0; $i < count($arr) ; $i++) {
		if($arr[$i]["BUSINESS_LINE"] == '  '){
			$arrFinal[0] = $arr[$i];
			$arrFinal[0]["BUSINESS_LINE"] = $countAll;
		}else if($arr[$i]["BUSINESS_LINE"] == '999'){
			$arrFinal[1] = $arr[$i];
		}else if($arr[$i]["BUSINESS_LINE"] == 'MS'){
			$arrFinal[2] = $arr[$i];
		}else if($arr[$i]["BUSINESS_LINE"] == 'NI'){
			$arrFinal[3] = $arr[$i];
		}else if($arr[$i]["BUSINESS_LINE"] == 'SI'){
			$arrFinal[4] = $arr[$i];
		}else if($arr[$i]["BUSINESS_LINE"] == 'NPO'){
			$arrFinal[5] = $arr[$i];
		}else if($arr[$i]["BUSINESS_LINE"] == 'MULTI BL'){
			$arrFinal[6] = $arr[$i];
		}else if($arr[$i]["BUSINESS_LINE"] == 'Total'){
			$arrFinal[7] = $arr[$i];
		}
	}

	//print_r($arrFinal);
	ksort($arrFinal);
	//print_r($arrFinal);
	echo json_encode($arrFinal);

?>