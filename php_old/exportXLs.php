<?php

// include '_exportSQL.php';

include '/srv/data203386/sftp/jail/ftp/gnoc/mssql_auth/mssql_config.php';

include "PHPExcel/Classes/PHPExcel/IOFactory.php";


$upi = $_SESSION['GOV_UPI'];
$view = $_GET["view"];
$date=$_GET["date"];
$region = $_GET["region"];

$heading = array('Work Packages on which GDC has actually worked (till P12)','UCELLNAME','NODEB_NAME','SECTEUR','CONSTRUCTEUR','ZONE_UU','REGION' ,'DEPARTEMENT','ETAT','DATE_PREMIERE_ALERTE','LISTE_ALERTEURS',
				 'PRIORITE','CRITICITE','CRITICITE_MAX','AVANT_DERNIERE_ACTION','DERNIERE_ACTION','AVANT_DERNIERE_DATE_MODIF','DERNIERE_DATE_MODIF','ACQUITTEE',
				 'DATE_AQUITTEMENT','RETARD','TEA_CS','TDC_CS','COUPURES_CS','ECHEC_CS','CUMUL_ECHEC_CS','ZST','VERIF_ACQ','TYPE_SITE','TECHNO','RESOLVED','REBOND','RESPONSABLE','NOINCIDENT' );
$objPHPExcel = new PHPExcel();

$objPHPExcel->getActiveSheet()->setTitle('Export');
$rowNumber = 1;
$col = 'A';
 foreach($heading as $heading) {
       $objPHPExcel->getActiveSheet()->setCellValue($col.$rowNumber, $heading);
       $col++;
 }


  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="exp.xls"');
  header('Cache-Control: max-age=0');
  $objWriter->save('php://output');
?>