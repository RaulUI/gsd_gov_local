<?php

include '_exportSQL.php';
include '_exportArrayToExcel.php';

include '/srv/data203386/sftp/jail/ftp/gnoc/mssql_auth/mssql_config.php';

include "PHPExcel/Classes/PHPExcel/IOFactory.php";


$upi = $_SESSION['GOV_UPI'];
$view = $_GET["view"];
$date=$_GET["date"];
$region = $_GET["region"];

if($view == 'Executive Dashboard'){
	$objTpl = PHPExcel_IOFactory::load("executive_template.xlsx");
	$objTpl->setActiveSheetIndex(0);

	$mustHave = array(" ","  ","Total", "MS", "NI", "NPO", "SI", "MULTI BL");

	$sql=" 
use ReportingDBProd
		SET ANSI_NULLS ON 
		SET QUOTED_IDENTIFIER ON
		SET CONCAT_NULL_YIELDS_NULL ON
		SET ANSI_WARNINGS ON
		SET ANSI_PADDING ON

		DECLARE @calculateValues nvarchar(4000)
		DECLARE @convertValues nvarchar(4000)
		DECLARE @showExecutive1 nvarchar(4000)
		DECLARE @showExecutive2 nvarchar(4000)
		DECLARE @showExecutive3 nvarchar(4000)
		DECLARE @rawValues nvarchar(4000)

		SET @calculateValues= 
				'
					SELECT * into #tempGov FROM(
						SELECT
						PROJECT_NAME
						,CUSTOMER_NAME
						,[BUSINESS_LINE] 
						,case when PMO not like  ''Y'' then PTA_BASED_ACTUAL_DOCUMENT else PTA_BASED_ON_BC end PTA_SIGNED
						, '''' PROJ_32
						,COST_BASELINE
						, CASE 
									WHEN isNumeric(COST_BASELINE) = 0 THEN NULL
									WHEN COST_BASELINE LIKE ''0'' THEN ''-100''
									
									WHEN (isNumeric(REVISED_CBL) = 0 OR REVISED_CBL IN (''0'', ''-100'') ) 
									THEN ROUND(CAST(ESTIMATED_COST AS FLOAT)/COST_BASELINE *100,2)
									ELSE ROUND(CAST(ESTIMATED_COST AS FLOAT)/REVISED_CBL *100,2)
							 END as COST_1
						,CASE
					        WHEN TOTAL_COST IS NULL AND PLANED_COST IS NULL THEN NULL
					        WHEN TOTAL_COST IS NULL OR PLANED_COST IS NULL THEN ''-100''
					        WHEN PLANED_COST=0 THEN ''-100''
					        ELSE ROUND(CAST(TOTAL_COST AS FLOAT)/PLANED_COST *100,2)
					      END as COST_2
						, CASE 
									WHEN isNumeric(COST_BASELINE) = 0 THEN NULL
									WHEN COST_BASELINE LIKE ''0'' THEN ''-100''
									
									WHEN (isNumeric(REVISED_CBL) = 0 OR REVISED_CBL IN (''0'', ''-100'') ) 
									THEN ROUND(CAST(ESTIMATED_COST AS FLOAT)/COST_BASELINE *100,2)
									ELSE ROUND(CAST(ESTIMATED_COST AS FLOAT)/REVISED_CBL *100,2)
							 END as COST_3
						, [PROJECT_START_DATE] as TIME_1
						, [PROJECT_END_DATE] as TIME_2
						, CASE WHEN BUSINESS_LINE LIKE ''MS'' THEN ''100''
							WHEN DATEDIFF(DD,[ACTUAL_START_DATE], [PROJECT_END_DATE] )=0 THEN NULL
							ELSE
							(CAST((DATEDIFF(dd, [ACTUAL_START_DATE], [ESTIMATED_COMPLETITION_DATE]) * 1.00 / DATEDIFF(dd, [ACTUAL_START_DATE], [PROJECT_END_DATE]) * 100) as nvarchar)) 
							END
						as TIME_3
		       		  ,CASE
				        WHEN BUSINESS_LINE=''MS'' THEN ''100''
				        WHEN BUSINESS_LINE=''SI'' THEN 
				          CASE
				            WHEN [ACTUAL_MILESTONES] IS NULL AND PLANNED_MILESTONES IS NULL THEN NULL
				            WHEN [ACTUAL_MILESTONES]=''-100'' AND PLANNED_MILESTONES=''-100'' THEN NULL
				            WHEN PLANNED_MILESTONES=0 THEN NULL
				            ELSE CAST(CAST(CAST(ACTUAL_MILESTONES AS FLOAT)/PLANNED_MILESTONES *100 AS DECIMAL(15,2)) AS VARCHAR)
				          END
				        ELSE
				          CASE
				            WHEN [ACTUAL_SITE_TILL_P12] IS NULL AND PLANNED_SITE_TILL_P12 IS NULL THEN NULL
				            WHEN [ACTUAL_SITE_TILL_P12]=''-100'' AND PLANNED_SITE_TILL_P12=''-100'' THEN NULL
				            WHEN PLANNED_SITE_TILL_P12=0 THEN NULL
				            ELSE CAST(CAST(CAST(ACTUAL_SITE_TILL_P12 AS FLOAT)/PLANNED_SITE_TILL_P12 *100 AS DECIMAL(15,2)) AS VARCHAR)
				          END
				      END AS TIME_4	
						, [NR_CHANGES_REQUEST_APPROVED] SCOPE_1
						, [COMPETENCE_ADHERENCE] SCOPE_2
						,CASE
					        WHEN TOTAL_WORK_PACKAGES_HAS_TO_WORK IS NULL AND TOTAL_WORK_PACKAGES_ACTUAL_WORKED IS NULL THEN NULL
					        WHEN TOTAL_WORK_PACKAGES_HAS_TO_WORK IS NULL OR TOTAL_WORK_PACKAGES_ACTUAL_WORKED IS NULL THEN NULL
					        WHEN TOTAL_WORK_PACKAGES_ACTUAL_WORKED=0 THEN ''-100''
							WHEN TOTAL_WORK_PACKAGES_HAS_TO_WORK LIKE ''-100'' OR TOTAL_WORK_PACKAGES_ACTUAL_WORKED LIKE ''-100'' THEN NULL
					        ELSE ROUND(CAST(TOTAL_WORK_PACKAGES_HAS_TO_WORK AS FLOAT)/TOTAL_WORK_PACKAGES_ACTUAL_WORKED * 100,2)
					      END SCOPE_3
						, case when PMO not like  ''Y'' then PTA_BASED_ACTUAL_DOCUMENT else PTA_BASED_ON_BC end as QUALITY_1
						, [SOLUTION_DOCUMENT] as QUALITY_2
						, [VIRTUAL_ZERO] as QUALITY_3
						, CASE WHEN [CUSTOMER_TEAM_SURVEY] LIKE ''-100'' THEN ''NA'' ELSE NULL END QUALITY_4 
						, ''-'' OVR_STS
						FROM ReportingDBProd.[gsd].[RAW_DATA] 
						WHERE BUSINESS_LINE IS NOT NULL
						AND YEAR_MONTH LIKE ''$date''
						AND MARKET LIKE ''$region''
					)f
					
					'

		SET @convertValues = '
					select 
						 PROJECT_NAME
						,CUSTOMER_NAME
						,BUSINESS_LINE
						,PTA_SIGNED
						,PROJ_32
						,[COST_BASELINE]
						,CASE WHEN COST_1 IS NOT NULL THEN 0 ELSE 2 END COST_1
						,CASE WHEN COST_2 IS NOT NULL THEN
							 CASE WHEN CAST(COST_2 as float) >= 90 AND CAST(COST_2 as float) <= 110 
									THEN 0 
							  ELSE 
									CASE WHEN (CAST(COST_2 as float) >= 80 AND CAST(COST_2 as float) < 90) OR (CAST(COST_2 as float) >= 111 AND CAST(COST_2 as float) <= 120)
									THEN 1
									ELSE
										2
									END
							  END 
						  ELSE NULL END
						  COST_2
						,
						CASE WHEN COST_3 IS NOT NULL THEN
							CASE WHEN CAST(COST_3 as float) > 90 AND CAST(COST_3 as float) < 110 
								THEN 0
							ELSE
								CASE WHEN (CAST(COST_3 as float) >= 80 AND CAST(COST_3 as float) <= 90) OR (CAST(COST_3 as float) >= 111 AND CAST(COST_3 as float) <= 120)
								THEN 1
								ELSE 
									2
								END
							END 
						ELSE NULL END
						COST_3
					   ,CASE WHEN TIME_1 IS NOT NULL THEN 0 ELSE 2 END TIME_1
					   ,CASE WHEN TIME_2 IS NOT NULL THEN 0 ELSE 2 END TIME_2
					   ,CASE WHEN TIME_3 IS NOT NULL THEN
						   CASE WHEN CAST(TIME_3 as float) > 90 AND CAST(TIME_3 as float) < 110 
								THEN 0
							ELSE
								CASE WHEN (CAST(TIME_3 as float) >= 80 AND CAST(TIME_3 as float) <= 90) OR (CAST(TIME_3 as float) >= 111 AND CAST(TIME_3 as float) <= 120)
								THEN 1
								ELSE 
									2
								END
							END 
						ELSE ''2'' END
						TIME_3
						,
						CASE WHEN TIME_4 IS NOT NULL THEN
							CASE WHEN CAST(TIME_4 as float) > 90 AND CAST(TIME_4 as float) < 110 
								THEN 0
							ELSE
								CASE WHEN (CAST(TIME_4 as float) >= 80 AND CAST(TIME_4 as float) <= 90) OR (CAST(TIME_4 as float) >= 111 AND CAST(TIME_4 as float) <= 120)
								THEN 1
								ELSE 
									2
								END
							END
						ELSE ''2'' END 
						TIME_4
						,CASE WHEN SCOPE_1 > 0 THEN 0 ELSE CASE WHEN SCOPE_1 = 0 THEN 2 ELSE NULL END END SCOPE_1
						,CASE WHEN SCOPE_2 LIKE ''Y'' THEN 0 ELSE CASE WHEN SCOPE_2 LIKE ''N'' THEN 2 ELSE NULL END END SCOPE_2
						,
						CASE WHEN SCOPE_3 IS NOT NULL THEN
							CASE WHEN CAST(SCOPE_3 as float) > 90 AND CAST(SCOPE_3 as float) < 110 
								THEN 0
							ELSE
								CASE WHEN CAST(SCOPE_3 as float) > 80 AND CAST(SCOPE_3 as float) <= 90
								THEN 1
								ELSE 2
								END
							END 
						ELSE NULL END
						SCOPE_3
						,CASE WHEN QUALITY_1 LIKE ''Y'' THEN 0 ELSE CASE WHEN QUALITY_1 LIKE ''N'' THEN 2 ELSE NULL END END QUALITY_1
						,CASE WHEN QUALITY_2 LIKE ''Y'' THEN 0 ELSE CASE WHEN QUALITY_2 LIKE ''N'' THEN 2 ELSE NULL END END QUALITY_2
						,
						CASE WHEN QUALITY_3 IS NOT NULL THEN
							CASE WHEN CAST(QUALITY_3 as float) > 93 
								THEN 0
							ELSE
								CASE WHEN (CAST(QUALITY_3 as float) >= 83.7 AND CAST(QUALITY_3 as float) <= 93) 
								THEN 1
								ELSE 
									2
								END
							END 
						ELSE NULL END
						QUALITY_3
						,
						CASE WHEN QUALITY_4 IS NOT NULL THEN
						  CASE WHEN QUALITY_4 LIKE ''NA'' THEN ''0'' ELSE
							CASE WHEN CAST(QUALITY_4 as float) > 8.2 
								THEN 0
							ELSE
								CASE WHEN (CAST(QUALITY_4 as float) >= 7.38 AND CAST(QUALITY_4 as float) <= 8.2) 
								THEN 1
								ELSE 
									2
								END
							END 
						 END
						ELSE NULL END
						QUALITY_4
					into #tempGovRaw
					from #tempGov	

				'

		SET @rawValues = '
			SELECT PROJECT_NAME
				,CUSTOMER_NAME
				,BUSINESS_LINE
				,PTA_SIGNED
				,PROJ_32
				, case when CAST(COST_BASELINE AS FLOAT) < 100 then ''100'' else 
					case when CAST(COST_BASELINE AS FLOAT) <= 300 AND CAST(COST_BASELINE AS FLOAT) > 100 then ''1_3'' else
						case when CAST(COST_BASELINE AS FLOAT) <= 500 AND CAST(COST_BASELINE AS FLOAT) > 300 then ''3_5'' else 
							case when CAST(COST_BASELINE AS FLOAT) > 500 then ''500'' else ''Wrong'' END
						END
					END
				END GDC_COST
				,CASE WHEN COST_1 IS NOT NULL OR COST_2 IS NOT NULL OR COST_3 IS NOT NULL THEN
					CASE WHEN COST_1 = 2 OR COST_2 = 2 OR COST_3 = 2 THEN ''RED'' 
						ELSE CASE WHEN COST_1 = 1 OR COST_2 = 1 OR COST_3 = 1 THEN ''AMBER''
						ELSE ''GREEN''
					END
				END
				ELSE '''' 
				END
				COST_COLOR
				,CASE WHEN TIME_1 IS NOT NULL AND TIME_2 IS NOT NULL AND TIME_3 IS NOT NULL AND TIME_4 IS NOT NULL THEN
				  CASE WHEN TIME_1 = 2 OR TIME_2 = 2 OR TIME_3 = 2 OR TIME_4 = 2 THEN ''RED'' 
					ELSE 
						CASE WHEN TIME_1 = 1 OR TIME_2 = 1 OR TIME_3 = 1 OR TIME_4 = 1 THEN ''AMBER''
						ELSE ''GREEN'' 
						END
					END 
				ELSE '''' END
				TIME_COLOR
				,CASE WHEN SCOPE_1 IS NOT NULL AND SCOPE_2 IS NOT NULL AND SCOPE_3 IS NOT NULL THEN
				  CASE WHEN SCOPE_1 = 2 OR SCOPE_2 = 2 OR SCOPE_3 = 2 THEN ''RED'' 
					ELSE 
						CASE WHEN SCOPE_1 = 1 OR SCOPE_2 = 1 OR SCOPE_3 = 1 THEN ''AMBER''
						ELSE ''GREEN'' 
						END
					END 
				 ELSE ''RED'' END
				SCOPE_COLOR
				,CASE WHEN QUALITY_1 IS NOT NULL AND QUALITY_2 IS NOT NULL AND QUALITY_3 IS NOT NULL AND QUALITY_4 IS NOT NULL THEN
				  CASE WHEN QUALITY_1 = 2 OR QUALITY_2 = 2 OR QUALITY_3 = 2 OR QUALITY_4 = 2 THEN ''RED'' 
					ELSE 
						CASE WHEN QUALITY_1 = 1 OR QUALITY_2 = 1 OR QUALITY_3 = 1 OR QUALITY_4 = 1 THEN ''AMBER''
						ELSE ''GREEN'' 
						END
					END 
				 ELSE ''RED'' END
				QUALITY_COLOR
				,(SELECT ''$upi'') AS UPI
			INTO #finalRaw
			FROM #tempGovRaw

			DELETE FROM [ReportingDBProd].[GSD].[DRILL_DOWN_DATA] WHERE UPI LIKE ''$upi''
			INSERT INTO [ReportingDBProd].[GSD].[DRILL_DOWN_DATA] SELECT * FROM #finalRaw
					
		'
		SET @showExecutive1 = 
				'
				SELECT 
					''  ''
					as BUSINESS_LINE,
					SUM(PROJ_EXCL_CARE)PROJ_EXCL_CARE,
					SUM(PTA_SIGNED)PTA_SIGNED,
					''''PROJ_32, --SUM(PROJ_32)PROJ_32,
					SUM(GDC_100)GDC_100,
					SUM(GDC_1_3)GDC_1_3,
					SUM(GDC_3_5)GDC_3_5,
					SUM(GDC_500)GDC_500,
					SUM(COST_GREEN)COST_GREEN,
					SUM(COST_AMBER)COST_AMBER,
					SUM(COST_RED)COST_RED,
					SUM(TIME_GREEN)TIME_GREEN,
					SUM(TIME_AMBER)TIME_AMBER,
					SUM(TIME_RED)TIME_RED,
					SUM(SCOPE_GREEN)SCOPE_GREEN,
					SUM(SCOPE_AMBER)SCOPE_AMBER,
					SUM(SCOPE_RED)SCOPE_RED,
					SUM(QUALITY_GREEN)QUALITY_GREEN,
					SUM(QUALITY_AMBER)QUALITY_AMBER,
					SUM(QUALITY_RED) QUALITY_RED
					FROM (
							SELECT
								COUNT(BUSINESS_LINE)PROJ_EXCL_CARE
								,SUM(CASE WHEN PTA_SIGNED LIKE ''Y'' THEN 1 ELSE 0 END)PTA_SIGNED
								,PROJ_32
								,SUM(CASE WHEN GDC_COST LIKE ''100'' THEN 1 ELSE 0 END)GDC_100
								,SUM(CASE WHEN GDC_COST LIKE ''1_3'' THEN 1 ELSE 0 END)GDC_1_3
								,SUM(CASE WHEN GDC_COST LIKE ''3_5'' THEN 1 ELSE 0 END)GDC_3_5
								,SUM(CASE WHEN GDC_COST LIKE ''500'' THEN 1 ELSE 0 END)GDC_500
								,SUM(CASE WHEN COST_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)COST_GREEN
								,SUM(CASE WHEN COST_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)COST_AMBER
								,SUM(CASE WHEN COST_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)COST_RED
								,SUM(CASE WHEN TIME_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)TIME_GREEN
								,SUM(CASE WHEN TIME_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)TIME_AMBER
								,SUM(CASE WHEN TIME_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)TIME_RED
								,SUM(CASE WHEN SCOPE_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)SCOPE_GREEN
								,SUM(CASE WHEN SCOPE_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)SCOPE_AMBER
								,SUM(CASE WHEN SCOPE_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)SCOPE_RED
								,SUM(CASE WHEN QUALITY_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)QUALITY_GREEN
								,SUM(CASE WHEN QUALITY_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)QUALITY_AMBER
								,SUM(CASE WHEN QUALITY_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)QUALITY_RED
							FROM
							#finalRaw
							GROUP BY BUSINESS_LINE, PROJ_32
					)fff
				UNION
		'

		SET @showExecutive3 ='
				SELECT
				''999'',999,999,999,999,999,999,999,999,999,999,999,999,999,999,999,999,999,999,999
				UNION
		'

		SET @showExecutive2 = '
				SELECT 
					BUSINESS_LINE
					,COUNT(BUSINESS_LINE)PROJ_EXCL_CARE
					,SUM(CASE WHEN PTA_SIGNED LIKE ''Y'' THEN 1 ELSE 0 END)PTA_SIGNED
					,PROJ_32
					,SUM(CASE WHEN GDC_COST LIKE ''100'' THEN 1 ELSE 0 END)GDC_100
					,SUM(CASE WHEN GDC_COST LIKE ''1_3'' THEN 1 ELSE 0 END)GDC_1_3
					,SUM(CASE WHEN GDC_COST LIKE ''3_5'' THEN 1 ELSE 0 END)GDC_3_5
					,SUM(CASE WHEN GDC_COST LIKE ''500'' THEN 1 ELSE 0 END)GDC_500
					,SUM(CASE WHEN COST_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)COST_GREEN
					,SUM(CASE WHEN COST_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)COST_AMBER
					,SUM(CASE WHEN COST_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)COST_RED
					,SUM(CASE WHEN TIME_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)TIME_GREEN
					,SUM(CASE WHEN TIME_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)TIME_AMBER
					,SUM(CASE WHEN TIME_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)TIME_RED
					,SUM(CASE WHEN SCOPE_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)SCOPE_GREEN
					,SUM(CASE WHEN SCOPE_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)SCOPE_AMBER
					,SUM(CASE WHEN SCOPE_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)SCOPE_RED
					,SUM(CASE WHEN QUALITY_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)QUALITY_GREEN
					,SUM(CASE WHEN QUALITY_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)QUALITY_AMBER
					,SUM(CASE WHEN QUALITY_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)QUALITY_RED
				FROM
				#finalRaw
				GROUP BY BUSINESS_LINE, PROJ_32
				UNION 
				SELECT 
					''Total'',
					SUM(PROJ_EXCL_CARE),
					SUM(PTA_SIGNED),
					'''', --SUM(PROJ_32),
					SUM(GDC_100),
					SUM(GDC_1_3),
					SUM(GDC_3_5),
					SUM(GDC_500),
					SUM(COST_GREEN),
					SUM(COST_AMBER),
					SUM(COST_RED),
					SUM(TIME_GREEN),
					SUM(TIME_AMBER),
					SUM(TIME_RED),
					SUM(SCOPE_GREEN),
					SUM(SCOPE_AMBER),
					SUM(SCOPE_RED),
					SUM(QUALITY_GREEN),
					SUM(QUALITY_AMBER),
					SUM(QUALITY_RED) 
					FROM (
							SELECT
								COUNT(BUSINESS_LINE)PROJ_EXCL_CARE
								,SUM(CASE WHEN PTA_SIGNED LIKE ''Y'' THEN 1 ELSE 0 END)PTA_SIGNED
								,PROJ_32
								,SUM(CASE WHEN GDC_COST LIKE ''100'' THEN 1 ELSE 0 END)GDC_100
								,SUM(CASE WHEN GDC_COST LIKE ''1_3'' THEN 1 ELSE 0 END)GDC_1_3
								,SUM(CASE WHEN GDC_COST LIKE ''3_5'' THEN 1 ELSE 0 END)GDC_3_5
								,SUM(CASE WHEN GDC_COST LIKE ''500'' THEN 1 ELSE 0 END)GDC_500
								,SUM(CASE WHEN COST_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)COST_GREEN
								,SUM(CASE WHEN COST_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)COST_AMBER
								,SUM(CASE WHEN COST_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)COST_RED
								,SUM(CASE WHEN TIME_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)TIME_GREEN
								,SUM(CASE WHEN TIME_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)TIME_AMBER
								,SUM(CASE WHEN TIME_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)TIME_RED
								,SUM(CASE WHEN SCOPE_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)SCOPE_GREEN
								,SUM(CASE WHEN SCOPE_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)SCOPE_AMBER
								,SUM(CASE WHEN SCOPE_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)SCOPE_RED
								,SUM(CASE WHEN QUALITY_COLOR LIKE ''GREEN'' THEN 1 ELSE 0 END)QUALITY_GREEN
								,SUM(CASE WHEN QUALITY_COLOR LIKE ''AMBER'' THEN 1 ELSE 0 END)QUALITY_AMBER
								,SUM(CASE WHEN QUALITY_COLOR LIKE ''RED'' THEN 1 ELSE 0 END)QUALITY_RED
							FROM
							#finalRaw
							GROUP BY BUSINESS_LINE, PROJ_32
					)ff

				DROP TABLE #finalRaw
				drop table #tempGov
				drop table #tempGovRaw

				'

		execute (@calculateValues + @convertValues + @rawValues + @showExecutive1 + @showExecutive3 +@showExecutive2)
	
	 ";

die($sql);
$rs=mssql_query($sql);
$month=explode('-',$date);
$objTpl->getActiveSheet()->setCellValue('B2','Executive Dashboard - '.$region.' (P'.$month[1].')');

  if (!$rs){
	exit('MSSQL error: ' . mssql_get_last_message());
  }	
  $i=5;
  while ($row=mssql_fetch_array($rs))
	{
		if($i<13)
		{
			$j=0;
			if($row['BUSINESS_LINE'] == '  ')
				$j = 5;
			if($row['BUSINESS_LINE'] == 'MS')
				$j = 7;
			if($row['BUSINESS_LINE'] == 'NI')
				$j = 8;
			if($row['BUSINESS_LINE'] == 'SI')
				$j = 9;
			if($row['BUSINESS_LINE'] == 'NPO')
				$j = 10;
			if($row['BUSINESS_LINE'] == 'Multi BL')
				$j = 11;
			if($row['BUSINESS_LINE'] == 'Total')
				$j = 12;
			if($j != 0)
			{
				$objTpl->getActiveSheet()->setCellValue('C'.$j,$row['PROJ_EXCL_CARE']);
				$objTpl->getActiveSheet()->setCellValue('D'.$j,$row['PTA_SIGNED']);
				$objTpl->getActiveSheet()->setCellValue('E'.$j,$row['PROJ_32']);
				$objTpl->getActiveSheet()->setCellValue('F'.$j,$row['GDC_100']);
				$objTpl->getActiveSheet()->setCellValue('G'.$j,$row['GDC_1_3']);
				$objTpl->getActiveSheet()->setCellValue('H'.$j,$row['GDC_3_5']);
				$objTpl->getActiveSheet()->setCellValue('I'.$j,$row['GDC_500']);
				$objTpl->getActiveSheet()->setCellValue('J'.$j,$row['COST_RED']);
				$objTpl->getActiveSheet()->setCellValue('K'.$j,$row['COST_AMBER']);
				$objTpl->getActiveSheet()->setCellValue('L'.$j,$row['COST_GREEN']);
				$objTpl->getActiveSheet()->setCellValue('M'.$j,$row['TIME_RED']);
				$objTpl->getActiveSheet()->setCellValue('N'.$j,$row['TIME_AMBER']);
				$objTpl->getActiveSheet()->setCellValue('O'.$j,$row['TIME_GREEN']);
				$objTpl->getActiveSheet()->setCellValue('P'.$j,$row['SCOPE_RED']);
				$objTpl->getActiveSheet()->setCellValue('Q'.$j,$row['SCOPE_AMBER']);
				$objTpl->getActiveSheet()->setCellValue('R'.$j,$row['SCOPE_GREEN']);
				$objTpl->getActiveSheet()->setCellValue('S'.$j,$row['QUALITY_RED']);
				$objTpl->getActiveSheet()->setCellValue('T'.$j,$row['QUALITY_AMBER']);
				$objTpl->getActiveSheet()->setCellValue('U'.$j,$row['QUALITY_GREEN']);
			}
			$i++;
		}
	//$nr_supp_ev=$nr_supp_ev+1;
  }	
	$filename='GSDM_Executive_Dashboard.xls'; 

	$objWriter = PHPExcel_IOFactory::createWriter($objTpl, 'Excel5');
	header('Content-Type: application/vnd.ms-excel; charset: utf-8');
	header('Content-Disposition: attachment;filename="export-market.xls"');
	header('Cache-Control: max-age=0');
	$objWriter->save('php://output');

}else if ($view == 'Project View'){

	$excel = new PHPExcel();

$green = array(
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '32CD32')
	)
);

$red = array(
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'E62D2D')
	)
);

$yellow = array(
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'FFFF00')
	)
);


	$sql = "
				USE ReportingDBProd
			DECLARE @QUERY1A VARCHAR(4000)
			SET @QUERY1A='
			SET ANSI_NULLS ON 
			SET QUOTED_IDENTIFIER ON
			SET CONCAT_NULL_YIELDS_NULL ON
			SET ANSI_WARNINGS ON
			SET ANSI_PADDING ON
			SELECT 
			
IDX AS Nr,
PROJECT_NAME AS [Project Name],
COST_CBL_REVISED AS [CBL /Revised CBL],
COST_PCA_ACTUAL AS [PCA (Actual)],
COST_PCA_EAC AS [PCA (EAC)],
TIME_START AS [Start Date],
TIME_END AS [End Date],
TIME_SCHEDULE AS [Schedule Adherence (EAC)],
TIME_PROJECT,
BUSINESS_LINE AS BLs,
SCOPE_CHANGE AS [No.of Change Requests],
SCOPE_COMPETENCE AS [Competence Adherence (Against)],
SCOPE_REMOTE AS [Remote Service Catalog],
QUALITY_PTA AS [PTA Sign Off],
QUALITY_SSD AS [SSD Solution Docu Avbl],
QUALITY_VIRTUAL AS [Virtual Zero Delivery Index (n-1)],
QUALITY_CUSTOMER AS [Customer Team Survey (CTS)],
OVERALL_PROJ_STS AS [Overall Project Status],
WBS_CODE AS [WBS Code],
SWO_CODE AS [SWO Code],
GIC_CODE AS [GIC Code],
NAME_OF_GDM AS [Name of GDM],
NAME_OF_PM AS [Name of PM],
MARKET,
DELIVERY_CENTER AS [Delivery Center]
			FROM(
			SELECT *,'+(SELECT FORMULA FROM [gsd].MAPPING_COLOR WHERE KPI_NAME='COST_PCA_ACTUAL')+'
			,'+(SELECT FORMULA FROM [gsd].MAPPING_COLOR WHERE KPI_NAME='COST_PCA_EAC')+'
			,'+(SELECT FORMULA FROM [gsd].MAPPING_COLOR WHERE KPI_NAME='TIME_PROJECT')+'
			,'+(SELECT FORMULA FROM [gsd].MAPPING_COLOR WHERE KPI_NAME='SCOPE_CHANGE')+'
			,'+(SELECT FORMULA FROM [gsd].MAPPING_COLOR WHERE KPI_NAME='SCOPE_REMOTE')+'
			,'+(SELECT FORMULA FROM [gsd].MAPPING_COLOR WHERE KPI_NAME='QUALITY_VIRTUAL')+'
			,'+(SELECT FORMULA FROM [gsd].MAPPING_COLOR WHERE KPI_NAME='QUALITY_CUSTOMER')+' '
			DECLARE @QUERY1B VARCHAR(4000)
			SET @QUERY1B='FROM(SELECT [IDX]
		      ,[PROJECT_NAME]
		      ,cast([COST_BASELINE] as nvarchar)+ ''/'' + cast([REVISED_CBL] as nvarchar) COST_CBL_REVISED
		      ,CASE
		        WHEN TOTAL_COST IS NULL AND PLANED_COST IS NULL THEN NULL
		        WHEN TOTAL_COST IS NULL OR PLANED_COST IS NULL THEN ''-100''
		        WHEN PLANED_COST=0 THEN ''-100''
		        WHEN PLANED_COST=''-100'' OR TOTAL_COST=''-100'' THEN ''-100''
		        ELSE ROUND(CAST(TOTAL_COST AS FLOAT)/PLANED_COST *100,2)
		      END AS COST_PCA_ACTUAL
				 ,CASE
			        WHEN ESTIMATED_COST IS NULL AND REVISED_CBL IS NULL THEN NULL
			        WHEN ESTIMATED_COST IS NULL OR REVISED_CBL IS NULL THEN ''-100''
			        WHEN REVISED_CBL=0 THEN ''-100''
			        WHEN REVISED_CBL=''-100'' OR REVISED_CBL IS NULL THEN 
			          CASE
			            WHEN COST_BASELINE=''-100'' THEN ''-100''
			            ELSE ROUND(CAST(ESTIMATED_COST AS FLOAT)/COST_BASELINE *100,2)
			          END
			        ELSE ROUND(CAST(ESTIMATED_COST AS FLOAT)/REVISED_CBL *100,2)
			      END AS COST_PCA_EAC	
		      ,[PROJECT_START_DATE] as TIME_START
		      ,[PROJECT_END_DATE] as TIME_END	   
        ,CASE
        WHEN [ACTUAL_START_DATE] IS NULL AND ESTIMATED_COMPLETITION_DATE IS NULL AND [PROJECT_END_DATE] IS NULL THEN NULL
        WHEN DATEDIFF(DD,[ACTUAL_START_DATE], [PROJECT_END_DATE] )=0 THEN NULL
        ELSE CAST(CAST(CAST(DATEDIFF(DD,[ACTUAL_START_DATE],[ESTIMATED_COMPLETITION_DATE]) AS FLOAT)/DATEDIFF(DD,[ACTUAL_START_DATE], [PROJECT_END_DATE] ) *100 AS DECIMAL(15,2)) AS VARCHAR)
      END AS TIME_SCHEDULE
		      '
   		  DECLARE @QUERY2 VARCHAR(4000)
			SET @QUERY2=',CASE
		        WHEN BUSINESS_LINE=''MS'' THEN ''100''
		        WHEN BUSINESS_LINE=''SI'' THEN 
		          CASE
		            WHEN [ACTUAL_MILESTONES] IS NULL AND PLANNED_MILESTONES IS NULL THEN NULL
		            WHEN [ACTUAL_MILESTONES]=''-100'' AND PLANNED_MILESTONES=''-100'' THEN ''-100''
		            WHEN PLANNED_MILESTONES=0 OR PLANNED_MILESTONES IS NULL THEN ''-100''
		            ELSE ROUND(CAST(ACTUAL_MILESTONES AS FLOAT)/PLANNED_MILESTONES *100,2)
		          END
		        ELSE
		          CASE
		            WHEN [ACTUAL_SITE_TILL_P12] IS NULL AND PLANNED_SITE_TILL_P12 IS NULL THEN NULL
		            WHEN [ACTUAL_SITE_TILL_P12]=''-100'' AND PLANNED_SITE_TILL_P12=''-100'' THEN ''-100''
		             WHEN PLANNED_SITE_TILL_P12=0 OR PLANNED_SITE_TILL_P12 IS NULL THEN ''-100''
		            ELSE ROUND(CAST(PLANNED_SITE_TILL_P12 AS FLOAT)/PLANNED_SITE_TILL_P12 *100,2)
		          END
		      END AS TIME_PROJECT
		      
			  ,[BUSINESS_LINE] as BUSINESS_LINE
			  ,[NR_CHANGES_REQUEST_APPROVED] as SCOPE_CHANGE
			  ,[COMPETENCE_ADHERENCE] as SCOPE_COMPETENCE
			  ,CASE
		        WHEN TOTAL_WORK_PACKAGES_HAS_TO_WORK IS NULL AND TOTAL_WORK_PACKAGES_ACTUAL_WORKED IS NULL THEN NULL
		        WHEN TOTAL_WORK_PACKAGES_HAS_TO_WORK IS NULL OR TOTAL_WORK_PACKAGES_ACTUAL_WORKED IS NULL THEN ''-100''
		        WHEN TOTAL_WORK_PACKAGES_ACTUAL_WORKED=0 THEN ''-100''
		        ELSE ROUND(CAST(TOTAL_WORK_PACKAGES_HAS_TO_WORK AS FLOAT)/TOTAL_WORK_PACKAGES_ACTUAL_WORKED * 100,2)
		      END AS SCOPE_REMOTE
		      ,case when PMO not like  ''Y'' then PTA_BASED_ACTUAL_DOCUMENT else PTA_BASED_ON_BC end AS QUALITY_PTA
			  ,CASE
			  	WHEN [SOLUTION_DOCUMENT]=''-100'' THEN ''NA'' 
			  	ELSE SOLUTION_DOCUMENT
			  	END as QUALITY_SSD
			  ,[VIRTUAL_ZERO] as QUALITY_VIRTUAL
			  ,[CUSTOMER_TEAM_SURVEY] as QUALITY_CUSTOMER
			  , '''' as OVERALL_PROJ_STS
			  ,C.[WBS] as  WBS_CODE
			  ,D.SVO as SWO_CODE
		      ,CASE
			  	WHEN [IDX_GIC]=''-100'' THEN ''NA'' 
			  	ELSE IDX_GIC
			  	END as GIC_CODE
			  ,[GDN_NAME] as NAME_OF_GDM
		      ,[PROJECT_MANAGER] as NAME_OF_PM
		      ,[MARKET]
		      ,[DELIVERY_CENTER] as DELIVERY_CENTER
		      '
		    DECLARE @QUERY3 VARCHAR(4000)
			SET @QUERY3='
		  	FROM ReportingDBProd.[gsd].[RAW_DATA] A
		    LEFT JOIN (SELECT DISTINCT r.IDX_COUNTRY_CUSTOMER,
			 STUFF((SELECT distinct '',''+ a.COUNTRY
		               FROM [ReportingDBProd].[gsd].[MAPPING_COUNTRY] a
		             WHERE r.IDX_COUNTRY_CUSTOMER = a.IDX_COUNTRY_CUSTOMER
		            FOR XML PATH(''''), TYPE).value(''.'',''VARCHAR(max)''), 1, 1, '''')  AS [COUNTRY_OF_CUSTOMER]
				FROM ReportingDBProd.[gsd].[MAPPING_COUNTRY] r) B 
				ON A.IDX=B.IDX_COUNTRY_CUSTOMER
			LEFT JOIN (SELECT DISTINCT r.IDX_WBS,
			 STUFF((SELECT distinct '',''+ a.WBS
		               FROM [ReportingDBProd].[gsd].[MAPPING_WBS] a
		             WHERE r.IDX_WBS = a.IDX_WBS
		            FOR XML PATH(''''), TYPE).value(''.'',''VARCHAR(max)''), 1, 1, '''')  AS [WBS]
				FROM ReportingDBProd.[gsd].[MAPPING_WBS] r) C
				ON A.IDX=C.IDX_WBS
			LEFT JOIN (SELECT DISTINCT r.IDX_SVO,
			 STUFF((SELECT distinct '',''+ a.SVO
		               FROM [ReportingDBProd].[gsd].[MAPPING_SVO] a
		             WHERE r.IDX_SVO = a.IDX_SVO
		            FOR XML PATH(''''), TYPE).value(''.'',''VARCHAR(max)''), 1, 1, '''')  AS [SVO]
				FROM ReportingDBProd.[gsd].[MAPPING_SVO] r) D
				ON A.IDX=D.IDX_SVO
			WHERE YEAR_MONTH=''$date'' AND MARKET LIKE ''$region'' 
			) AS FINAL
			) AS FINAL
			ORDER BY IDX DESC'
			EXECUTE (@QUERY1A+@QUERY1B+@QUERY2+@QUERY3)
			";
$row = array(
'Nr',
'Project Name',
'CBL /Revised CBL',
'PCA (Actual)',
'PCA (EAC)',
'Start Date',
'End Date',
'Schedule Adherence (EAC)',
'TIME_PROJECT',
'BLs',
'No.of Change Requests',
'Competence Adherence (Against)',
'Remote Service Catalog',
'PTA Sign Off',
'SSD Solution Docu Avbl',
'Virtual Zero Delivery Index (n-1)',
'Customer Team Survey (CTS)',
'Overall Project Status',
'WBS Code',
'SWO Code',
'GIC Code',
'Name of GDM',
'Name of PM',
'MARKET',
'Delivery Center'

);
$arr[] = $row;
$data = mssql_query($sql);

	while ($row = mssql_fetch_assoc($data)) {
		
	    foreach ($row as $key => $value) {
	        //$row[$key] = utf8_encode($value);
			
			
			if (utf8_encode($value)=='-100'){
				 $row[$key] = 'NA';
			}else{
				 $row[$key] = utf8_encode($value);
			}
			
	    }
	    $arr[] = $row;

	}


$doc = new PHPExcel();
$doc->setActiveSheetIndex(0);

// $rows = $doc->getActiveSheet()->fromArray(array_keys($arr[0]),NULL,'A2');

$doc->getActiveSheet()->fromArray($arr, null, 'A1');

foreach(range('A','AR') as $columnID) {

    $doc->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);

}
$style = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    )
);

$doc->getDefaultStyle()->applyFromArray($style);
$doc->getActiveSheet()->getStyle('1:1')->getFont()->setBold(true);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Export Project Data.xls"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$writer = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

$writer->save('php://output');
	//include '../../_libs/php/Utilities/_exportSQL.php';
	//die($sql);
	exportSQL($sql, 'Export Project Dashboard', 'Sheet1');
	// $rs = mssql_query($sql);
	// $nf = mssql_num_fields($rs);
	// 	for($i=1 ; $row = mssql_fetch_row($rs) ; ++$i){
	//     	for( $j = 0, $column='B' ; $j<$nf ; ++$j){
	//     		if($column == 'C' || $column == 'F' || $column == 'G'){
	//     			if($row[$j]){
	//     				$sheet->getStyle($column.$i)->applyFromArray($green);
	//     				$sheet->setCellValue($column.$i, $row[$j]);
	//     			}else{
	//     				$sheet->getStyle($column.$i)->applyFromArray($red);
	//     				$sheet->setCellValue($column.$i, $row[$j]);
	//     			}
	//     		}
	//     		if($column == 'D' || $column == 'E' || $column == 'H' || $column == 'I'){
	//     			if(strtok($row[$j], '%')>90 && strtok($row[$j], '%')<110){ 
	//     				$sheet->getStyle($column.$i)->applyFromArray($green);
	//     				$sheet->setCellValue($column.$i, $row[$j]);
	//     			}else if((strtok($row[$j], '%')>80 && strtok($row[$j], '%')<89) || ((strtok($row[$j], '%')>111 && strtok($row[$j], '%')<120))){
	//     				$sheet->getStyle($column.$i)->applyFromArray($yellow);
	//     				$sheet->setCellValue($column.$i, $row[$j]);
	//     			}else if(strtok($row[$j], '%')<80 || strtok($row[$j], '%')>121){
	//     				$sheet->getStyle($column.$i)->applyFromArray($red);
	//     				$sheet->setCellValue($column.$i, $row[$j]);
	//     			}	
	//     		}
	//     		if($column == 'K'){
	//     			if($row[$j]>0){
	//     				$sheet->getStyle($column.$i)->applyFromArray($green);
	//     				$sheet->setCellValue($column.$i, $row[$j]);
	//     			}else{
	//     				$sheet->getStyle($column.$i)->applyFromArray($red);
	//     				$sheet->setCellValue($column.$i, $row[$j]);
	//     			}	
	//     		}
	//     		if($column == 'L' || $column == 'N' || $column == 'O'){
	//     			if($row[$j] == 'Y'){
	//     				$sheet->getStyle($column.$i)->applyFromArray($green);
	//     				$sheet->setCellValue($column.$i, $row[$j]);
	//     			}else if($row[$j] == 'N'){
	//     				$sheet->getStyle($column.$i)->applyFromArray($red);
	//     				$sheet->setCellValue($column.$i, $row[$j]);
	//     			}	
	//     		}
	//     		if($column == 'M'){
	//     			if(strtok($row[$j], '%')>90 && strtok($row[$j], '%')<100){
	//     				$sheet->getStyle($column.$i)->applyFromArray($green);
	//     				$sheet->setCellValue($column.$i, $row[$j]);
	//     			}else if(strtok($row[$j], '%')>80 && strtok($row[$j], '%')<89){
	//     				$sheet->getStyle($column.$i)->applyFromArray($yellow);
	//     				$sheet->setCellValue($column.$i, $row[$j]);
	//     			}else if(strtok($row[$j], '%')<80){
	//     				$sheet->getStyle($column.$i)->applyFromArray($red);
	//     				$sheet->setCellValue($column.$i, $row[$j]);
	//     			}
	//     		}
	//     		else{
	//     			$sheet->setCellValue($column.$i, $row[$j]);
	//     		}
	//     		$column++;
	//     	}
	// 	}

	// header('Content-type: application/vnd.ms-excel');
	// header('Content-Disposition: attachment; filename="Project_Dashboard.xlsx"');

	// $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
	// $objWriter->save('php://output');
}else if($view == 'Raw Data View'){

	$sql = "SET ANSI_NULLS ON 
	SET QUOTED_IDENTIFIER ON
	SET CONCAT_NULL_YIELDS_NULL ON
	SET ANSI_WARNINGS ON
	SET ANSI_PADDING ON
	SELECT[IDX] as '#'
      ,CASE WHEN B.[COUNTRY_OF_CUSTOMER]='-100' THEN 'NA' ELSE CAST(B.COUNTRY_OF_CUSTOMER AS VARCHAR) END AS [Country Of Customer]
      ,CASE WHEN [CUSTOMER_NAME]='-100' THEN 'NA' ELSE CAST(CUSTOMER_NAME AS VARCHAR) END AS [Customer Name]
      ,CASE WHEN [PROJECT_NAME]='-100' THEN 'NA' ELSE CAST(PROJECT_NAME AS VARCHAR) END AS [Project Name]
      ,CASE WHEN [COST_BASELINE]='-100' THEN 'NA' ELSE CAST(COST_BASELINE AS VARCHAR) END AS [Cost Baseline Value in K Euro]
      ,CASE WHEN [REVISED_CBL]='-100' THEN 'NA' ELSE CAST(REVISED_CBL AS VARCHAR) END AS [Revised CBL]
      ,CASE WHEN [ESTIMATED_COST]='-100' THEN 'NA' ELSE CAST(ESTIMATED_COST AS VARCHAR) END AS [Estimated Cost at Completion EAC in K Euro]
      ,CASE WHEN [TOTAL_COST]='-100' THEN 'NA' ELSE CAST(TOTAL_COST AS VARCHAR) END AS [Total Cost (K Euro) charged Actual from F&C report base on WBS/Svo/GIC]
      ,CASE WHEN [PLANED_COST]='-100' THEN 'NA' ELSE CAST(PLANED_COST AS VARCHAR) END AS [Planned Cost (Till P12 End)]
    ,CASE
        WHEN TOTAL_COST IS NULL AND PLANED_COST IS NULL THEN NULL
        WHEN TOTAL_COST IS NULL OR PLANED_COST IS NULL THEN 'NA'
        WHEN PLANED_COST=0 THEN 'NA'
        WHEN PLANED_COST='-100' OR TOTAL_COST='-100' THEN 'NA'
        ELSE CAST(CAST(CAST(TOTAL_COST AS FLOAT)/PLANED_COST *100 AS DECIMAL(15,2)) AS VARCHAR)+' %' 
      END AS [PCA % (Actual)- Formula]
      ,CASE
        WHEN ESTIMATED_COST IS NULL AND (REVISED_CBL IS NULL AND COST_BASELINE IS NULL) THEN NULL
        WHEN ESTIMATED_COST IS NULL THEN 'NA'
        WHEN REVISED_CBL='-100' OR REVISED_CBL IS NULL OR REVISED_CBL=0 THEN 
          CASE
            WHEN CAST(COST_BASELINE AS FLOAT)=-100 OR COST_BASELINE IS NULL OR CAST(COST_BASELINE AS FLOAT)=0 THEN 'NA'
            ELSE CAST(CAST(CAST(ESTIMATED_COST AS FLOAT)/COST_BASELINE *100 AS DECIMAL(15,2)) AS VARCHAR)+' %' 
          END
        ELSE CAST(CAST(CAST(ESTIMATED_COST AS FLOAT)/REVISED_CBL *100 AS DECIMAL(15,2)) AS VARCHAR)+' %' 
      END AS [PCA% (EAC) - Formula]
      ,[PROJECT_START_DATE] AS [Project Planned Start Date (Month)]
      ,[ACTUAL_START_DATE] AS [Project Actual Start Date (Month)]
      ,[PROJECT_END_DATE] AS [Project Planned End Date (Month)]
      ,[ESTIMATED_COMPLETITION_DATE] AS [Estimated Project Completion Date]
      ,CASE WHEN [PROJECT_PLANED_END_DATE]='-100' THEN 'NA' ELSE CAST(PROJECT_PLANED_END_DATE AS VARCHAR) END AS [Project Planned End Date (Duration in days)]
      ,CASE WHEN [PROJECT_ESTIMATED_COMPLETITION_DATE]='-100' THEN 'NA' ELSE CAST(PROJECT_ESTIMATED_COMPLETITION_DATE AS VARCHAR) END AS [Estimated Project Completion Date (duration in days)]
       
      ,CASE WHEN [PLANNED_SITE_TILL_P12]='-100' THEN 'NA' ELSE CAST(PLANNED_SITE_TILL_P12 AS VARCHAR) END AS [Planned Sites till P12.(NI/NPO)/Planned Milestones till P12 (SI)]
      ,CASE WHEN [ACTUAL_SITE_TILL_P12]='-100' THEN 'NA' ELSE CAST(ACTUAL_SITE_TILL_P12 AS VARCHAR) END AS [Actual Sites till P12 (NI/NPO)/Actual Milestones till P12 (SI)]
      ,CASE WHEN [PLANNED_MILESTONES]='-100' THEN 'NA' ELSE CAST(PLANNED_MILESTONES AS VARCHAR) END AS [Planned Milestones Delivered till Pn-1(SI)]
      ,CASE WHEN [ACTUAL_MILESTONES]='-100' THEN 'NA' ELSE CAST(ACTUAL_MILESTONES AS VARCHAR) END AS [Actual Milestones Delivered till Pn-1 (SI)]
	  ,CASE
        WHEN BUSINESS_LINE='MS' THEN '100 %'
        WHEN BUSINESS_LINE='SI' THEN 
          CASE
            WHEN [ACTUAL_MILESTONES] IS NULL AND PLANNED_MILESTONES IS NULL THEN NULL
            WHEN [ACTUAL_MILESTONES]='-100' AND PLANNED_MILESTONES='-100' THEN 'NA'
            WHEN PLANNED_MILESTONES=0 THEN 'NA'
            ELSE CAST(CAST(CAST(ACTUAL_MILESTONES AS FLOAT)/PLANNED_MILESTONES *100 AS DECIMAL(15,2)) AS VARCHAR)+' %' 
          END
        ELSE
          CASE
            WHEN [ACTUAL_SITE_TILL_P12] IS NULL AND PLANNED_SITE_TILL_P12 IS NULL THEN NULL
            WHEN [ACTUAL_SITE_TILL_P12]='-100' AND PLANNED_SITE_TILL_P12='-100' THEN 'NA'
            WHEN PLANNED_SITE_TILL_P12=0 THEN 'NA'
            ELSE CAST(CAST(CAST(ACTUAL_SITE_TILL_P12 AS FLOAT)/PLANNED_SITE_TILL_P12 *100 AS DECIMAL(15,2)) AS VARCHAR)+' %' 
          END
      END AS [Project Completion -Actual %- Formula]
      ,CASE WHEN [BUSINESS_LINE]='-100' THEN 'NA' ELSE CAST(BUSINESS_LINE AS VARCHAR) END AS [Business Line (excel Care)]
      ,CASE WHEN [NR_CHANGES_REQUEST_APPROVED]='-100' THEN 'NA' ELSE CAST(NR_CHANGES_REQUEST_APPROVED AS VARCHAR) END AS [No. of Changes Requests Aprovved]
      ,CASE WHEN [COMPETENCE_ADHERENCE]='-100' THEN 'NA' ELSE CAST(COMPETENCE_ADHERENCE AS VARCHAR) END AS [Competence Adherence (Against SSD)]
      ,CASE WHEN [TOTAL_WORK_PACKAGES_HAS_TO_WORK]='-100' THEN 'NA' ELSE CAST(TOTAL_WORK_PACKAGES_HAS_TO_WORK AS VARCHAR) END AS [Total Work Packages on which GDC has to work]
      ,CASE WHEN [TOTAL_WORK_PACKAGES_ACTUAL_WORKED]='-100' THEN 'NA' ELSE CAST(TOTAL_WORK_PACKAGES_ACTUAL_WORKED AS VARCHAR) END AS [Work Packages on which GDC has actually worked (till P12)]
      ,CASE
        WHEN [ACTUAL_START_DATE] IS NULL AND ESTIMATED_COMPLETITION_DATE IS NULL AND [PROJECT_END_DATE] IS NULL THEN NULL
        WHEN DATEDIFF(DD,[ACTUAL_START_DATE], [PROJECT_END_DATE] )=0 THEN NULL
        ELSE CAST(CAST(CAST(DATEDIFF(DD,[ACTUAL_START_DATE],[ESTIMATED_COMPLETITION_DATE]) AS FLOAT)/DATEDIFF(DD,[ACTUAL_START_DATE], [PROJECT_END_DATE] ) *100 AS DECIMAL(15,2)) AS VARCHAR)
      END AS [Schedule Adherence -EAC (%)- Formula]
       ,CASE
        WHEN TOTAL_WORK_PACKAGES_HAS_TO_WORK IS NULL AND TOTAL_WORK_PACKAGES_ACTUAL_WORKED IS NULL THEN NULL
        WHEN TOTAL_WORK_PACKAGES_HAS_TO_WORK IS NULL OR TOTAL_WORK_PACKAGES_ACTUAL_WORKED IS NULL THEN 'NA'
        WHEN TOTAL_WORK_PACKAGES_ACTUAL_WORKED=0 THEN 'NA'
        ELSE CAST(CAST(CAST(TOTAL_WORK_PACKAGES_HAS_TO_WORK AS FLOAT)/TOTAL_WORK_PACKAGES_ACTUAL_WORKED *100 AS DECIMAL(15,2)) AS VARCHAR)+' %' 
      END AS [Remote Service Catalog Copliance Formula]
      ,CASE WHEN [PTA_BASED_ON_BC]='-100' THEN 'NA' ELSE CAST(PTA_BASED_ON_BC AS VARCHAR) END AS [PTA Sign-off (based on BC approval & SOW/WLA agreement)]
      ,CASE WHEN [PTA_BASED_ACTUAL_DOCUMENT]='-100' THEN 'NA' ELSE CAST(PTA_BASED_ACTUAL_DOCUMENT AS VARCHAR) END AS [PTA Sign-off (actual Document in PMO Onboarding)]
      ,CASE WHEN [SOLUTION_DOCUMENT]='-100' THEN 'NA' ELSE CAST(SOLUTION_DOCUMENT AS VARCHAR) END AS [Solution Document Available (Y/N)]
      ,CASE WHEN [VIRTUAL_ZERO]='-100' THEN 'NA' ELSE CAST(VIRTUAL_ZERO AS VARCHAR) END AS [Virtual Zero Delivery Index (n-1)]
      ,CASE WHEN [CUSTOMER_TEAM_SURVEY]='-100' THEN 'NA' ELSE CAST(CUSTOMER_TEAM_SURVEY AS VARCHAR) END AS [Customer Team Survey(CTS)]
      ,CASE WHEN C.[WBS]='-100' THEN 'NA' ELSE CAST(C.WBS AS VARCHAR) END AS [WBS/Project Code as per fNok FTE report or Lawson tool]
      ,D.SVO AS [Service Order (SvO) Number as per FTE report or Lawson for fAlu]
      ,CASE WHEN [IDX_GIC]='-100' THEN 'NA' ELSE CAST(IDX_GIC AS VARCHAR) END AS [GIC Code]
      ,CASE WHEN [GDN_NAME]='-100' THEN 'NA' ELSE CAST(GDN_NAME AS VARCHAR) END AS [GDN Name]
      ,CASE WHEN [PROJECT_MANAGER]='-100' THEN 'NA' ELSE CAST(PROJECT_MANAGER AS VARCHAR) END AS [Project Manager]
      ,CASE WHEN [DELIVERY_CENTER]='-100' THEN 'NA' ELSE CAST(DELIVERY_CENTER AS VARCHAR) END AS [Delivery Center]
      ,CASE WHEN [BUSINESSS_CASE]='-100' THEN 'NA' ELSE CAST(BUSINESSS_CASE AS VARCHAR) END AS [Business Case Available Y/N]
      ,CASE WHEN [PROJECT_DURATION]='-100' THEN 'NA' ELSE CAST(PROJECT_DURATION AS VARCHAR) END AS [Project Duration Baseline in Months(L-K)]
      ,CASE WHEN [PMO]='-100' THEN 'NA' ELSE CAST(PMO AS VARCHAR) END AS PMO
      ,CASE WHEN [REMARKS]='-100' THEN 'NA' ELSE CAST(REMARKS AS VARCHAR) END AS Remarks
  	FROM ReportingDBProd.[gsd].[RAW_DATA] A
    LEFT JOIN (SELECT DISTINCT r.IDX_COUNTRY_CUSTOMER,
	 STUFF((SELECT distinct ','+ a.COUNTRY
               FROM [ReportingDBProd].[gsd].[MAPPING_COUNTRY] a
             WHERE r.IDX_COUNTRY_CUSTOMER = a.IDX_COUNTRY_CUSTOMER
            FOR XML PATH(''), TYPE).value('.','VARCHAR(max)'), 1, 1, '')  AS [COUNTRY_OF_CUSTOMER]
		FROM ReportingDBProd.[gsd].[MAPPING_COUNTRY] r) B 
		ON A.IDX=B.IDX_COUNTRY_CUSTOMER
	LEFT JOIN (SELECT DISTINCT r.IDX_WBS,
	 STUFF((SELECT distinct ','+ a.WBS
               FROM [ReportingDBProd].[gsd].[MAPPING_WBS] a
             WHERE r.IDX_WBS = a.IDX_WBS
            FOR XML PATH(''), TYPE).value('.','VARCHAR(max)'), 1, 1, '')  AS [WBS]
		FROM ReportingDBProd.[gsd].[MAPPING_WBS] r) C
		ON A.IDX=C.IDX_WBS
	LEFT JOIN (SELECT DISTINCT r.IDX_SVO,
	 STUFF((SELECT distinct ','+ a.SVO
               FROM [ReportingDBProd].[gsd].[MAPPING_SVO] a
             WHERE r.IDX_SVO = a.IDX_SVO
            FOR XML PATH(''), TYPE).value('.','VARCHAR(max)'), 1, 1, '')  AS [SVO]
		FROM ReportingDBProd.[gsd].[MAPPING_SVO] r) D
		ON A.IDX=D.IDX_SVO
    WHERE YEAR_MONTH='$date'
    AND MARKET LIKE '$region'
	ORDER BY IDX DESC";
  //die($sql);
  
	$row = array('#',
			'Country Of Customer',
			'Customer Name',
			'Project Name',
			'Cost Baseline Value in K Euro',
			'Revised CBL',
			'Estimated Cost at Completion E',
			'Total Cost (K Euro) charged Actual from F&C report base on WBS/Svo/GIC]',
			'Planned Cost (Till P12 End)',
			'PCA % (Actual)- Formula',
			'PCA% (EAC) - Formula',
			'Project Planned Start Date (Month)',
			'Project Actual Start Date (Month)',
			'Project Planned End Date (Month)',
			'Estimated Project Completion Date',
			'Project Planned End Date (Duration in days)',
			'Estimated Project Completion Date (Duration in days)',
			'Planned Sites till P12.(NI/NPO)/Planned Milestones till P12 (SI)',
			'Actual Sites till P12 (NI/NPO)/Actual Milestones till P12 (SI)',
			'Planned Milestones Delivered till Pn-1(SI)',
			'Actual Milestones Delivered till Pn-1 (SI)',
			'Project Completion -Actual %- Formula',
			'Business Line (excel Care)',
			'No. of Changes Requests Aproved',
			'Competence Adherence (Against SSD) ',
			'Total Work Packages on which GDC has to work',
			'Work Packages on which GDC has actually worked (till P12)',
			'Schedule Adherence -EAC (%)- Formula',
			'Remote Service Catalog Copliance Formula',
			'PTA Sign-off (based on BC approval & SOW/WLA agreement)',
			'PTA Sign-off (actual Document in PMO Onboarding)',
			'Solution Document Available (Y/N)',
			'Virtual Zero Delivery Index (n-1)',
			'Customer Team Survey(CTS)',
			'WBS/Project Code as per fNok FTE report or Lawson tool',
			'Service Order (SvO) Number as per FTE report or Lawson for fAlu',
			'GIC Code',
			'GDN Name',
			'Project Manager',
			'Delivery Center',
			'Business Case Available Y/N',
			'Project Duration Baseline in M',
			'PMO',
			'Remarks'
	);
$arr[] = $row;
$data = mssql_query($sql);

	while ($row = mssql_fetch_assoc($data)) {
		
	    foreach ($row as $key => $value) {
	        //$row[$key] = utf8_encode($value);
			
			if (utf8_encode($value)=='-100'){
				 $row[$key] = 'NA';
			}else{
				 $row[$key] = utf8_encode($value);
			}
			
	    }
	    $arr[] = $row;

	}


$doc = new PHPExcel();
$doc->setActiveSheetIndex(0);

// $rows = $doc->getActiveSheet()->fromArray(array_keys($arr[0]),NULL,'A2');

$doc->getActiveSheet()->fromArray($arr, null, 'A1');

foreach(range('A','AR') as $columnID) {

    $doc->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);

}
$style = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    )
);

$doc->getDefaultStyle()->applyFromArray($style);
$doc->getActiveSheet()->getStyle('1:1')->getFont()->setBold(true);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Export Raw Data.xls"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$writer = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

$writer->save('php://output');


}
// header('Content-Type: application/vnd.ms-excel; charset: utf-8');
// header('Content-Disposition: attachment;filename="exp.xls"');
// header('Cache-Control: max-age=0');
 

//$objWriter->save('test2.xls');  //send it to user, of course you can save it to disk also!
 
exit; //done.. exiting!
?>