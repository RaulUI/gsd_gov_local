<?php

include '_exportSQL.php';
include '_exportArrayToExcel.php';

include '/srv/data203386/sftp/jail/ftp/gnoc/mssql_auth/mssql_config.php';

include "PHPExcel/Classes/PHPExcel/IOFactory.php";


$upi = $_SESSION['GOV_UPI'];
$view = $_GET["view"];
$date=$_GET["date"];
$region = $_GET["region"];
$market = $_GET["market"];
$ym=$_GET["date"];

$ymWithDay = $ym . '-01';

/*	echo'upi-';
	echo $upi;
	echo'-upi';
	echo $view;
	echo $date;
	echo $region;
	echo $ym;
	echo $ymWithDay;
	die();*/

if($view == 'Executive Dashboard'){
	$objTpl = PHPExcel_IOFactory::load("executive_template.xlsx");
	$objTpl->setActiveSheetIndex(0);

	$mustHave = array(" ","  ","Total", "MS", "NI", "NPO", "SI", "MULTI BL");

	$sql = "select * from ReportingDBProd.GSD.SNAP_DATA
		WHERE SNAP_DATE LIKE '$ym' AND MARKET like '$region'
	
	
	";

//die($sql);
$rs=mssql_query($sql);
$month=explode('-',$date);
$objTpl->getActiveSheet()->setCellValue('B2','Executive Dashboard - '.$region.' (P'.$month[1].')');

  if (!$rs){
	exit('MSSQL error: ' . mssql_get_last_message());
  }	
  $i=5;
  while ($row=mssql_fetch_array($rs))
	{
		if($i<13)
		{
			$j=0;
			if($row['BUSINESS_LINE'] != '999' && is_numeric($row['BUSINESS_LINE']))
				$j = 5;
			if($row['BUSINESS_LINE'] == 'MS')
				$j = 7;
			if($row['BUSINESS_LINE'] == 'NI')
				$j = 8;
			if($row['BUSINESS_LINE'] == 'SI')
				$j = 9;
			if($row['BUSINESS_LINE'] == 'NPO')
				$j = 10;
			if($row['BUSINESS_LINE'] == 'MULTI BL')
				$j = 11;
			if($row['BUSINESS_LINE'] == 'Total')
				$j = 12;
			if($j != 0)
			{
				$objTpl->getActiveSheet()->setCellValue('C'.$j,$row['PROJ_EXCL_CARE']);
				$objTpl->getActiveSheet()->setCellValue('D'.$j,$row['PTA_SIGNED']);
				$objTpl->getActiveSheet()->setCellValue('E'.$j,$row['PROJ_32']);
				$objTpl->getActiveSheet()->setCellValue('F'.$j,$row['GDC_100']);
				$objTpl->getActiveSheet()->setCellValue('G'.$j,$row['GDC_1_3']);
				$objTpl->getActiveSheet()->setCellValue('H'.$j,$row['GDC_3_5']);
				$objTpl->getActiveSheet()->setCellValue('I'.$j,$row['GDC_500']);
				$objTpl->getActiveSheet()->setCellValue('J'.$j,$row['COST_RED']);
				$objTpl->getActiveSheet()->setCellValue('K'.$j,$row['COST_AMBER']);
				$objTpl->getActiveSheet()->setCellValue('L'.$j,$row['COST_GREEN']);
				$objTpl->getActiveSheet()->setCellValue('M'.$j,$row['TIME_RED']);
				$objTpl->getActiveSheet()->setCellValue('N'.$j,$row['TIME_AMBER']);
				$objTpl->getActiveSheet()->setCellValue('O'.$j,$row['TIME_GREEN']);
				$objTpl->getActiveSheet()->setCellValue('P'.$j,$row['SCOPE_RED']);
				$objTpl->getActiveSheet()->setCellValue('Q'.$j,$row['SCOPE_AMBER']);
				$objTpl->getActiveSheet()->setCellValue('R'.$j,$row['SCOPE_GREEN']);
				$objTpl->getActiveSheet()->setCellValue('S'.$j,$row['QUALITY_RED']);
				$objTpl->getActiveSheet()->setCellValue('T'.$j,$row['QUALITY_AMBER']);
				$objTpl->getActiveSheet()->setCellValue('U'.$j,$row['QUALITY_GREEN']);
			}
			$i++;
		}

	
	//$nr_supp_ev=$nr_supp_ev+1;
  }	

  $queryCountAll = "SELECT TOTAL FROM ReportingDBProd.gsd.TOTAL2MARKET
					WHERE YEAR_MONTH like '$ym'
						AND MARKET LIKE '$region'";
	//die($queryCountAll);		

	$runCount = mssql_query($queryCountAll);

	while($rowTwo = mssql_fetch_array($runCount)){
		$objTpl->getActiveSheet()->setCellValue('B5',$rowTwo['TOTAL']);
	}
	$filename='GSDM_Executive_Dashboard.xls'; 

	$objWriter = PHPExcel_IOFactory::createWriter($objTpl, 'Excel5');
	header('Content-Type: application/vnd.ms-excel; charset: utf-8');
	header('Content-Disposition: attachment;filename="export-market.xls"');
	header('Cache-Control: max-age=0');
	$objWriter->save('php://output');
}


exit; //done.. exiting!
?>