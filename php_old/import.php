<?php
	ini_set('memory_limit', '4096M');
	header('Content-Type: application/json');
		include "../../_libs/php/Utilities/Config.php";
	include "../../_libs/php/Utilities/MsSql.php";
	include "../../_libs/php/PHPExcel/PHPExcel.php";
	include "../../_libs/php/Utilities/util.php";
	include "../../_libs/php/PHPMailer/PHPMailerAutoload.php";
	include "../../_libs/php/Utilities/SynopsisMailer.php";
	include '/srv/data203386/sftp/jail/ftp/gnoc/mssql_auth/mssql_config.php';
	include '../_exportSQL.php';
	
	ini_set('mssql.charset', 'UTF-8');
	session_start();

	$upi = $_SESSION['GOV_UPI'];
	
	
	function verificator($word){


		$wordlen = strlen($word);
		$count = 0;

		// Numaram numarul de quotes ce trebuie dublate
		for($i = 0; $i < $wordlen; $i++){
			if($word[$i] == "'"){
				$count++;
			}
		}

		for($j = 0; $j < $count; $j++){
			$wordlen = strlen($word);
			if($j == 0){
				$crt_count = 0;
			}else{
				$crt_count = 0 - $j;
			}
			for($i = 0; $i < $wordlen; $i++){
				if($word[$i] == "'" && $crt_count == $j){
					$word = substr($word, 0, $i)."'".substr($word, $i, $wordlen);
					break;
				}else if($word[$i] == "'"){
					$crt_count++;
				}
			}
		}

		
		if ( ($word==='N/A')||($word==='NA') || ($word === '-') ){
			$word='-100';
		}
		if ($word=='#VALUE!' ){
			$word='';
		}
		
		if ($word=='#N/A' ){
			$word='';
		}

		if ($word== ''){
			$word=NULL;
		}
		
		//echo $word.'<br>'; 
		return $word;
	}

	$data = array();
	/*$upi = $_SESSION['CROZOP_UPI'];
	$user = $_POST['user'];*/

	$_market = $_POST['market'];
	$_yearMonth = $_POST['yearMonth'];

	$error = false;
	$files = array();
	$uploaddir = '../uploads/';

	foreach($_FILES as $file)
	{
		$file_ext = strtolower(end(explode('.', $file['name'])));
		$file_name = str_replace($file_ext, '', $file['name']);
	    if(move_uploaded_file($file['tmp_name'], $uploaddir .basename($file_name  .date("Y-m-d H:i:s") .'.' .$file_ext)))
	    {
	        $files[] = $uploaddir .$file_name  .date("Y-m-d H:i:s") .'.' .$file_ext;
	        $filePath = $uploaddir .$file_name  .date("Y-m-d H:i:s") .'.' .$file_ext;
	    }
	    else
	    {
	        $error = true;
	    }
	}
	$data = ($error) ? array('error' => 'There was an error uploading your files') : array('files' => $files);

	//echo json_encode($data);



	$inputFileType = PHPExcel_IOFactory::identify($filePath);
	$objReader = PHPExcel_IOFactory::createReader($inputFileType);

	$objReader->setReadDataOnly(true);
	$objPHPExcel = $objReader->load($filePath);
	$worksheet = $objPHPExcel->getSheet(0); 
	$highestRow = $worksheet->getHighestRow(); 
	$highestColumn = $worksheet->getHighestColumn();

	 // print_r($headings);
	 // die();
	for ($row = 1; $row <= $highestRow; $row++){ 
    //  Read a row of data into an array
	    $rowData[$row] = $worksheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
	                                            NULL,
	                                            TRUE,
	                                            FALSE);
	     //$rowData[$row] = array_combine($headings[$row], $rowData[$row]);
	}
// print_r($rowData);
// die();

	if(count($rowData[1][0]) != 45){
		$sql = "INSERT INTO ReportingDBProd.[gsd].[INSERT_LOG] (
					[ERROR_STRING],
					[FILENAME],
					[UPLOAD_DATE],
					[MARKET],
					[YEAR_MONTH],
					[UPI]

					) VALUES (
					'Wrong Template',
					'".verificator($file_name)."',
					GETDATE(),
					'".$_market."',
					'".$_yearMonth."',
					'".$upi."'

					)";

					$runSql = mssql_query($sql);
					if(!$runSql){
						echo $sql;
					}
	}else {

		$wbs = array();
		$svo = array();
		$countryz = array();
		
		for($i=2;$i<=count($rowData);$i++){
			$countryz[$i] = explode(',', $rowData[$i][0][1]);
			
			/*
			$rowData[$i][0][11] = '20'.PHPExcel_Style_NumberFormat::toFormattedString($rowData[$i][0][11], 'Y-m-d');
			$rowData[$i][0][12] = '20'.PHPExcel_Style_NumberFormat::toFormattedString($rowData[$i][0][12], 'Y-m-d');
			$rowData[$i][0][13] = '20'.PHPExcel_Style_NumberFormat::toFormattedString($rowData[$i][0][13], 'Y-m-d');
			$rowData[$i][0][14] = '20'.PHPExcel_Style_NumberFormat::toFormattedString($rowData[$i][0][14], 'Y-m-d');
			*/
			
			// $convertSql11 = "SELECT CONVERT(NVARCHAR(10), CONVERT(DATE, CAST($rowData[$i][0][11]as SmallDateTime)), 105)as VAL ";
			// $runConvert11 = mssql_query($convertSql11);

			// while($resultConv1 = mssql_fetch_array($runConvert11)){
			// 	$rowData[$i][0][11] = $resultConv1[0];
			// }

			// $convertSql12 = "SELECT CONVERT(NVARCHAR(10), CONVERT(DATE, CAST($rowData[$i][0][12]as SmallDateTime)), 105)as VAL ";
			// $runConvert12 = mssql_query($convertSql12);

			// while($resultConv2 = mssql_fetch_array($runConvert12)){
			// 	$rowData[$i][0][12] = $resultConv2[0];
			// }

			// $convertSql13 = "SELECT CONVERT(NVARCHAR(10), CONVERT(DATE, CAST($rowData[$i][0][13]as SmallDateTime)), 105)as VAL ";
			// $runConvert13 = mssql_query($convertSql13);

			// while($resultConv3 = mssql_fetch_array($runConvert13)){
			// 	$rowData[$i][0][13] = $resultConv3[0];
			// }

			// $convertSql14 = "SELECT CONVERT(NVARCHAR(10), CONVERT(DATE, CAST($rowData[$i][0][14]as SmallDateTime)), 105)as VAL ";
			// $runConvert14 = mssql_query($convertSql14);

			// while($resultConv4 = mssql_fetch_array($runConvert14)){
			// 	$rowData[$i][0][14] = $resultConv4[0];
			// }


			if( PHPExcel_Style_NumberFormat::toFormattedString($rowData[$i][0][11]) ){
				if( $rowData[$i][0][11] == 'NA' ||  $rowData[$i][0][11] == ''||  $rowData[$i][0][11] == NULL){
					 $rowData[$i][0][11] = NULL;
				}
				else if ( substr(PHPExcel_Style_NumberFormat::toFormattedString($rowData[$i][0][11], 'Y-m-d'), 0, 2)!='20' ){
					$rowData[$i][0][11] = '20'.PHPExcel_Style_NumberFormat::toFormattedString($rowData[$i][0][11], 'Y-m-d');
				}
			}
			if( PHPExcel_Style_NumberFormat::toFormattedString($rowData[$i][0][12]) ){
				if( $rowData[$i][0][12] == 'NA'||  $rowData[$i][0][12] == ''||  $rowData[$i][0][12] == NULL){
					 $rowData[$i][0][12] = NULL;
				}
				else if ( substr(PHPExcel_Style_NumberFormat::toFormattedString($rowData[$i][0][12], 'Y-m-d'), 0, 2)!='20' ){
					$rowData[$i][0][12] = '20'.PHPExcel_Style_NumberFormat::toFormattedString($rowData[$i][0][12], 'Y-m-d');
				}
			}
			if( PHPExcel_Style_NumberFormat::toFormattedString($rowData[$i][0][13]) ){
				if( $rowData[$i][0][13] == 'NA' ||  $rowData[$i][0][13] == ''||  $rowData[$i][0][13] == NULL){
					 $rowData[$i][0][13] = NULL;
				}
				else if ( substr(PHPExcel_Style_NumberFormat::toFormattedString($rowData[$i][0][13], 'Y-m-d'), 0, 2)!='20' ){
					$rowData[$i][0][13] = '20'.PHPExcel_Style_NumberFormat::toFormattedString($rowData[$i][0][13], 'Y-m-d');
				}	
			}
			if( PHPExcel_Style_NumberFormat::toFormattedString($rowData[$i][0][14]) ){
				if( $rowData[$i][0][14] == 'NA' ||  $rowData[$i][0][14] == ''||  $rowData[$i][0][14] == NULL){
					 $rowData[$i][0][14] = NULL;
				}
				else if ( substr(PHPExcel_Style_NumberFormat::toFormattedString($rowData[$i][0][14], 'Y-m-d'), 0, 2)!='20' ){
					$rowData[$i][0][14] = '20'.PHPExcel_Style_NumberFormat::toFormattedString($rowData[$i][0][14], 'Y-m-d');
				}	
			}			
			
			if($rowData[$i][0][18] == 'NA'){
				$rowData[$i][0][18] = '-100';
			}
			if($rowData[$i][0][19] == 'NA'){
				$rowData[$i][0][19] = '-100';
			}
			if($rowData[$i][0][26] == 'NA'){
				$rowData[$i][0][26] = '-100';
			}
			if($rowData[$i][0][27] == 'NA'){
				$rowData[$i][0][27] = '-100';
			}
			if($rowData[$i][0][32] == 'NA' || $rowData[$i][0][32] == 'NA '){
				$rowData[$i][0][32] = '-100';
			}
			if(strpos($rowData[$i][0][34],',') === false){
				$rowData[$i][0][34] = preg_replace('~[\r\n]+~', ',', $rowData[$i][0][34]);
			}
			$rowData[$i][0][34]=rtrim($rowData[$i][0][34],',');
			$wbs[$i] = explode(',', $rowData[$i][0][34]);
			
			if(strpos($rowData[$i][0][35],',') === false){
				$rowData[$i][0][35] = preg_replace('~[\r\n]+~', ',', $rowData[$i][0][35]);
			}
			$rowData[$i][0][35]=rtrim($rowData[$i][0][35],',');
			$svo[$i] = explode(',', $rowData[$i][0][35]);

			if($rowData[$i][0][36] == 'NA'){
				$rowData[$i][0][36] = '-100';
			}
		
		}
		// print_r($wbs);
		// print_r($svo);
		// print_r($countryz);
		// die();


		
		// for($i=2;$i<count($rowData);$i++){
		// 	$sql = ''
		// }

		$countInsert = 0;
		$countErrorInsert = 0;
		$countUpdate = 0;
		$countErrorUpdate = 0;
		$stringErrorUpdate = '';
		$stringErrorInsert = '';
		
			for($j=2; $j<=count($rowData);$j++){
				if(!empty($rowData[$j][0][3])){

					$insert="";
					for ($i=2; $i < 45; $i++) { 
						if(!in_array($i, array(9,10,17,22,28,33,34,35), true )){
							if(!in_array($i, array(11,12,13,14), true )){
								$insert.="'".verificator($rowData[$j][0][$i])."',";
							}else{
								if(verificator($rowData[$j][0][$i]) == NULL){
									$insert.= 'NULL'.",";
								}else{
									$insert.="'".verificator($rowData[$j][0][$i])."',";
								}
							}
						}
					}
					
					$insert=rtrim($insert,',');

					//echo $insert;

					$queryCheck = " SELECT COUNT(*) FROM ReportingDBProd.[gsd].[RAW_DATA] 
								WHERE [YEAR_MONTH] = '".$_yearMonth."'
								  AND [PROJECT_NAME] = '".verificator($rowData[$j][0][3])."'
								  AND [MARKET] = '".$_market."'
								  AND [BUSINESS_LINE] = '".verificator($rowData[$j][0][23])."'
						";

					$result = mssql_query($queryCheck); 
					while($row = mssql_fetch_row($result)){
						$arr = $row[0];
					}
					
					//$arr=0 ;// conditie pusa temporar de Gabi sa se incarce toate proiectele
					if($arr == 0){
						//INSERT INTO ReportingDBProd.[gsd].[RAW_DATA_18042017] 
						$sql = "INSERT INTO ReportingDBProd.[gsd].[RAW_DATA] (
							[CUSTOMER_NAME],
							[PROJECT_NAME],
							[COST_BASELINE],
							[REVISED_CBL],
							[ESTIMATED_COST],
							[TOTAL_COST],
							[PLANED_COST],
							[PROJECT_START_DATE],
							[ACTUAL_START_DATE],
							[PROJECT_END_DATE],
							[ESTIMATED_COMPLETITION_DATE],
							[PROJECT_PLANED_END_DATE],
							[PROJECT_ESTIMATED_COMPLETITION_DATE],
							[PLANNED_SITE_TILL_P12],
							[ACTUAL_SITE_TILL_P12],
							[TOTAL_MILESTONES],
							[REVISED_MILESTONE],
							[BUSINESS_LINE],
							[NR_CHANGES_REQUEST_APPROVED],
							[COMPETENCE_ADHERENCE],
							[TOTAL_WORK_PACKAGES_HAS_TO_WORK],
							[TOTAL_WORK_PACKAGES_ACTUAL_WORKED],
							[PTA_BASED_ACTUAL_DOCUMENT],
							[SOLUTION_DOCUMENT],
							[VIRTUAL_ZERO],
							[CUSTOMER_TEAM_SURVEY],
							[IDX_GIC],
							[GDN_NAME],
							[PROJECT_MANAGER],
							[DELIVERY_CENTER],
							[BUSINESSS_CASE],
							[PROJECT_DURATION],
							[PMO],
							[REMARKS],
							[TOP32PROJECT],
							[MARKET],
							[YEAR_MONTH]
							) VALUES (
							".$insert.",
							'".$_market."',
							'".$_yearMonth."'
							)";
						// echo $sql;
						// die($sql);
						$runSql = mssql_query($sql);
						if(!$runSql){
							echo $sql;
							$countErrorInsert++;
							$stringErrorInsert .= 'Project: '.verificator($rowData[$j][0][3]).',		BL: '.verificator($rowData[$j][0][23]).',		Error: '.mssql_get_last_message().'<br>';
							
							$strErrorSql=" insert into ReportingDBProd.[gsd].[UPLOAD_ERROR_LOG] ([PROJECT_NAME],[BUSINESS_LINE],[TYPE_UPLOAD],[ERROR_STRING],[FILENAME],[UPLOAD_DATE],[MARKET],[YEAR_MONTH],[UPI] )";
							$strErrorSql.=" values('".verificator($rowData[$j][0][3])."','".verificator($rowData[$j][0][23])."','INSERT'".",'".mssql_get_last_message()."','".verificator($file_name)."',CURRENT_TIMESTAMP";
							$strErrorSql.=",'".$_market."',";
							$strErrorSql.="'".$_yearMonth."',";
							$strErrorSql.="'".$upi."'";
							$strErrorSql.=")";
							
							$runSqlError = mssql_query($strErrorSql);
							if(!$runSqlError ){
								echo $strErrorSql;
							}
							
						}else{
							$countInsert++;

							$sqlGetIdx = 'SELECT MAX(IDX) FROM ReportingDBProd.[gsd].[RAW_DATA] ';
							$runsqlGetIdx = mssql_query($sqlGetIdx);

							while ($row = mssql_fetch_row($runsqlGetIdx)){	
								$idx = $row[0];
							}

							for($k=0; $k < count($countryz[$j]);$k++){

								$sqlInsertCountries = "INSERT INTO ReportingDBProd.[gsd].[MAPPING_COUNTRY] (
								[IDX_COUNTRY_CUSTOMER],
								[COUNTRY]
								)VALUES (
								'".$idx."', 
								'".$countryz[$j][$k]."'
								)";

								$runsqlInsertCountries = mssql_query($sqlInsertCountries);
							}

							for($l=0; $l < count($wbs[$j]);$l++){

								$sqlInsertWBS = "INSERT INTO ReportingDBProd.[gsd].[MAPPING_WBS] (
								[IDX_WBS],
								[WBS]
								)VALUES (
								'".$idx."', 
								'".$wbs[$j][$l]."'
								)";

								$runsqlInsertWBS = mssql_query($sqlInsertWBS);
							}

							for($m=0; $m < count($svo[$j]);$m++){

								$sqlInsertSVO = "INSERT INTO ReportingDBProd.[gsd].[MAPPING_SVO] (
								[IDX_SVO],
								[SVO]
								)VALUES (
								'".$idx."', 
								'".$svo[$j][$m]."'
								)";

								$runsqlInsertSVO = mssql_query($sqlInsertSVO);
							}
						}

					}else{

						$sqlGetIdxUpdate = "SELECT IDX FROM ReportingDBProd.[gsd].[RAW_DATA] 
								WHERE [YEAR_MONTH] = '".$_yearMonth."'
								  AND [PROJECT_NAME] = '".verificator($rowData[$j][0][3])."'
								  AND [MARKET] = '".$_market."'
								  AND [BUSINESS_LINE] = '".verificator($rowData[$j][0][23])."'";

						$result = mssql_query($sqlGetIdxUpdate); 
							while($row = mssql_fetch_row($result)){
								$idx = $row[0];
						}

						$sqlClean = " DELETE FROM ReportingDBProd.[gsd].[MAPPING_COUNTRY] WHERE IDX_COUNTRY_CUSTOMER = '".$idx."'
						DELETE FROM ReportingDBProd.[gsd].[MAPPING_WBS] WHERE IDX_WBS = '".$idx."'
						DELETE FROM ReportingDBProd.[gsd].[MAPPING_SVO] WHERE IDX_SVO = '".$idx."'";

						$runsqlClean = mssql_query($sqlClean);
						

						$start_date				=	verificator($rowData[$j][0][11]);
						$actual_start_date		=	verificator($rowData[$j][0][12]);
						$end_date				=	verificator($rowData[$j][0][13]);
						$estimation_end_date	=	verificator($rowData[$j][0][14]);
						
						if($start_date!=''){
							$start_date="'".$start_date."'";
						}else{
							$start_date='NULL';
						}
						
						if($actual_start_date!=''){
							$actual_start_date="'".$actual_start_date."'";
						}else{
							$actual_start_date='NULL';
						}
						
						if($end_date!=''){
							$end_date="'".$end_date."'";
						}else{
							$end_date='NULL';
						}
						
						if($estimation_end_date!=''){
							$estimation_end_date="'".$estimation_end_date."'";
						}else{
							$estimation_end_date='NULL';
						}

						$query = "UPDATE ReportingDBProd.[gsd].[RAW_DATA] 
								SET
							[CUSTOMER_NAME] = '".verificator($rowData[$j][0][2])."',
							[COST_BASELINE] = '".verificator($rowData[$j][0][4])."',
							[REVISED_CBL] = '".verificator($rowData[$j][0][5])."',
							[ESTIMATED_COST] = '".verificator($rowData[$j][0][6])."',
							[TOTAL_COST] = '".verificator($rowData[$j][0][7])."',
							[PLANED_COST] = '".verificator($rowData[$j][0][8])."',
							[PROJECT_START_DATE] = ".$start_date.",
							[ACTUAL_START_DATE] = ".$actual_start_date.",
							[PROJECT_END_DATE] = ".$end_date.",
							[ESTIMATED_COMPLETITION_DATE] = ".$estimation_end_date.",
							[PROJECT_PLANED_END_DATE] = '".verificator($rowData[$j][0][15])."',
							[PROJECT_ESTIMATED_COMPLETITION_DATE] = '".verificator($rowData[$j][0][16])."',
							[PLANNED_SITE_TILL_P12] = '".verificator($rowData[$j][0][18])."',
							[ACTUAL_SITE_TILL_P12] = '".verificator($rowData[$j][0][19])."',
							[TOTAL_MILESTONES] = '".verificator($rowData[$j][0][20])."',
							[REVISED_MILESTONE] = '".verificator($rowData[$j][0][21])."',
							[BUSINESS_LINE] = '".verificator($rowData[$j][0][23])."',
							[NR_CHANGES_REQUEST_APPROVED] = '".verificator($rowData[$j][0][24])."',
							[COMPETENCE_ADHERENCE] = '".verificator($rowData[$j][0][25])."',
							[TOTAL_WORK_PACKAGES_HAS_TO_WORK] = '".verificator($rowData[$j][0][26])."',
							[TOTAL_WORK_PACKAGES_ACTUAL_WORKED] = '".verificator($rowData[$j][0][27])."',
							[PTA_BASED_ACTUAL_DOCUMENT] = '".verificator($rowData[$j][0][29])."',
							[SOLUTION_DOCUMENT] = '".verificator($rowData[$j][0][30])."',
							[VIRTUAL_ZERO] = '".verificator($rowData[$j][0][31])."',
							[CUSTOMER_TEAM_SURVEY] = '".verificator($rowData[$j][0][32])."',
							[IDX_GIC] = '".verificator($rowData[$j][0][36])."',
							[GDN_NAME] = '".verificator($rowData[$j][0][37])."',
							[PROJECT_MANAGER] = '".verificator($rowData[$j][0][38])."',
							[DELIVERY_CENTER] = '".verificator($rowData[$j][0][39])."',
							[BUSINESSS_CASE] = '".verificator($rowData[$j][0][40])."',
							[PROJECT_DURATION] = '".verificator($rowData[$j][0][41])."',
							[PMO] = '".verificator($rowData[$j][0][42])."',
							[REMARKS] = '".verificator($rowData[$j][0][43])."',
							[TOP32PROJECT] = '".verificator($rowData[$j][0][44])."'
							
							WHERE [YEAR_MONTH] = '$_yearMonth'
							AND [PROJECT_NAME] = '".verificator($rowData[$j][0][3])."'
							AND [MARKET] = '".$_market."'
							AND [BUSINESS_LINE] = '".verificator($rowData[$j][0][23])."'";
							//die($query);
							//echo $query;
							$runSql = mssql_query($query);

							if(!$runSql){
								echo $sql;
								$countErrorUpdate++;
								
								$strErrorSql="insert into ReportingDBProd.[gsd].[UPLOAD_ERROR_LOG] ([PROJECT_NAME],[BUSINESS_LINE],[TYPE_UPLOAD],[ERROR_STRING],[FILENAME],[UPLOAD_DATE],[MARKET],[YEAR_MONTH],[UPI] )";
								$strErrorSql.=" values('".verificator($rowData[$j][0][3])."','".verificator($rowData[$j][0][23])."','UPDATE'".",'".mssql_get_last_message()."','".verificator($file_name)."',CURRENT_TIMESTAMP";
								$strErrorSql.=",'".$_market."',";
								$strErrorSql.="'".$_yearMonth."',";
								$strErrorSql.="'".$upi."'";
								$strErrorSql.=")";
								
								$runSqlError = mssql_query($strErrorSql);
								if(!$runSqlError ){
									echo $strErrorSql;
								}		
								
							}else{
								$countUpdate++;

								$strInsertAudit = "INSERT INTO ReportingDBProd.[gsd].[RAW_DATA_AUDIT_REVISED] 
								VALUES(
									". $idx . ", 
									'".verificator($rowData[$j][0][2])."',
									'".verificator($rowData[$j][0][3])."',
									'".verificator($rowData[$j][0][4])."',
									'".verificator($rowData[$j][0][5])."',
									'".verificator($rowData[$j][0][6])."',
									'".verificator($rowData[$j][0][7])."',
									'".verificator($rowData[$j][0][8])."',
									".$start_date.",
									".$actual_start_date.",
									".$end_date.",
									".$estimation_end_date.",
									'".verificator($rowData[$j][0][15])."',
									'".verificator($rowData[$j][0][16])."',
									'".verificator($rowData[$j][0][18])."',
									'".verificator($rowData[$j][0][19])."',
									'".verificator($rowData[$j][0][20])."',
									'".verificator($rowData[$j][0][21])."',
									'".verificator($rowData[$j][0][23])."',
									'".verificator($rowData[$j][0][24])."',
									'".verificator($rowData[$j][0][25])."',
									'".verificator($rowData[$j][0][26])."',
									'".verificator($rowData[$j][0][27])."', NULL,
									'".verificator($rowData[$j][0][29])."',
									'".verificator($rowData[$j][0][30])."',
									'".verificator($rowData[$j][0][31])."',
									'".verificator($rowData[$j][0][32])."',
									'".verificator($rowData[$j][0][36])."',
									'".verificator($rowData[$j][0][37])."',
									'".verificator($rowData[$j][0][38])."',
									'".verificator($rowData[$j][0][39])."',
									'".verificator($rowData[$j][0][40])."',
									'".verificator($rowData[$j][0][41])."',
									'".verificator($rowData[$j][0][42])."',
									'".verificator($rowData[$j][0][43])."',
									'" . $_market . "',
									'" . $_yearMonth . "', NULL, NULL, NULL,
									'".verificator($rowData[$j][0][44])."', NULL, NULL, NULL,
									'".$upi."', CURRENT_TIMESTAMP)";

								// print_r($strInsertAudit);
								// die();
								$runSqlError = mssql_query($strInsertAudit);
							}


							for($k=0; $k < count($countryz[$j]);$k++){

								$sqlInsertCountries = "INSERT INTO ReportingDBProd.[gsd].[MAPPING_COUNTRY] (
								[IDX_COUNTRY_CUSTOMER],
								[COUNTRY]
								)VALUES (
								'".$idx."', 
								'".$countryz[$j][$k]."'
								)";

								$runsqlInsertCountries = mssql_query($sqlInsertCountries);
							}

							for($l=0; $l < count($wbs[$j]);$l++){

								$sqlInsertWBS = "INSERT INTO ReportingDBProd.[gsd].[MAPPING_WBS] (
								[IDX_WBS],
								[WBS]
								)VALUES (
								'".$idx."', 
								'".$wbs[$j][$l]."'
								)";

								$runsqlInsertWBS = mssql_query($sqlInsertWBS);
							}

							for($m=0; $m < count($svo[$j]);$m++){

								$sqlInsertSVO = "INSERT INTO ReportingDBProd.[gsd].[MAPPING_SVO] (
								[IDX_SVO],
								[SVO]
								)VALUES (
								'".$idx."', 
								'".$svo[$j][$m]."'
								)";

								$runsqlInsertSVO = mssql_query($sqlInsertSVO);
							}


					}
				}
			}

			$sqlLog = "

					INSERT INTO ReportingDBProd.[gsd].[INSERT_LOG] (
						[INSERT_OK],
						[INSERT_ERROR],
						[UPDATE_OK],
						[UPDATE_ERROR],
						[ERROR_STRING],
						[FILENAME],
						[UPLOAD_DATE],
						[MARKET],
						[YEAR_MONTH],
						[UPI]

						) VALUES (
						".$countInsert.",
						".$countErrorInsert.",
						".$countUpdate.",
						".$countErrorUpdate.",
						'".verificator($stringErrorInsert)."',
						'".verificator($file_name)."',
						GETDATE(),
						'".$_market."',
						'".$_yearMonth."',
						'".$upi."'

						)
				";

				$runSql = mssql_query($sqlLog);
						if(!$runSql){
							echo $sqlLog;
						}
		}
	// for($i=2; $i<=count($toDb); $i++){
	// 	$queryCheck = " SELECT COUNT(*) FROM ReportingDBProd.[gsd].[RAW_DATA] 
	// 					WHERE [YEAR_MONTH] = $_yearMonth
	// 					  AND [PROJECT_NAME] = '".$toDb[$i]['PROJECT_NAME']."'
	// 			";

	// 	$result = mssql_query($queryCheck); 
	// 	while($row = mssql_fetch_row($result)){
	// 		$arr = $row[0];
	// 	}

	// 	if($arr == 0){
	// 		$insertsReport++;
	// 		$query = "INSERT INTO ReportingDBProd.[gsd].[RAW_DATA_18042017] 
	// 				VALUES
	// 				(
	// 					'".$toDb[$i]['CUSTOMER_NAME']."',
	// 					'".$toDb[$i]['PROJECT_NAME']."',
	// 					'".$toDb[$i]['COST_BASELINE']."',
	// 					'".$toDb[$i]['REVISED_CBL']."',
	// 					'".$toDb[$i]['ESTIMATED_COST']."',
	// 					'".$toDb[$i]['TOTAL_COST']."',
	// 					'".$toDb[$i]['PLANED_COST']."',
	// 					'".$toDb[$i]['PROJECT_START_DATE']."',
	// 					'".$toDb[$i]['ACTUAL_START_DATE']."',
	// 					'".$toDb[$i]['PROJECT_END_DATE']."',
	// 					'".$toDb[$i]['ESTIMATED_COMPLETITION_DATE']."',
	// 					'".$toDb[$i]['PROJECT_PLANED_END_DATE']."',
	// 					'".$toDb[$i]['PROJECT_ESTIMATED_COMPLETITION_DATE']."',
	// 					'".$toDb[$i]['PLANNED_SITE_TILL_P12']."',
	// 					'".$toDb[$i]['ACTUAL_SITE_TILL_P12']."',
	// 					'".$toDb[$i]['PLANNED_MILESTONES']."',
	// 					'".$toDb[$i]['BUSINESS_LINE']."',
	// 					'".$toDb[$i]['NR_CHANGES_REQUEST_APPROVED']."',
	// 					'".$toDb[$i]['COMPETENCE_ADHERENCE']."',
	// 					'".$toDb[$i]['TOTAL_WORK_PACKAGES_HAS_TO_WORK']."',
	// 					'".$toDb[$i]['TOTAL_WORK_PACKAGES_ACTUAL_WORKED']."',
	// 					'".$toDb[$i]['PTA_BASED_ON_BC']."',
	// 					'".$toDb[$i]['PTA_BASED_ACTUAL_DOCUMENT']."',
	// 					'".$toDb[$i]['SOLUTION_DOCUMENT']."',
	// 					'".$toDb[$i]['VIRTUAL_ZERO']."',
	// 					'".$toDb[$i]['CUSTOMER_TEAM_SURVEY']."',
	// 					'".$toDb[$i]['IDX_GIC']."',
	// 					'".$toDb[$i]['GDN_NAME']."',
	// 					'".$toDb[$i]['PROJECT_MANAGER']."',
	// 					'".$toDb[$i]['DELIVERY_CENTER']."',
	// 					'".$toDb[$i]['ACTEUR']."',
	// 					'".$toDb[$i]['CAUSE']."',
	// 					'".$toDb[$i]['BUSINESSS_CASE']."',
	// 					'".$toDb[$i]['REPLANIF']."',
	// 					'".$toDb[$i]['PROJECT_DURATION']."',
	// 					'".$toDb[$i]['PMO']."',
	// 					'".$toDb[$i]['REMARKS']."',
	// 					'$_market',
	// 					'$_yearMonth'
						
	// 				)
	// 		";
	// 		die($query);
	// 	}else{			
	// 		$query = "UPDATE ReportingDBProd.[gsd].[RAW_DATA] 
	// 				SET
	// 					[CUSTOMER_NAME] = '".$toDb[$i]['CUSTOMER_NAME']."',
	// 					[PROJECT_NAME] = '".$toDb[$i]['PROJECT_NAME']."',
	// 					[COST_BASELINE] = '".$toDb[$i]['COST_BASELINE']."',
	// 					[REVISED_CBL] = '".$toDb[$i]['REVISED_CBL']."',


	// 					[ESTIMATED_COST] = '".$toDb[$i]['ESTIMATED_COST']."',
	// 					[TOTAL_COST] = '".$toDb[$i]['TOTAL_COST']."',
	// 					[PLANED_COST] ='".$toDb[$i]['PLANED_COST']."',
	// 					[PROJECT_START_DATE] = '".$toDb[$i]['PROJECT_START_DATE']."',
	// 					[ACTUAL_START_DATE] = '".$toDb[$i]['ACTUAL_START_DATE']."',
	// 					[PROJECT_END_DATE] = '".$toDb[$i]['PROJECT_END_DATE']."',



	// 					[ESTIMATED_COMPLETITION_DATE] = '".$toDb[$i]['ESTIMATED_COMPLETITION_DATE']."',
	// 					[PROJECT_PLANED_END_DATE] = '".$toDb[$i]['PROJECT_PLANED_END_DATE']."',
	// 					[PROJECT_ESTIMATED_COMPLETITION_DATE] = '".$toDb[$i]['PROJECT_ESTIMATED_COMPLETITION_DATE']."',

	// 					[PLANNED_SITE_TILL_P12] ='".$toDb[$i]['PLANNED_SITE_TILL_P12']."',
	// 					[ACTUAL_SITE_TILL_P12] = '".$toDb[$i]['ACTUAL_SITE_TILL_P12']."',
	// 					[RESULTAT] = '".$toDb[$i]['RESULTAT']."',
	// 					[PLANNED_MILESTONES] = '".$toDb[$i]['PLANNED_MILESTONES']."',
			
	// 					[ACTUAL_MILESTONES] = '".$toDb[$i]['ACTUAL_MILESTONES']."',
	// 					[BUSINESS_LINE] = '".$toDb[$i]['BUSINESS_LINE']."',
	// 					[NR_CHANGES_REQUEST_APPROVED] = '".$toDb[$i]['NR_CHANGES_REQUEST_APPROVED']."',


	// 					[TOTAL_WORK_PACKAGES_HAS_TO_WORK] ='".$toDb[$i]['TOTAL_WORK_PACKAGES_HAS_TO_WORK']."',

	// 					[TOTAL_WORK_PACKAGES_ACTUAL_WORKED] = '".$toDb[$i]['TOTAL_WORK_PACKAGES_ACTUAL_WORKED']."',


	// 					[PTA_BASED_ACTUAL_DOCUMENT] = '".$toDb[$i]['PTA_BASED_ACTUAL_DOCUMENT']."',
	// 					[SOLUTION_DOCUMENT] = '".$toDb[$i]['SOLUTION_DOCUMENT']."',
				

	// 					[VIRTUAL_ZERO] = '".$toDb[$i]['VIRTUAL_ZERO']."',
	// 					[CUSTOMER_TEAM_SURVEY] = '".$toDb[$i]['CUSTOMER_TEAM_SURVEY']."',
	// 					[IDX_GIC] = '".$toDb[$i]['IDX_GIC']."',
	// 					[GDN_NAME] ='".$toDb[$i]['GDN_NAME']."',


	// 					[PROJECT_MANAGER] = '".$toDb[$i]['PROJECT_MANAGER']."',
	// 					[DELIVERY_CENTER] = '".$toDb[$i]['DELIVERY_CENTER']."',
	// 					[BUSINESSS_CASE] = '".$toDb[$i]['BUSINESSS_CASE']."',
						
	// 					[PROJECT_DURATION] = '".$toDb[$i]['PROJECT_DURATION']."',
	// 					[PMO] = '".$toDb[$i]['PMO']."',
	// 					[REMARKS] = '".$toDb[$i]['REMARKS']."',


	// 					[MARKET] = '$_market',
	// 					[YEAR_MONTH] = '$_yearMonth',
				

	// 				WHERE [DATE] = $_yearMonth
	// 				AND [PROJECT_NAME] = '".$toDb[$i]['PROJECT_NAME']."'
	// 		";
	// 		die($query);
	// 	}

	// 	//die($query);
	// 	if($formatErrorsReport == 0){
	// 		$runSql = mssql_query($query);		
	// 	}else{
	// 		echo 'Format at errors! Check the hour column';
	// 	}

?>