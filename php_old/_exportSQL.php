<?php 
function exportSQL($sql, $filename = 'Export SQL', $sheetTitle = NULL, $ignoreList = NULL, $supressErrors=false)
  {
    include '/srv/data203386/sftp/jail/ftp/gnoc/mssql_auth/mssql_config.php';
    $conn  = $connection_string;

    if($conn)
    {
      include '../../../_libs/php/PHPExcel/PHPExcel.php';
      $objPHPExcel = new PHPExcel();
      $objPHPExcel->getProperties() ->setCreator("ALU Reporting Tool")
                                    ->setLastModifiedBy("ALU Reporting Tool");

      if(!is_array($sql))
        $sql = Array($sql);

      if(!is_array($sheetTitle))
        if($sheetTitle == NULL)
          $sheetTitle = Array();
        else
          $sheetTitle = Array($sheetTitle);      

      if(!is_array($ignoreList))
        if($ignoreList == NULL)
          $ignoreList = Array();
        else
          $ignoreList = Array($ignoreList);

      for($i = count($sheetTitle); $i < count($sql); ++$i)
        $sheetTitle[$i] = "Sheet ".($i+1);

      for($x = 0; $x < count($sql); ++$x)
      {
        $rs=mssql_query($sql[$x]);

        if (!$rs){
          exit($supressErrors==true ? '' : "MSSQL error on sheet $x: " . mssql_get_last_message());
        }
        $sheet = $x==0 ? $objPHPExcel->getActiveSheet() : $objPHPExcel->createSheet();
        $sheet ->setTitle($sheetTitle[$x]);
        $nf = mssql_num_fields($rs);
        for($i = 0, $column = 'A'; $i < $nf ; ++$i)
        {
          if(!in_array(mssql_field_name($rs, $i), $ignoreList))
          {
            $objPHPExcel->setActiveSheetIndex($x)->setCellValue($column."1", mssql_field_name($rs, $i));
            ++$column;
          }
        }
        for($i=2 ; $row = mssql_fetch_row($rs) ; ++$i)
          for($j=0, $column='A' ; $j<$nf ; ++$j)
          {
            if(!in_array(mssql_field_name($rs, $j), $ignoreList))
            {
              $objPHPExcel->setActiveSheetIndex($x)
                          ->setCellValue($column.$i, $row[$j]);
              $sheet->getColumnDimension($column)->setAutoSize(true);
              ++$column;
            }
          }

        $objPHPExcel->getActiveSheet()->getStyle('A1:' . $objPHPExcel->getActiveSheet()->getHighestColumn() . '1')
        ->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK
                )
            ),
           'font' => array(
              'bold'=>true
           ),
           'alignment' => array(
              'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
          )
        ));

        $objPHPExcel->getActiveSheet()->getStyle('A2:' . $objPHPExcel->getActiveSheet()->getHighestColumn() . $objPHPExcel->getActiveSheet()->getHighestRow())
        ->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
           'alignment' => array(
              'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
          )
        ));
      }

      $objPHPExcel->setActiveSheetIndex(0);

      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
      header('Cache-Control: max-age=0');
      header('Cache-Control: max-age=1');
      header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
      header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
      header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
      header ('Pragma: public'); // HTTP/1.0

      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
      $objWriter->save('php://output');
      exit();
    }
    else
       die();
 }
?>