<?php

include '/srv/data203386/sftp/jail/ftp/gnoc/mssql_auth/mssql_config.php';
// include '../markets/getData.php';
session_start();
$groupedBy = 'YEAR_MONTH';
// echo json_encode($arrFinal);
// print_r($ym);
//echo $_POST;

if ($_POST['groupedBy']) {
	$groupedBy = $_POST['groupedBy'];
}
$date = $_POST["date"];
$market = $_POST["market"];
$market = str_replace(",", "','", $market);
$reportId = $_POST['reportId'];
// print_r($market);
$businessLine = $_POST["businessLine"];
$businessLine = str_replace(",", "','", $businessLine);
if ($businessLine!= null) {
	$str_bl="BUSINESS_LINE IN ('$businessLine') AND";
	# code...
}else{
	$str_bl='';
}
// echo $date. " - ".$market. " - ". $businessLine;

$date = explode(':', $date);
$startDate = $date[0];
$endDate = $date[1];


// echo $startDate. " - ".$endDate;
if ($reportId == 18) {
	$sql = "USE ReportingDBProd
	SELECT MARKET AS YEAR_MONTH,
		COUNT(*) as [All_projects],
		COUNT(CASE WHEN DATEDIFF(month, ACTUAL_START_DATE, PROJECT_END_DATE) <= 3 THEN 1 ELSE NULL END) [GDC<100],
		COUNT(CASE WHEN DATEDIFF(month, ACTUAL_START_DATE, PROJECT_END_DATE) <= 6 AND DATEDIFF(month, ACTUAL_START_DATE, PROJECT_END_DATE) > 3 THEN 1 ELSE NULL END) [GDC100-500],
		COUNT(CASE WHEN DATEDIFF(month, ACTUAL_START_DATE, PROJECT_END_DATE) <= 12 AND DATEDIFF(month, ACTUAL_START_DATE, PROJECT_END_DATE) > 6 THEN 1 ELSE NULL END) [GDC500-1000],
		COUNT(CASE WHEN DATEDIFF(month, ACTUAL_START_DATE, PROJECT_END_DATE) > 12 THEN 1 ELSE NULL END) [GDC>1000]
						
			FROM (
				SELECT MARKET, ACTUAL_START_DATE, PROJECT_END_DATE,
							
				ROW_NUMBER() OVER(PARTITION BY CUSTOMER_NAME, PROJECT_NAME, BUSINESS_LINE ORDER BY  YEAR_MONTH DESC) AS [ROW_NR]
				FROM gsd.raw_data
							
				WHERE ACTUAL_START_DATE > CONVERT(datetime, '$startDate' + '-01', 120) AND ACTUAL_START_DATE < CONVERT(datetime, '$endDate' + '-01', 120)) a
	WHERE [ROW_NR]=1
	GROUP BY MARKET
	ORDER BY MARKET";
} else {
	$sql = "USE ReportingDBProd
	SELECT  $groupedBy AS YEAR_MONTH, count(*) as [All_projects],COUNT(CASE WHEN COST_BASELINE<100 THEN 1 ELSE NULL END) AS [GDC<100]
						,COUNT(CASE WHEN COST_BASELINE>=100 AND COST_BASELINE<500 THEN 1 ELSE NULL END) AS [GDC100-500]
						,COUNT(CASE WHEN COST_BASELINE>=500 AND COST_BASELINE<1000 THEN 1 ELSE NULL END) AS [GDC500-1000]
						,COUNT(CASE WHEN COST_BASELINE>=1000 THEN 1 ELSE NULL END) AS [GDC>1000]
						
						 FROM (SELECT $groupedBy, CASE WHEN COST_BASELINE NOT LIKE '-100' 
									THEN ISNULL([COST_BASELINE],COST_BASELINE)
								WHEN (ISNUMERIC(COST_BASELINE) = 0 OR REVISED_CBL = 0) 
									THEN ISNULL([COST_BASELINE],COST_BASELINE)
						ELSE CAST(COST_BASELINE AS FLOAT)
							END as COST_BASELINE 
							,ROW_NUMBER() OVER(PARTITION BY CUSTOMER_NAME, PROJECT_NAME, BUSINESS_LINE ORDER BY  YEAR_MONTH DESC) AS [ROW_NR]
							FROM [gsd].[raw_data] 
							
							WHERE $str_bl
	MARKET IN ('$market') 
	AND CONVERT(VARCHAR(15), YEAR_MONTH, 120) BETWEEN '$startDate' AND '$endDate') a
	WHERE [ROW_NR]=1
	GROUP BY $groupedBy
	ORDER BY $groupedBy";
}
// die($sql);
$runSql = mssql_query($sql);
$i=0;
$j = 0;

while ($row = mssql_fetch_assoc($runSql)){
	foreach ($row as $key => $value) {
		$row[$key] = $value;
		// $arr[$j]['name'] = $key;
		// $arr[$j]['value'] = $value;
		// $finalData[$i][$j] = $arr[$j];
		// $j++;
	}
	// $j = 0;
	// $i++;		
	$arr[] = $row;
	// echo $arr["0"]->All_projects;
}
// $finalArr[] = 
// print_r($arr);
// die();

$_SESSION['arrToExport'] = $arr;

echo json_encode($arr);

?>