<?php

include '/srv/data203386/sftp/jail/ftp/gnoc/mssql_auth/mssql_config.php';

$date = $_POST["date"];
$date = explode(':', $date);
$startDate = $date[0];
$endDate = $date[1];

$market = $_POST["market"];
$market = str_replace(",", "','", $market);


$sqlPCA = "USE ReportingDBProd
SELECT FORMULA FROM [gsd].[KPI_FORMULA]
WHERE KPI_NAME = 'COST_PCA_ACTUAL'";

$sqlEAC = "USE ReportingDBProd
SELECT FORMULA FROM [gsd].[KPI_FORMULA]
WHERE KPI_NAME = 'COST_PCA_EAC'";
// die($sql);
$runSqlPCA = mssql_query($sqlPCA);
$runSqlEAC = mssql_query($sqlEAC);

$costKPIsqlCase = "";//. mssql_fetch_assoc($runSqlEAC);
foreach (mssql_fetch_assoc($runSqlPCA) as $key => $value) {
	$costKPIsqlCase .= $value. ", ";
}

foreach (mssql_fetch_assoc($runSqlEAC) as $key => $value) {
	$costKPIsqlCase .= $value;
}

// print_r($costKPIsqlCase);



$sqlCost = "USE ReportingDBProd SELECT YEAR_MONTH, ROUND(CAST(COST_BASELINE AS FLOAT)/PLANED_COST,2) AS COST_PCA_ACTUAL,
			ROUND(CAST(PLANED_COST AS FLOAT)/RECHARGE_COST,2) AS COST_PCA_EAC
			FROM (
				SELECT YEAR_MONTH, SUM(TOTAL_COST) AS RECHARGE_COST, SUM(REVISED_CBL) AS SOLUTION_COST,
				SUM(ESTIMATED_COST) AS ESTIMATED_COST, SUM(PLANED_COST) AS PLANED_COST, SUM(CAST(COST_BASELINE AS FLOAT)) AS COST_BASELINE
				FROM [gsd].[raw_data]
				WHERE market = '" . $market . "'
				AND CONVERT(VARCHAR(15), YEAR_MONTH, 120) BETWEEN '" . $startDate . "' AND '" . $endDate . "'
				GROUP BY YEAR_MONTH
			) a
			ORDER BY YEAR_MONTH ASC";

// "USE ReportingDBProd 
// SELECT TOTAL_COST AS RECHARGE_COST, REVISED_CBL AS SOLUTION_COST, ESTIMATED_COST, PLANED_COST, " . $costKPIsqlCase . " 
// FROM [gsd].[raw_data]
// WHERE market = '" . $market . "'
// AND CONVERT(VARCHAR(15), YEAR_MONTH, 120) BETWEEN '$startDate' AND '$endDate'
// ORDER BY YEAR_MONTH";

// print_r($sqlCost);

// die($sqlCost);

$costQuery = mssql_query($sqlCost);

while ($row = mssql_fetch_assoc($costQuery)){
	foreach ($row as $key => $value) {
		$row[$key] = $value;
	}
	$arr[] = $row;
}
// print_r($arr);

echo json_encode($arr);
// foreach (mssql_fetch_assoc($costQuery) as $key => $value) {
// 	print_r($key . " - " . $value);
// }

?>