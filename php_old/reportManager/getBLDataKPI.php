<?php

include '/srv/data203386/sftp/jail/ftp/gnoc/mssql_auth/mssql_config.php';
require_once('../markets/functionExecuteData.php');
require_once('../markets/getConsolidatedView.php');

session_start();
// echo json_encode($arrFinal);

// echo $_POST;
$upi = $_SESSION['GOV_UPI'];
$date = $_POST["date"];

$market = $_POST["market"];
$market = str_replace(",", "','", $market);

$type = $_POST['type'];
$type = substr($type, 0, strpos($type, " "));

$businessLine = $_POST["businessLine"];
$businessLine = str_replace(",", "','", $businessLine);

if ($businessLine!= null) {
	$str_bl="BUSINESS_LINE IN ('$businessLine') AND";
	# code...
}else{
	$str_bl='';
}
// echo $date. " - ".$market. " - ". $businessLine;

//Get data with validations
$arrBefore=executeData($upi,$date,$market,false,false);

$date = explode(':', $date);
$startDate = $date[0];
$endDate = $date[1];

$arrBLTotal = $arrBefore[0];

// echo json_encode($arrBLTotal);
// die();
// echo $startDate. " - ".$endDate;


$sql = "USE ReportingDBProd
SELECT  YEAR_MONTH, count(*) as [All_projects], 
		COUNT(CASE WHEN PROJECT_END_DATE BETWEEN YEAR_MONTH + '-01' AND YEAR_MONTH + '-' + EOM THEN 1 ELSE NULL END) AS [CLOSED_PROJECTS]		
					
	 	FROM (SELECT YEAR_MONTH, PROJECT_END_DATE,
			CONVERT (varchar(20), DAY(DATEADD(day, -day(CONVERT( VARCHAR(30), YEAR_MONTH+'-01' ,120 )), dateadd(month,1,CONVERT( VARCHAR(30), YEAR_MONTH+'-01' ,120 ))))) AS EOM
			FROM [gsd].[raw_data]
			
						
			WHERE $str_bl
			MARKET IN ('$market') 
			AND CONVERT(VARCHAR(15), YEAR_MONTH, 120) BETWEEN '$startDate' AND '$endDate') a
GROUP BY YEAR_MONTH
ORDER BY YEAR_MONTH";
// die($sql);
$runSql = mssql_query($sql);

// print_r($arrBLTotal[0]);

//Get from arrBLTotal only 'Total' entries
foreach ($arrBLTotal as $entry) {
	if ($entry['BUSINESS_LINE'] == 'Total') {
		$blTotal[] = $entry;
	}
}

// echo json_encode($blTotal);
// die();

$i=0;

while ($row = mssql_fetch_assoc($runSql)){
	foreach ($row as $key => $value) {
		$row[$key] = $value;
	}

	$row["COST_GREEN"] = $blTotal[$i]["COST_GREEN"];
	$row["TIME_GREEN"] = $blTotal[$i]["TIME_GREEN"];
	$row["SCOPE_GREEN"] = $blTotal[$i]["SCOPE_GREEN"];
	$row["QUALITY_GREEN"] = $blTotal[$i]["QUALITY_GREEN"];

	$arr[] = $row;
	$i = $i + 1;
}
// print_r($arr);
// die();
$_SESSION['arrToExport'] = $arr;
// print_r($_SESSION['arrToExport']);
echo json_encode($arr);

?>