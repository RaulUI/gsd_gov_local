<?php

ini_set('memory_limit', '4096M');
header('Content-Type: application/json');

include '/srv/data203386/sftp/jail/ftp/gnoc/mssql_auth/mssql_config.php';
include "../PHPExcel/Classes/PHPExcel/IOFactory.php";

session_start();

ini_set('mssql.charset', 'UTF-8');

$category = $_POST['category'];
$id = $_POST['id'];
$ym = $_POST['ym'];
$projectName = $_POST['Project_Name'];

$_SESSION['graphId'] = $id;

foreach($_POST['region'] AS $region ) {
    $regions .= "''".$region."'',";
}  

$regions = substr($regions, 0 , -1);
//die($regions);
if($regions=="''''"){
	$regions="''MEA'',''EUR'',''IND'',''GCHN'',''APJ'',''LAT'',''NAM''";
}
$sql="USE ReportingDBProd
		SELECT DESCR, cast(QUERY as text) as QUERY, ISFUNCTION FROM [gdc].[GDC_GOV_REPORTS] WHERE IDX = ".$id."";
//die($sql);
$runSql = mssql_query($sql);

while ($row = mssql_fetch_assoc($runSql)){
	$arr = $row;
}








if($arr['ISFUNCTION'] == 0){

	//This is Mapping WBS SVO GIC COST Export
	if($id == 12){

		$arr = str_replace('@param1', $ym, $arr);
		$arr = str_replace('@param2', $regions, $arr['QUERY']);
		$arr = str_replace('@param3', $projectName, $arr);
		
		// print_r($arr);
		// die();

		// include '../../../_libs/php/Utilities/_exportSQL.php';
		// exportSQL($arr, 'Mapping WBS SVO GIC COST' , 'Sheet1');
		// die();
		// print_r($arr);
		// die();

		$runSql = mssql_query($arr);

		while ($row = mssql_fetch_assoc($runSql)){
			// echo $row;
			$arr2[] = $row;
		}
		// print_r($arr2);
		// die();


		//Excell Columns
		$row = array(
			'Customer Name',
			'CID',
			'Project Name',
			'Business Line',
			'Market',
			'WBS',
			'SVO',
			'GIC',
			'Cost'
		);

		$doc = new PHPExcel();

		for ($i = 0; $i < 2; $i++) {

			//Handle creation of sheet, else it would create an additional sheet
			if ($i != 0) {
				$doc->createSheet($i);
			}

			$doc->setActiveSheetIndex($i);

			//Set sheet name
			if ($i == 1) {
				$doc->getActiveSheet()->setTitle('Historical old data');
			} else if ($i == 0) {
				$doc->getActiveSheet()->setTitle('WBS - SVO - GIC - COST');
			}

			//Change excel data for the second sheet
			if ($i == 1) {

				$tmpRegions = $regions;
				$markets = str_replace('\'\'', '\'', $tmpRegions);

				//Data for second sheet from sql
				$sqlForOld = 'SELECT a.Customer_Name, CID, Project_Name, Business_Line, a.Market, WBS, SVO, IDX_GIC, PLANED_COST
					FROM [gsd].[raw_data] a
					LEFT JOIN [gsd].[MAPPING_WBS] b ON a.IDX = b.IDX_WBS
					LEFT JOIN [gsd].[MAPPING_SVO] c ON a.IDX = c.IDX_SVO
					LEFT JOIN [gdc].[GDC_GOV_CUSTOMERS] d ON a.CUSTOMER_NAME = d.CUSTOMER_NAME
					WHERE YEAR_MONTH = \'2018-01\'
					AND a.MARKET IN (' . $markets . ')
					';

				$runSql = mssql_query($sqlForOld);

				//Save data for second sheet in arr2
				while ($data = mssql_fetch_assoc($runSql)){
					// echo $row;
					$arr2[] = $data;
				}

			}

			//Populate data in excel
			for ($j=0; $j < count($arr2); $j++) { 
				foreach ($arr2[$j] as $key => $value) {
					if($value == 'NULL'){
						$value = '';
					}
					$arr2[$j][$key]=$value;
				}
			}


			// print_r($arr);
			// print_r($arr2);
			// die();
			// $rows = $doc->getActiveSheet()->fromArray(array_keys($arr[0]),NULL,'A2');
			$doc->getActiveSheet()->fromArray($row, null, 'A1');
			$doc->getActiveSheet()->fromArray($arr2, null, 'A2');


			foreach(range('A','I') as $columnID) {

			    $doc->getActiveSheet()->getColumnDimension($columnID)
			        ->setAutoSize(true);

			}


			// $style = array(
			//     'alignment' => array(
			//         'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			//     )
			// );

			$doc->getActiveSheet()->getStyle('A1:' . $doc->getActiveSheet()->getHighestColumn() . '1')
	        ->applyFromArray(array(
	            'borders' => array(
	                'allborders' => array(
	                    'style' => PHPExcel_Style_Border::BORDER_THICK
	                )
	            ),
	           'font' => array(
	              'bold'=>true
	           ),
	           'alignment' => array(
	              'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
	          )
	        ));

	        $doc->getActiveSheet()->getStyle('A2:' . $doc->getActiveSheet()->getHighestColumn() . $doc->getActiveSheet()->getHighestRow())
	        ->applyFromArray(array(
	            'borders' => array(
	                'allborders' => array(
	                    'style' => PHPExcel_Style_Border::BORDER_THIN
	                )
	            ),
	           'alignment' => array(
	              'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
	          )
	        ));

	        //Define the columns id from excel to color
	        $columnsIDtoColor = array('F', 'G', 'H', 'I');

	        //Color cells and change font olny for Selected month sheet
	        if ($i == 0) {
		        foreach ($columnsIDtoColor as $key => $value) {

		        	//Cell Fill style
			        $doc->getActiveSheet()->getStyle($value . '2:' . $value . (sizeof($arr2)+1))->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => '1D94BF')
					        )
					    )
					);

		        	//Cell Font style
					$doc->getActiveSheet()->getStyle($value . '2:' . $value . (sizeof($arr2)+1))->applyFromArray(
					    array(
					        'font' => array(
					            'color' => array('rgb' => '2D3C3F')
					        )
					    )
					);
		        }
	    	}
    	}

    	$doc->setActiveSheetIndex(0 );
		// $doc->getDefaultStyle()->applyFromArray($style);
		// $doc->getActiveSheet()->getStyle('1:1')->getFont()->setBold(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Export Raw Data.xls"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$writer = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

		$writer->save('php://output');
	}










	//This is Export Rawdata Export
	else if($id == 1){
// print_r($arr);
		// die();

		$arr = str_replace('@param1',$ym,$arr);
		$arr = str_replace('@param2',$regions,$arr['QUERY']);

		 //print_r($arr);
		 //die();
		$runSql = mssql_query($arr);

		while ($row = mssql_fetch_assoc($runSql)){
			// echo $row;
			$arr2[] = $row;
		}
		// print_r($arr2);
		// die();


		// $runFinal = mssql_query($arr);

	// 	$row = array('#',
	// 		'Country Of Customer',
	// 		'Customer Name',
	// 		'Project Name',
	// 		'Cost Baseline Value in K Euro',
	// 		'Revised CBL',
	// 		'Estimated Cost at Completion E',
	// 		'Total Cost (K Euro) charged Actual from F&C report base on WBS/Svo/GIC]',
	// 		'Planned Cost (Till (n-1) End)',
	// 		//'PCA % (Actual)- Formula',
	// 		'CBL Consume Cum(n-1)',
	// 		'PCA% (EAC) - Formula',
	// 		'Project Planned Start Date (Month)',
	// 		'Project Actual Start Date (Month)',
	// 		'Project Planned End Date (Month)',
	// 		'Estimated Project Completion Date',
	// 		//'Project Planned End Date (Duration in days)',
	// 		//'Estimated Project Completion Date (Duration in days)',
	// 		'Schedule Adherence -EAC (%)- Formula',
	// 		'Planned Sites till P'.((int)$month-1).'.(NI/NPO)/Planned Milestones till P'.((int)$month-1).' (SI)',
	// 		'Actual Sites till P'.((int)$month-1).' (NI/NPO)/Actual Milestones till P'.((int)$month-1).' (SI)',
	// 		'Total Milestones',
	// 		'Revised Milestones',
	// 		'Project Completion -Actual %- Formula',
	// 		'Business Line (excel Care)',
	// 		'No. of Changes Requests Aproved',
	// 		'Competence Adherence (Against SSD) ',
	// 		'Total Work Packages on which GDC has to work',
	// 		'Work Packages on which GDC has actually worked',
	// 		'Remote Service Catalog Compliance Formula',
	// 		'Scope Creep',
	// 		//'PTA Sign-off (based on BC approval & SOW/WLA agreement)',
	// 		'PTA Sign-off (actual Document in PMO Onboarding)',
	// 		// 'Solution Document Available (Y/N)',
	// 		"SSD or OLA Avbl (Y/N)",
	// 		'Virtual Zero Delivery Index (n-1)',
	// 		'Customer Team Survey(CTS)',
	// 		'WBS/Project Code as per fNok FTE report or Lawson tool',
	// 		'Service Order (SvO) Number as per FTE report or Lawson for fAlu',
	// 		'GIC Code',
	// 		'GDM Name',
	// 		'Project Manager',
	// 		'Delivery Center',
	// 		'Business Case Available Y/N',
	// 		'Project Duration Baseline in M',
	// 		'PMO',
	// 		'Remarks',
	// 		'TOP 32 PROJECTS',
	// 		'Former ALU/NOKIA',
	// 		'MARKET',
	// 		'YEAR MONTH'
	// );

		$row = array('#',
			'Country Of Customer',
			'Customer Name',
			'Project Name',
			'Cost Baseline Value in K Euro',
			'Revised CBL',
			'Estimated Cost at Completion E',
			'Total Cost (K Euro) charged Actual from F&C report base on WBS/Svo/GIC]',
			'Planned Cost (Till (n-1) End)',
			// 'PCA % (Actual)- Formula',
			'CBL Consume Cum(n-1)',
			'PCA% (EAC) - Formula',
			'Project Planned Start Date (Month)',
			'Project Actual Start Date (Month)',
			'Project Planned End Date (Month)',
			'Estimated Project Completion Date',
			'Schedule Adherence -EAC (%)- Formula',
			'Planned Sites till P7.(NI/NPO)/Planned Milestones till P7 (SI)',
			'Actual Sites till P7 (NI/NPO)/Actual Milestones till P7 (SI)',
			'Project Completion -Actual%- Formula',
			'Business Line (excel Care)',
			'No. of Changes Requests Aproved',
			'Competence Adherence (Against SSD) ',
			'Total Work Packages on which GDC has to work',
			'Work Packages on which GDC has actually worked',
			'Remote Service Catalog Compliance Formula',
			'Scope Creep',
			'PTA Sign-off (actual Document in PMO Onboarding)',
			'Solution Document Available (Y/N)',
			'Virtual Zero Delivery Index (n-1)',
			'Customer Team Survey(CTS)',
			'WBS/Project Code as per fNok FTE report or Lawson tool',
			'Service Order (SvO) Number as per FTE report or Lawson for fAlu',
			'GIC Code',
			'GDM Name',
			'Project Manager',
			'Delivery Center',
			'Business Case Available Y/N',
			'Project Duration Baseline in M',
			'PMO',
			'Remarks',
			'TOP 32 PROJECTS',
			'Market',
			'Month',
			'PCA actual',
			'PCA EAC',
			'RSC option',
			'PCA act. Completion',
			'Schedule Adherance',
			'VZ',
			'SSD',
			'On cost KPI',
			'On schedule KPI',
			'On scope KPI',
			'On quality KPI',
		);

		$doc = new PHPExcel();
		$doc->setActiveSheetIndex(0);
		for ($i=0; $i < count($arr2); $i++) { 
			foreach ($arr2[$i] as $key => $value) {
				if($value == 'NULL'){
					$value = '';
				}
				if($key=='COST_1' || $key=='TIME_1' || $key=='SCOPE_1' || $key=='QUALITY_1'){
					if($value == '1'){
						$value = 'GREEN';
					}else if($value == '2'){
						$value = 'AMBER';
					}else if($value == '3'){
						$value = 'RED';
					}
				}
				if(strpos($key , '%') && $value != '-100' && $value != 'NA' && $value != ''){
					$value = $value.'%';
				}
				if($value=='-100'){
					$value='NA';
				}else if($value!='NA' && $value!='' && ($key=='COST_PCA_ACTUAL' || $key=='COST_PCA_EAC' || $key=='TIME_SCHEDULE' || $key=='TIME_PROJECT' || $key=='SCOPE_REMOTE') ){
					$value=$value.' %';
				}
				$arr2[$i][$key]=$value;
			}
		}
		// die();
		// print_r($arr);
		// die();
		// $rows = $doc->getActiveSheet()->fromArray(array_keys($arr[0]),NULL,'A2');
		$doc->getActiveSheet()->fromArray($row, null, 'A1');
		$doc->getActiveSheet()->fromArray($arr2, null, 'A2');
		// die();
		foreach(range('A','AR') as $columnID) {

		    $doc->getActiveSheet()->getColumnDimension($columnID)
		        ->setAutoSize(true);

		}
		$style = array(
		    'alignment' => array(
		        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		    )
		);

		$doc->getDefaultStyle()->applyFromArray($style);
		$doc->getActiveSheet()->getStyle('1:1')->getFont()->setBold(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Export Raw Data.xls"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$writer = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

		$writer->save('php://output');
















		//This is Consolidated Report Export
	}else if($id == 2){

		$arr = str_replace('@param1',$ym,$arr);

	// print_r($arr);

		$runSql = mssql_query($arr['QUERY']);

		while ($row = mssql_fetch_assoc($runSql)){
			$arr2[] = $row;
		}
		// print_r($arr2);
		// die();


		// $runFinal = mssql_query($arr);

		$row = array(
			'Market',
			'Total Projects',
			'Proj (Excl Care)',
			'PTA Signed',
			'Proj (T 32)',
			'<100',
			'100 - 500',
			'500 -1000',
			'>1000',
			'On Cost(Red)',
			'On Cost(Amber)',
			'On Cost(Green)',
			'On Schedule(Red)',
			'On Schedule(Amber)',
			'On Schedule(Green)',
			'OnScope(Red)',
			'OnScope(Amber)',
			'OnScope(Green)',
			'On Quality(Red)',
			'On Quality(Amber)',
			'On Quality(Green)',
			'Unique WBS as per GSDM Dashboard',
			'Unique WBS as per FTE report',
			'Common WBS',
			'Key Observations',
			'New Projects Count',
			'PTA signed New Projects',
			'Period',
		);

		$doc = new PHPExcel();
		$doc->setActiveSheetIndex(0);
		for ($i=0; $i < count($arr2); $i++) { 
			foreach ($arr2[$i] as $key => $value) {
				if($value == 'NULL'){
					$value = '';
				}
				$arr2[$i][$key]=$value;
			}
		}
		//die();
		// print_r($arr);
		// die();
		// $rows = $doc->getActiveSheet()->fromArray(array_keys($arr[0]),NULL,'A2');
		$doc->getActiveSheet()->fromArray($row, null, 'A1');
		$doc->getActiveSheet()->fromArray($arr2, null, 'A2');
		
		// ;

		foreach(range('A','AR') as $columnID) {

		    $doc->getActiveSheet()->getColumnDimension($columnID)
		        ->setAutoSize(true);

		}
		$style = array(
		    'alignment' => array(
		        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		    )
		);

		$doc->getDefaultStyle()->applyFromArray($style);
		$doc->getActiveSheet()->getStyle('1:1')->getFont()->setBold(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Consolidated Report.xls"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$writer = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

		$writer->save('php://output');
	
















	
	//This is Comments Report Export
	}else if($id == 4){
		$arr = str_replace('@param1',$ym,$arr);
		$arr = str_replace('@param2',$regions,$arr['QUERY']);

		// print_r($arr);
		// die();
		// $runSql = mssql_query($arr);

		// while ($row = mssql_fetch_assoc($runSql)){
		// 	$arr2[] = $row;
		// /}
		//print_r($arr2);
		include '../../../_libs/php/Utilities/_exportSQL.php';
		exportSQL($arr, 'Export' , 'Sheet1');
	} 





	//Used to get YM and Market as filters
	else if ($id == 15) {

		$arr = str_replace('@param1', $ym, $arr);
		$arr = str_replace('@param2', $regions, $arr);

		//$regions = str_replace('\'\'', '\'', $regions);
		// print_r($arr);
		// die();

		include '../../../_libs/php/Utilities/_exportSQL.php';
		exportSQL($arr['QUERY'], 'Export' , 'Sheet1');

	}



	//Used to get YM, YM -1 month and Market as filters
	//This is NOT Updated Total Cost Report
	//This is Audit Report And NOT Updated Projects Report
	else if ($id == 17 || $id == 16) {

		$arr = str_replace('@param1', $ym, $arr);
		$arr = str_replace('@param2', $regions, $arr);
		$arr = str_replace('@param3', getPreviousMonthFromYM($ym), $arr);

		// print_r(getPreviousMonthFromYM($ym));
		// die();
		//$regions = str_replace('\'\'', '\'', $regions);
		// print_r($arr);
		// die();

		include '../../../_libs/php/Utilities/_exportSQL.php';
		exportSQL($arr['QUERY'], 'Export' , 'Sheet1');

	}




}else if ($arr['ISFUNCTION'] == 1){
	$function = $arr['QUERY'];
	echo json_encode($arr);
	
}else if ($arr['ISFUNCTION'] == 2){
	$function = $arr['QUERY'];
	echo json_encode($arr);
}else if ($arr['ISFUNCTION'] == 3){
	$function = $arr['QUERY'];
	echo json_encode($arr);
}else if ($arr['ISFUNCTION'] == 4){
	$function = $arr['QUERY'];
	echo json_encode($arr);
}



// echo json_encode($arr);

/*
SELECT reports.id, reports.name, categories. from 
*/

// REPORTS
// CATEGORIES_REPORTS_CONNECTIONS
// FILTERS_REPORTS_CONNECTIONS
	function getPreviousMonthFromYM ($ym) {
		$pastYM = new DateTime($ym);
		$pastYM->modify( 'first day of last month' );

		return $pastYM->format( 'Y-m' );
	}
?>