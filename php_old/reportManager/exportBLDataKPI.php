<?php
include '/srv/data203386/sftp/jail/ftp/gnoc/mssql_auth/mssql_config.php';
// require_once('getBLDataKPI.php');
include '../../../_libs/php/PHPExcel/PHPExcel.php';

session_start();

$graphId = $_SESSION['graphId'];
$sessionArr = $_SESSION['arrToExport'];

//print_r($sessionArr);
//die();

//Create new excel file
$doc = new PHPExcel();
$doc->setActiveSheetIndex(0);


//This is for KPI from BL Wise charts
if ($graphId > 6) {

	//Headers
	$row = array(
		'YEAR_MONTH'=>'YEAR_MONTH'
		,'ALL_PROJECTS'=>'ALL_PROJECTS'
		,'COST_GREEN'=>'COST_GREEN'
		,'QUALITY_GREEN'=>'QUALITY_GREEN'
		,'SCOPE_GREEN'=>'SCOPE_GREEN'
		,'TIME_GREEN'=>'TIME_GREEN');

	$arra[] = $row;

	foreach ($sessionArr as $key => $value) {
		$arra[] = $value;
	}

	// print_r($arra);
	// die();

	
	// $rows = $doc->getActiveSheet()->fromArray(array_keys($arra[0]),NULL,'A2');

	$doc->getActiveSheet()->fromArray($arra, null, 'A1');

	foreach(range('A','F') as $columnID) {

	    $doc->getActiveSheet()->getColumnDimension($columnID)
	        ->setAutoSize(true);

	}
	
	//Wise charts export
} else if ($graphId <= 6) {
	//Headers
	$row = array(
		'YEAR_MONTH'=>'YEAR_MONTH'
		,'ALL_PROJECTS'=>'ALL_PROJECTS'
		,'GDC<100'=>'GDC<100'
		,'GDC100-500'=>'GDC100-500'
		,'GDC500-1000'=>'GDC500-1000'
		,'GDC>1000'=>'GDC>1000');

	$arra[] = $row;

	foreach ($sessionArr as $key => $value) {
		$arra[] = $value;
	}

	// print_r($arra);
	// die();

	
	// $rows = $doc->getActiveSheet()->fromArray(array_keys($arra[0]),NULL,'A2');

	$doc->getActiveSheet()->fromArray($arra, null, 'A1');

	foreach(range('A','F') as $columnID) {

	    $doc->getActiveSheet()->getColumnDimension($columnID)
	        ->setAutoSize(true);

	}
}

//Style thick header
$doc->getActiveSheet()->getStyle('A1:' . $doc->getActiveSheet()->getHighestColumn() . '1')
	->applyFromArray(array(
	    'borders' => array(
	        'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_THICK
        )
    ),
   'font' => array(
      'bold'=>true
   ),
   'alignment' => array(
      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
  )
));

$doc->getActiveSheet()->getStyle('A2:' . $doc->getActiveSheet()->getHighestColumn() . $doc->getActiveSheet()->getHighestRow())
->applyFromArray(array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    ),
   'alignment' => array(
      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
  )
));

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Bonus/Malus Export.xls"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$writer = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

$writer->save('php://output');

?>