<?php
  	include '/srv/data203386/sftp/jail/ftp/gnoc/mssql_auth/mssql_config.php';

	$query = "
		USE ReportingDbProd
		SELECT c.MONTH_NAME, MEA, EUR, IND, GCHN, APJ, LAT, NAM FROM(
			SELECT MONTH_NAME, MARKET, DATE FROM (
				SELECT MARKET,DATE,MONTH, MONTH_NAME FROM GSD.MARKETS_SNAP_DATE a
				JOIN GSD.MONTHS b ON a.MONTH = b.MONTH_NR
			)F
		)FF
		PIVOT(
			MAX(DATE)
			FOR MARKET IN ([MEA], [EUR], [IND], [GCHN], [APJ], [LAT], [NAM])
		)ff
		RIGHT JOIN GSD.MONTHS c ON ff.MONTH_NAME = c.MONTH_NAME
	";

	$data = mssql_query($query);

	while($result = mssql_fetch_assoc($data) ){
		$arr[] = $result;          
	}

	if (json_encode($arr) == 'null'){
		//echo '[{"REGION":"No Entry!"}]';
	}else{
		echo json_encode($arr);
	}

?>