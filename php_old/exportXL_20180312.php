<?php

include '_exportSQL.php';
include '_exportArrayToExcel.php';
include '/srv/data203386/sftp/jail/ftp/gnoc/mssql_auth/mssql_config.php';

include "PHPExcel/Classes/PHPExcel/IOFactory.php";


$upi = $_SESSION['GOV_UPI'];
$view = $_GET["view"];
$date=$_GET["date"];
$region = $_GET["region"];
$market = $_GET["market"];
$ym=$_GET["date"];

$ymWithDay = $ym . '-01';
$month=substr($date, 5,2);

$year = substr($ym, 0, 4);
$month = substr($ym, 5);
$nextMonth = intval($month);
	$nextMonth = $nextMonth + 1;
	
	if($nextMonth < 10){
		$nextMonth = '0' . $nextMonth;
	}else if($nextMonth > 12){
		$year = $year + 1;
	}
$ymWithDay = $year. '-' . $nextMonth . '-' . '01';
$ymWithDayCrt = $year. '-' . $month . '-' . '01';
//echo (int)$month-1;die();
/*	echo'upi-';
	echo $upi;
	echo'-upi';
	echo $view;
	echo $date;
	echo $region;
	echo $ym;
	echo $ymWithDay;
	die();*/

if($view == 'Executive Dashboard'){
	if ($region != 'ALL'){
		include 'markets/functionExecuteData.php';
		$objTpl = PHPExcel_IOFactory::load("executive_template.xlsx");
		$objTpl->setActiveSheetIndex(0);

		$mustHave = array(" ","  ","Total", "MS", "NI", "NPO", "SI", "MULTI BL");

		$arrBefore=executeData($upi,$ym,$region);

		$arr=$arrBefore[0];
		$arrNames=$arrBefore[0];

	//die($sql);
	$month=explode('-',$date);
	$objTpl->getActiveSheet()->setCellValue('B2','Executive Dashboard - '.$region.' (P'.$month[1].')');
		//print_r($arr);die();
	  $i=5;
	  for ($k=0;$k<=count($arr);$k++)
		{
			$j=0;
			if($i<13)
			{
				if($arr[$k]['BUSINESS_LINE'] == '  ')
					$j = 5;
				if($arr[$k]['BUSINESS_LINE'] == 'MS')
					$j = 7;
				if($arr[$k]['BUSINESS_LINE'] == 'NI')
					$j = 8;
				if($arr[$k]['BUSINESS_LINE'] == 'SI')
					$j = 9;
				if($arr[$k]['BUSINESS_LINE'] == 'NPO')
					$j = 10;
				if(strtoupper($arr[$k]['BUSINESS_LINE']) == 'MULTI BL')
					$j = 11;
				if($arr[$k]['BUSINESS_LINE'] == 'Total')
					$j = 12;
				if($j != 0)
				{
					if($j == 12)
					$objTpl->getActiveSheet()->setCellValue('E5',$arr[$k]['PROJ_32']);
					$objTpl->getActiveSheet()->setCellValue('C'.$j,$arr[$k]['PROJ_EXCL_CARE']);
					$objTpl->getActiveSheet()->setCellValue('D'.$j,$arr[$k]['PTA_SIGNED']);
					$objTpl->getActiveSheet()->setCellValue('E'.$j,$arr[$k]['PROJ_32']);
					$objTpl->getActiveSheet()->setCellValue('F'.$j,$arr[$k]['GDC_100']);
					$objTpl->getActiveSheet()->setCellValue('G'.$j,$arr[$k]['GDC_1_3']);
					$objTpl->getActiveSheet()->setCellValue('H'.$j,$arr[$k]['GDC_3_5']);
					$objTpl->getActiveSheet()->setCellValue('I'.$j,$arr[$k]['GDC_500']);
					$objTpl->getActiveSheet()->setCellValue('J'.$j,$arr[$k]['COST_RED']);
					$objTpl->getActiveSheet()->setCellValue('K'.$j,$arr[$k]['COST_AMBER']);
					$objTpl->getActiveSheet()->setCellValue('L'.$j,$arr[$k]['COST_GREEN']);
					$objTpl->getActiveSheet()->setCellValue('M'.$j,$arr[$k]['TIME_RED']);
					$objTpl->getActiveSheet()->setCellValue('N'.$j,$arr[$k]['TIME_AMBER']);
					$objTpl->getActiveSheet()->setCellValue('O'.$j,$arr[$k]['TIME_GREEN']);
					$objTpl->getActiveSheet()->setCellValue('P'.$j,$arr[$k]['SCOPE_RED']);
					$objTpl->getActiveSheet()->setCellValue('Q'.$j,$arr[$k]['SCOPE_AMBER']);
					$objTpl->getActiveSheet()->setCellValue('R'.$j,$arr[$k]['SCOPE_GREEN']);
					$objTpl->getActiveSheet()->setCellValue('S'.$j,$arr[$k]['QUALITY_RED']);
					$objTpl->getActiveSheet()->setCellValue('T'.$j,$arr[$k]['QUALITY_AMBER']);
					$objTpl->getActiveSheet()->setCellValue('U'.$j,$arr[$k]['QUALITY_GREEN']);
				}
				$i++;
			}

		
		//$nr_supp_ev=$nr_supp_ev+1;
	  }	
	  $redRemarks = array(
		'font'  => array(
	        'bold'  => true,
	        'color' => array('rgb' => 'FF0000'),
	        'size'  => 8
	));
	  $query = "USE ReportingDBProd
			SELECT REMARK from [ReportingDBProd].[gsd].[MARKET_REMARKS]
			WHERE YEAR_MONTH LIKE '$ym' AND MARKET LIKE '$region'
		";
		//die($query);
	  $rs = mssql_query($query);
	  while ($row=mssql_fetch_array($rs))
		{
			$arr2[] = $row[0]; 
			//print_r($row);die();
		}
		//print_r($arr);die();
		$objTpl->getActiveSheet()->mergeCells('B14:H14');
		$objTpl->getActiveSheet()->mergeCells('B16:H17');
		$objTpl->getActiveSheet()->setCellValue('B14',strip_tags($arr2[0]));
		$objTpl->getActiveSheet()->setCellValue('B16',strip_tags($arr2[1]));
		$objTpl->getActiveSheet()->getStyle('B14')->applyFromArray($redRemarks);
		$objTpl->getActiveSheet()->getStyle('B16')->applyFromArray($redRemarks);
	  	//$objTpl->getActiveSheet()->getStyle('B14')->getAlignment()->setWrapText(true);
	  	//$objTpl->getActiveSheet()->getStyle('B15')->getAlignment()->setWrapText(true);
		//print_r($arr);die();
	  $queryCountAll = "USE ReportingDBProd SELECT TOTAL FROM ReportingDBProd.gsd.TOTAL2MARKET
						WHERE YEAR_MONTH like '$ym'
							AND MARKET LIKE '$region'";
		//die($queryCountAll);		

		$runCount = mssql_query($queryCountAll);

		while($rowTwo = mssql_fetch_array($runCount)){
			$objTpl->getActiveSheet()->setCellValue('B5',$rowTwo['TOTAL']);
		}
		$filename='GSDM_Executive_Dashboard.xls'; 

		$objWriter = PHPExcel_IOFactory::createWriter($objTpl, 'Excel5');
		header('Content-Type: application/vnd.ms-excel; charset: utf-8');
		header('Content-Disposition: attachment;filename="export-market.xls"');
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
	}else{
		include 'markets/getConsolidatedView.php';
		$objTpl = PHPExcel_IOFactory::load("executive_templateAll.xlsx");
		$objTpl->setActiveSheetIndex(0);

		$mustHave = array(" ","  ","TOTAL", "APJ", "EUR", "GCHN", "IND", "LAT", "MEA","NAM");

		$arrBefore=executeConsolidatedData($ym);

		$arr=$arrBefore[0];
		$arrNames=$arrBefore[0];

	//die($sql);
	$month=explode('-',$date);
	$objTpl->getActiveSheet()->setCellValue('B2','Executive Dashboard - '.$region.' (P'.$month[1].')');
		//print_r($arr);die();
	  $i=5;
	  for ($k=0;$k<=count($arr);$k++)
		{
			$j=0;
			if($i<15)
			{
				if($arr[$k]['BUSINESS_LINE'] == '  ')
					$j = 5;
				if($arr[$k]['BUSINESS_LINE'] == 'APJ')
					$j = 7;
				if($arr[$k]['BUSINESS_LINE'] == 'EUR')
					$j = 8;
				if($arr[$k]['BUSINESS_LINE'] == 'GCHN')
					$j = 9;
				if($arr[$k]['BUSINESS_LINE'] == 'IND')
					$j = 10;
				if($arr[$k]['BUSINESS_LINE'] == 'LAT')
					$j = 11;
				if($arr[$k]['BUSINESS_LINE'] == 'MEA')
					$j = 12;
				if($arr[$k]['BUSINESS_LINE'] == 'NAM')
					$j = 13;
				if($arr[$k]['BUSINESS_LINE'] == 'Total')
					$j = 14;
				if($j != 0)
				{
					if($j == 12)
					$objTpl->getActiveSheet()->setCellValue('E5',$arr[$k]['PROJ_32']);
					$objTpl->getActiveSheet()->setCellValue('C'.$j,$arr[$k]['PROJ_EXCL_CARE']);
					$objTpl->getActiveSheet()->setCellValue('D'.$j,$arr[$k]['PTA_SIGNED']);
					$objTpl->getActiveSheet()->setCellValue('E'.$j,$arr[$k]['PROJ_32']);
					$objTpl->getActiveSheet()->setCellValue('F'.$j,$arr[$k]['GDC_100']);
					$objTpl->getActiveSheet()->setCellValue('G'.$j,$arr[$k]['GDC_1_3']);
					$objTpl->getActiveSheet()->setCellValue('H'.$j,$arr[$k]['GDC_3_5']);
					$objTpl->getActiveSheet()->setCellValue('I'.$j,$arr[$k]['GDC_500']);
					$objTpl->getActiveSheet()->setCellValue('J'.$j,$arr[$k]['COST_RED']);
					$objTpl->getActiveSheet()->setCellValue('K'.$j,$arr[$k]['COST_AMBER']);
					$objTpl->getActiveSheet()->setCellValue('L'.$j,$arr[$k]['COST_GREEN']);
					$objTpl->getActiveSheet()->setCellValue('M'.$j,$arr[$k]['TIME_RED']);
					$objTpl->getActiveSheet()->setCellValue('N'.$j,$arr[$k]['TIME_AMBER']);
					$objTpl->getActiveSheet()->setCellValue('O'.$j,$arr[$k]['TIME_GREEN']);
					$objTpl->getActiveSheet()->setCellValue('P'.$j,$arr[$k]['SCOPE_RED']);
					$objTpl->getActiveSheet()->setCellValue('Q'.$j,$arr[$k]['SCOPE_AMBER']);
					$objTpl->getActiveSheet()->setCellValue('R'.$j,$arr[$k]['SCOPE_GREEN']);
					$objTpl->getActiveSheet()->setCellValue('S'.$j,$arr[$k]['QUALITY_RED']);
					$objTpl->getActiveSheet()->setCellValue('T'.$j,$arr[$k]['QUALITY_AMBER']);
					$objTpl->getActiveSheet()->setCellValue('U'.$j,$arr[$k]['QUALITY_GREEN']);
				}
				$i++;
			}

		
		//$nr_supp_ev=$nr_supp_ev+1;
	  }	
	  $redRemarks = array(
		'font'  => array(
	        'bold'  => true,
	        'color' => array('rgb' => 'FF0000'),
	        'size'  => 8
	));
	  $query = "USE ReportingDBProd
			SELECT REMARK from [ReportingDBProd].[gsd].[MARKET_REMARKS]
			WHERE YEAR_MONTH LIKE '$ym'
		";
		//die($query);
	  $rs = mssql_query($query);
	  while ($row=mssql_fetch_array($rs))
		{
			$arr2[] = $row[0]; 
			//print_r($row);die();
		}
		//print_r($arr);die();
		$objTpl->getActiveSheet()->mergeCells('B16:H16');
		$objTpl->getActiveSheet()->mergeCells('B18:H19');
		$objTpl->getActiveSheet()->setCellValue('B16',strip_tags($arr2[0]));
		$objTpl->getActiveSheet()->setCellValue('B16',strip_tags($arr2[1]));
		$objTpl->getActiveSheet()->getStyle('B16')->applyFromArray($redRemarks);
		$objTpl->getActiveSheet()->getStyle('B18')->applyFromArray($redRemarks);
	  	//$objTpl->getActiveSheet()->getStyle('B14')->getAlignment()->setWrapText(true);
	  	//$objTpl->getActiveSheet()->getStyle('B15')->getAlignment()->setWrapText(true);
		//print_r($arr);die();
	  $queryCountAll = "USE ReportingDBProd SELECT SUM(TOTAL) AS TOTAL FROM ReportingDBProd.gsd.TOTAL2MARKET
						WHERE YEAR_MONTH like '$ym'";
		//die($queryCountAll);		

		$runCount = mssql_query($queryCountAll);

		while($rowTwo = mssql_fetch_array($runCount)){
			$objTpl->getActiveSheet()->setCellValue('B5',$rowTwo['TOTAL']);
		}
		$filename='GSDM_Executive_Dashboard.xls'; 
		ob_end_clean();
		$objWriter = PHPExcel_IOFactory::createWriter($objTpl, 'Excel5');
		header('Content-Type: application/vnd.ms-excel; charset: utf-8');
		header('Content-Disposition: attachment;filename="export-market.xls"');
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
	}
}else if ($view == 'Project View'){

	$excel = new PHPExcel();

$green = array(
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '32CD32')
	)
);

$red = array(
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'E62D2D')
	)
);

$yellow = array(
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'FFFF00')
	)
);


	$sql = "
					USE ReportingDBProd
			DECLARE @QUERY1A VARCHAR(4000)
			SET @QUERY1A='
			SET ANSI_NULLS ON 
			SET QUOTED_IDENTIFIER ON
			SET CONCAT_NULL_YIELDS_NULL ON
			SET ANSI_WARNINGS ON
			SET ANSI_PADDING ON
			SELECT IDX AS Nr,
PROJECT_NAME AS [Project Name],
COST_CBL_REVISED AS [CBL /Revised CBL],
RECHARGE_ACTUAL AS [Recharge Actual],
COST_PCA_EAC AS [PCA (EAC)],
--RECHARGE_PLAN AS  [Recharge Plan],
COST_PCA_ACTUAL AS [PCA (Actual)],
CASE WHEN TIME_PROJECT IS NULL OR TIME_PROJECT='''' THEN ''NA'' ELSE CAST(TIME_PROJECT AS VARCHAR(50)) END AS TIME_PROJECT,
TIME_START AS [Start Date],
TIME_END AS [End Date],
TIME_SCHEDULE AS [Schedule Adherence (EAC)],
BUSINESS_LINE AS BLs,SCOPE_CHANGE AS [No.of Change Requests],SCOPE_COMPETENCE AS [Competence Adherence (Against)],SCOPE_REMOTE AS [Remote Service Catalog],[SCOPE_CREEP]  [Scope Creep],
QUALITY_PTA AS [PTA Sign Off],QUALITY_SSD AS [SSD Solution Docu Avbl],QUALITY_VIRTUAL AS [Virtual Zero Delivery Index (n-1)],QUALITY_CUSTOMER AS [Customer Team Survey (CTS)],OVERALL_PROJ_STS AS [Overall Project Status],
WBS_CODE AS [WBS Code],
SWO_CODE AS [SWO Code],
GIC_CODE AS [GIC Code],
NAME_OF_GDM AS [Name of GDM],
NAME_OF_PM AS [Name of PM],
MARKET,
DELIVERY_CENTER AS [Delivery Center],[COST_PCA_ACTUAL-COLOR],[COST_PCA_EAC-COLOR],[TIME_PROJECT-COLOR],
CASE
	WHEN SCOPE_CHANGE=''0'' THEN ''RED'' ELSE ''GREEN''
END 
,[SCOPE_REMOTE-COLOR],[QUALITY_VIRTUAL-COLOR],[QUALITY_CUSTOMER-COLOR],[OVERALL_PROJ_STS-COLOR]
FROM(
			SELECT *, 
			CASE
				WHEN ([COST_PCA_ACTUAL-COLOR]=''RED'' AND SUBSTRING(CAST([ACTUAL_START_DATE] AS VARCHAR),0,8)!=''$ym'') OR [COST_PCA_EAC-COLOR]=''RED'' OR [TIME_PROJECT-COLOR]=''RED''  
					OR [SCOPE_REMOTE-COLOR]=''RED'' OR [QUALITY_VIRTUAL-COLOR]=''RED''  OR QUALITY_SSD=''N'' OR [TIME_SCHEDULE-COLOR]=''RED'' THEN ''RED''
				WHEN ([COST_PCA_ACTUAL-COLOR]=''AMBER'' AND SUBSTRING(CAST([ACTUAL_START_DATE] AS VARCHAR),0,8)!=''$ym'') OR [COST_PCA_EAC-COLOR]=''AMBER'' OR [TIME_PROJECT-COLOR]=''AMBER'' 
				OR [SCOPE_REMOTE-COLOR]=''AMBER'' OR [QUALITY_VIRTUAL-COLOR]=''AMBER'' OR [TIME_SCHEDULE-COLOR]=''AMBER''
					THEN ''AMBER''
				WHEN [COST_PCA_ACTUAL-COLOR]=''GREEN'' OR [COST_PCA_EAC-COLOR]=''GREEN'' OR [TIME_PROJECT-COLOR]=''GREEN'' OR [TIME_SCHEDULE-COLOR]=''GREEN''
					OR [SCOPE_REMOTE-COLOR]=''GREEN'' OR [QUALITY_VIRTUAL-COLOR]=''GREEN'' OR QUALITY_SSD=''Y''
					THEN ''GREEN'' 
			END AS [OVERALL_PROJ_STS-COLOR]
			FROM( '
			DECLARE @QUERY1A1 VARCHAR(4000)
			SET @QUERY1A1='
			SELECT *,'+(SELECT FORMULA FROM [gsd].MAPPING_COLOR_NEW WHERE KPI_NAME='COST_PCA_ACTUAL')+'
			,'+(SELECT FORMULA FROM [gsd].MAPPING_COLOR_NEW WHERE KPI_NAME='COST_PCA_EAC')+'
			,'+(SELECT FORMULA FROM [gsd].MAPPING_COLOR_NEW WHERE KPI_NAME='TIME_PROJECT')+'
			,'+(SELECT FORMULA FROM [gsd].MAPPING_COLOR_NEW WHERE KPI_NAME='SCOPE_CHANGE')+'
			,'+(SELECT FORMULA FROM [gsd].MAPPING_COLOR_NEW WHERE KPI_NAME='SCOPE_REMOTE')+'
			,'+(SELECT FORMULA FROM [gsd].MAPPING_COLOR_NEW WHERE KPI_NAME='QUALITY_VIRTUAL')+'
			,'+(SELECT FORMULA FROM [gsd].MAPPING_COLOR_NEW WHERE KPI_NAME='QUALITY_CUSTOMER')+'
			,'+(SELECT FORMULA FROM [gsd].MAPPING_COLOR_NEW WHERE KPI_NAME='TIME_SCHEDULE')+' '
			DECLARE @QUERY1B VARCHAR(4000)
			SET @QUERY1B='FROM(SELECT [IDX]
		      ,[PROJECT_NAME]
		       , cast(ROUND(CAST(COST_BASELINE AS FLOAT),2) as nvarchar)+ ''/'' + 
						CASE
							 WHEN (REVISED_CBL IS NULL OR REVISED_CBL=''-100'') 
								THEN cast(ISNULL(ROUND(CAST(COST_BASELINE AS FLOAT),2),ROUND(CAST(COST_BASELINE AS FLOAT),2)) as nvarchar)
							 WHEN (REVISED_CBL = 0 OR ISNUMERIC(REVISED_CBL) = 0) 
								THEN cast(ISNULL(ROUND(CAST(COST_BASELINE AS FLOAT),2),ROUND(CAST(COST_BASELINE AS FLOAT),2)) as nvarchar) 
						ELSE 
							CASE 
								WHEN (REVISED_CBL = 0 OR ISNUMERIC(REVISED_CBL) = 0) 
									THEN NULL
								ELSE
									cast(ISNULL(ROUND(CAST(REVISED_CBL AS FLOAT),2),ROUND(CAST(COST_BASELINE AS FLOAT),2)) as nvarchar) \
							END
					END as COST_CBL_REVISED 
		        ,CASE
				WHEN YEAR_MONTH=SUBSTRING(CAST(ACTUAL_START_DATE AS VARCHAR(25)),1,7) 
					AND (TOTAL_COST IS NULL OR REVISED_CBL IS NULL OR REVISED_CBL=0 OR REVISED_CBL=''-100'' OR TOTAL_COST=''-100'')
					THEN ''-100''
				WHEN TOTAL_COST IS NULL AND REVISED_CBL IS NULL THEN NULL
				WHEN TOTAL_COST IS NULL OR REVISED_CBL IS NULL THEN ''-100''
				WHEN REVISED_CBL=0 THEN ''-100''
				WHEN REVISED_CBL=''-100'' OR TOTAL_COST=''-100'' THEN ''-100''
				ELSE ROUND(CAST(TOTAL_COST AS FLOAT)/REVISED_CBL *100,2)
			END AS COST_PCA_ACTUAL
				 ,CASE
				 	WHEN CAST(CAST(DATEPART(YEAR,ACTUAL_START_DATE) AS VARCHAR)+''-''+
						CASE
						WHEN DATEPART(MM,ACTUAL_START_DATE)<10 THEN ''0''+CAST(DATEPART(MM,ACTUAL_START_DATE) AS VARCHAR) 
						ELSE CAST(DATEPART(MM,ACTUAL_START_DATE) AS VARCHAR)
						END AS VARCHAR)>YEAR_MONTH THEN ''-101''
			        WHEN ESTIMATED_COST IS NULL AND REVISED_CBL IS NULL THEN NULL
			        WHEN ESTIMATED_COST IS NULL THEN ''-100''
			        WHEN REVISED_CBL=''-100'' OR REVISED_CBL IS NULL OR REVISED_CBL=0 THEN 
			          CASE
			            WHEN CAST(COST_BASELINE AS FLOAT)=-100 OR COST_BASELINE IS NULL OR CAST(COST_BASELINE AS FLOAT)=0 THEN ''-100''
			            ELSE ROUND(CAST(ESTIMATED_COST AS FLOAT)/COST_BASELINE *100,2)
			          END
			        ELSE ROUND(CAST(ESTIMATED_COST AS FLOAT)/REVISED_CBL *100,2)
			      END AS COST_PCA_EAC	
		      ,[ACTUAL_START_DATE] as TIME_START
		      ,ISNULL([ESTIMATED_COMPLETITION_DATE],[PROJECT_END_DATE]) as TIME_END	   
        ,CASE
        WHEN [ACTUAL_START_DATE] IS NULL AND ESTIMATED_COMPLETITION_DATE IS NULL AND [PROJECT_END_DATE] IS NULL THEN NULL
        WHEN DATEDIFF(DD,[ACTUAL_START_DATE], [PROJECT_END_DATE] )=0 THEN NULL
        ELSE CAST(CAST(CAST(DATEDIFF(DD,[ACTUAL_START_DATE],[ESTIMATED_COMPLETITION_DATE]) AS FLOAT)/DATEDIFF(DD,[ACTUAL_START_DATE], [PROJECT_END_DATE] ) *100 AS DECIMAL(15,2)) AS VARCHAR)
      END AS TIME_SCHEDULE,
      CAST(CASE            
	WHEN BUSINESS_LINE=''MS'' THEN ''100''           ELSE              
	CASE                
		WHEN [ACTUAL_SITE_TILL_P12] IS NULL AND PLANNED_SITE_TILL_P12 IS NULL THEN NULL
		WHEN isnumeric([ACTUAL_SITE_TILL_P12])=0 or isnumeric(PLANNED_SITE_TILL_P12)=0 THEN NULL                 
		WHEN CAST([ACTUAL_SITE_TILL_P12] AS FLOAT)=-100 OR CAST(PLANNED_SITE_TILL_P12 AS FLOAT)=-100 THEN ''-100''               
		WHEN CAST(PLANNED_SITE_TILL_P12 AS FLOAT)=0 THEN ''-100''               
		ELSE CAST(ROUND(CAST(ACTUAL_SITE_TILL_P12 AS FLOAT)/PLANNED_SITE_TILL_P12 *100,2) AS VARCHAR(20))              
	END          
END AS FLOAT) AS TIME_PROJECT_OLD, '
   		  DECLARE @QUERY2 VARCHAR(4000)
			SET @QUERY2='
			CASE            
				WHEN BUSINESS_LINE=''MS'' 
					THEN ROUND((CAST(DATEDIFF (dd , ACTUAL_START_DATE, DATEADD(MONTH, DATEDIFF(MONTH, -1, CAST(YEAR_MONTH+''-01'' AS DATE))-1, -1)) AS FLOAT)/DATEDIFF (dd , ACTUAL_START_DATE, PROJECT_END_DATE))*100,2)
				        ELSE   
				CASE WHEN REVISED_MILESTONE IS NULL THEN
					CASE                
						WHEN [ACTUAL_SITE_TILL_P12] IS NULL AND TOTAL_MILESTONES IS NULL THEN NULL
						WHEN isnumeric([ACTUAL_SITE_TILL_P12])=0 or isnumeric(TOTAL_MILESTONES)=0 THEN NULL                 
						WHEN CAST([ACTUAL_SITE_TILL_P12] AS FLOAT)=-100 OR CAST(TOTAL_MILESTONES AS FLOAT)=-100 THEN ''-100''        
						WHEN CAST(TOTAL_MILESTONES AS FLOAT)=0 THEN ''-100''              
						ELSE CAST(ROUND(CAST(ACTUAL_SITE_TILL_P12 AS FLOAT)/TOTAL_MILESTONES *100,2) AS VARCHAR(20))              
					END     
					ELSE CASE                
						WHEN [ACTUAL_SITE_TILL_P12] IS NULL AND REVISED_MILESTONE IS NULL THEN NULL
						WHEN isnumeric([ACTUAL_SITE_TILL_P12])=0 or isnumeric(REVISED_MILESTONE)=0 THEN NULL                 
						WHEN CAST([ACTUAL_SITE_TILL_P12] AS FLOAT)=-100 OR CAST(REVISED_MILESTONE AS FLOAT)=-100 THEN ''-100''        
						WHEN CAST(REVISED_MILESTONE AS FLOAT)=0 THEN ''-100''              
						ELSE CAST(ROUND(CAST(ACTUAL_SITE_TILL_P12 AS FLOAT)/REVISED_MILESTONE *100,2) AS VARCHAR(20))              
					END   
				END     
			END AS TIME_PROJECT,
			CASE
		      	WHEN YEAR_MONTH=SUBSTRING(CAST(ACTUAL_START_DATE AS VARCHAR(25)),1,7) 
					AND (TOTAL_COST IS NULL OR PLANED_COST IS NULL OR PLANED_COST=0 OR PLANED_COST=''-100'' OR TOTAL_COST=''-100'')
					THEN ''-100''
		        WHEN TOTAL_COST IS NULL AND PLANED_COST IS NULL THEN NULL
		        WHEN TOTAL_COST IS NULL OR PLANED_COST IS NULL THEN ''-100''
		        WHEN PLANED_COST=0 THEN ''-100''
		        WHEN PLANED_COST=''-100'' OR TOTAL_COST=''-100'' THEN ''-100''
		        ELSE ROUND(CAST(TOTAL_COST AS FLOAT)/PLANED_COST *100,2)
		      END AS COST_PCA_ACTUAL_OLD
		      ,[SCOPE_CREEP] 
		      ,[ACTUAL_START_DATE]	
			  ,[BUSINESS_LINE] as BUSINESS_LINE
			  ,[NR_CHANGES_REQUEST_APPROVED] as SCOPE_CHANGE
			  ,[COMPETENCE_ADHERENCE] as SCOPE_COMPETENCE
			  ,CASE
		        WHEN TOTAL_WORK_PACKAGES_HAS_TO_WORK IS NULL AND TOTAL_WORK_PACKAGES_ACTUAL_WORKED IS NULL THEN NULL
		        WHEN TOTAL_WORK_PACKAGES_HAS_TO_WORK IS NULL OR TOTAL_WORK_PACKAGES_ACTUAL_WORKED IS NULL THEN ''-100''
		        WHEN TOTAL_WORK_PACKAGES_HAS_TO_WORK=''-100'' OR TOTAL_WORK_PACKAGES_ACTUAL_WORKED=''-100'' THEN ''-100''
		        WHEN TOTAL_WORK_PACKAGES_ACTUAL_WORKED=0 THEN ''-100''
		        WHEN ( (ISNUMERIC(TOTAL_WORK_PACKAGES_HAS_TO_WORK)=0 or TOTAL_WORK_PACKAGES_HAS_TO_WORK=0) ) THEN ''-100''
		        ELSE ROUND(CAST(TOTAL_WORK_PACKAGES_ACTUAL_WORKED AS FLOAT)/TOTAL_WORK_PACKAGES_HAS_TO_WORK * 100,2)
		      END AS SCOPE_REMOTE
		      ,CASE WHEN [PTA_BASED_ACTUAL_DOCUMENT]=''-100'' THEN ''NA'' ELSE CAST(PTA_BASED_ACTUAL_DOCUMENT AS VARCHAR) END AS QUALITY_PTA
			  ,CASE
			  	WHEN SOLUTION_DOCUMENT=''Y'' THEN ''Y''
			  	ELSE ''N''
			  	END as QUALITY_SSD
			  ,CASE WHEN VIRTUAL_ZERO IS NULL THEN NULL
			  		WHEN VIRTUAL_ZERO LIKE ''-100'' THEN ''-100''
					WHEN VIRTUAL_ZERO =''1'' THEN [VIRTUAL_ZERO] * 100
			  		ELSE [VIRTUAL_ZERO]  
				END as QUALITY_VIRTUAL
			  ,[CUSTOMER_TEAM_SURVEY] as QUALITY_CUSTOMER
			  , '''' as OVERALL_PROJ_STS
			  ,C.[WBS] as  WBS_CODE
			  ,D.SVO as SWO_CODE
		      ,CASE
			  	WHEN [IDX_GIC]=''-100'' THEN ''NA'' 
			  	ELSE IDX_GIC
			  	END as GIC_CODE
			  ,[GDN_NAME] as NAME_OF_GDM
		      ,[PROJECT_MANAGER] as NAME_OF_PM
		      ,[MARKET]
		      ,REVISED_MILESTONE
		      ,TOTAL_COST
		      ,[DELIVERY_CENTER] as DELIVERY_CENTER
		      ,CASE WHEN [TOTAL_COST]=''-100'' THEN ''NA'' ELSE CAST(ROUND(TOTAL_COST,2) AS VARCHAR) END AS RECHARGE_ACTUAL
		      ,CASE WHEN [PLANED_COST]=''-100'' THEN ''NA'' ELSE CAST(ROUND(PLANED_COST,2) AS VARCHAR) END AS RECHARGE_PLAN
		      ,YEAR_MONTH
		      '
		    DECLARE @QUERY3 VARCHAR(4000)
			SET @QUERY3='
		  	FROM ReportingDBProd.[gsd].[RAW_DATA] A
		    LEFT JOIN (SELECT DISTINCT r.IDX_COUNTRY_CUSTOMER,
			 STUFF((SELECT distinct '',''+ a.COUNTRY
		               FROM [ReportingDBProd].[gsd].[MAPPING_COUNTRY] a
		             WHERE r.IDX_COUNTRY_CUSTOMER = a.IDX_COUNTRY_CUSTOMER
		            FOR XML PATH(''''), TYPE).value(''.'',''VARCHAR(max)''), 1, 1, '''')  AS [COUNTRY_OF_CUSTOMER]
				FROM ReportingDBProd.[gsd].[MAPPING_COUNTRY] r) B 
				ON A.IDX=B.IDX_COUNTRY_CUSTOMER
			LEFT JOIN (SELECT DISTINCT r.IDX_WBS,
			 STUFF((SELECT distinct '',''+ a.WBS
		               FROM [ReportingDBProd].[gsd].[MAPPING_WBS] a
		             WHERE r.IDX_WBS = a.IDX_WBS
		            FOR XML PATH(''''), TYPE).value(''.'',''VARCHAR(max)''), 1, 1, '''')  AS [WBS]
				FROM ReportingDBProd.[gsd].[MAPPING_WBS] r) C
				ON A.IDX=C.IDX_WBS
			LEFT JOIN (SELECT DISTINCT r.IDX_SVO,
			 STUFF((SELECT distinct '',''+ a.SVO
		               FROM [ReportingDBProd].[gsd].[MAPPING_SVO] a
		             WHERE r.IDX_SVO = a.IDX_SVO
		            FOR XML PATH(''''), TYPE).value(''.'',''VARCHAR(max)''), 1, 1, '''')  AS [SVO]
				FROM ReportingDBProd.[gsd].[MAPPING_SVO] r) D
				ON A.IDX=D.IDX_SVO
			WHERE YEAR_MONTH=''$ym'' AND MARKET LIKE ''$region'' $stringBusiness $stringProject
			) AS FINAL
			) AS FINAL ) AS FINAL
			--where IDX=9958
			ORDER BY IDX '
			EXECUTE (@QUERY1A+@QUERY1A1+@QUERY1B+@QUERY2+@QUERY3)
			";
$row = array(
'Nr',
'Project Name',
"On Cost",
"On Cost",
"On Cost",
"On Cost",
"On Schedule",
"On Schedule",
"On Schedule",
"On Schedule",
"On Scope",
"On Scope",
"On Scope",
"On Scope",
"On Scope",
"On Quality",
"On Quality",
"On Quality",
"On Quality",
"Overall Project Status",
"WBS Code",
"SvO Code",
"GIC Code",
"Name of GDM",
"Name of PM",
"MARKET",
"Delivery Center"

);
// die($sql);
$arr[] = $row;
$row = array(
'Nr',
'Project Name',
"CBL / \nRevised CBL \n (K/K)",
"Recharge \nCum(n-1)",
"PCA \n(EAC) \n (%)",
"CBL \nConsume\nCum(n-1) } \n (%)",
"Project \nVolume\nComp(n-1) } \n (%)",
"Start Date \n (yyyy-mm-dd)",
"End Date \n (yyyy-mm-dd)",
"Schedule \nAdherence \n(EAC) \n (%)",
"BLs",
"No. of \nChange \nRequests \n Approved\n (No)",
"Competence \nAdherence \n (Against SSD or acceptable documents) \n (Against) \n (Y/N)",
"Remote \nService \nCatalog \n Compliance \n (%)",
"Scope \nCreep",
"PTA \nSign Off \n (Y/N)",
"SSD or OLA \n Avbl \n (Y/N)",
"Virtual Zero \nDelivery Index \n(n)",
"Customer \nTeam \nSurvey (CTS)",
"Overall Project Status \n (RAG)",
"WBS Code",
"SvO Code",
"GIC Code",
"Name of GDM",
"Name of PM",
"MARKET",
"Delivery Center"

);
//die($sql);
$arr[] = $row;
$data = mssql_query($sql);

	while ($row = mssql_fetch_assoc($data)) {
		
	    foreach ($row as $key => $value) {
	        //$row[$key] = utf8_encode($value);			
				 $row[$key] = utf8_encode($value);
			
	    }
	    $arr[] = $row;

	}


$doc = new PHPExcel();
$doc->setActiveSheetIndex(0);

// $rows = $doc->getActiveSheet()->fromArray(array_keys($arr[0]),NULL,'A2');

$doc->getActiveSheet()->fromArray($arr, null, 'A1');

foreach(range('A','AR') as $columnID) {

    $doc->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);

}



  $doc->getActiveSheet()->mergeCells('A1:A2');
  $doc->getActiveSheet()->mergeCells('B1:B2');
  $doc->getActiveSheet()->mergeCells('C1:F1');
  $doc->getActiveSheet()->mergeCells('G1:J1');
  $doc->getActiveSheet()->mergeCells('K1:O1');
  $doc->getActiveSheet()->mergeCells('P1:S1');
  $doc->getActiveSheet()->mergeCells('U1:U2');
  $doc->getActiveSheet()->mergeCells('T1:T2');
  $doc->getActiveSheet()->mergeCells('V1:V2');
  $doc->getActiveSheet()->mergeCells('W1:W2');
  $doc->getActiveSheet()->mergeCells('X1:X2');
  $doc->getActiveSheet()->mergeCells('Y1:Y2');
  $doc->getActiveSheet()->mergeCells('Z1:Z2');
  $doc->getActiveSheet()->mergeCells('AA1:AA2');
  $doc->getActiveSheet()->mergeCells('AB1:AB2');

  $doc->getActiveSheet()->getColumnDimension('A')->setWidth(10);
  $doc->getActiveSheet()->getColumnDimension('B')->setWidth(40);
  $doc->getActiveSheet()->getColumnDimension('C')->setWidth(15);
  $doc->getActiveSheet()->getColumnDimension('D')->setWidth(15);
  $doc->getActiveSheet()->getColumnDimension('E')->setWidth(15);
  $doc->getActiveSheet()->getColumnDimension('F')->setWidth(15);
  $doc->getActiveSheet()->getColumnDimension('G')->setWidth(15);
  $doc->getActiveSheet()->getColumnDimension('H')->setWidth(15);
  $doc->getActiveSheet()->getColumnDimension('I')->setWidth(15);
  $doc->getActiveSheet()->getColumnDimension('J')->setWidth(15);
  $doc->getActiveSheet()->getColumnDimension('K')->setWidth(15);
  $doc->getActiveSheet()->getColumnDimension('L')->setWidth(15);
  $doc->getActiveSheet()->getColumnDimension('M')->setWidth(15);
  $doc->getActiveSheet()->getColumnDimension('N')->setWidth(15);
  $doc->getActiveSheet()->getColumnDimension('O')->setWidth(15);
  $doc->getActiveSheet()->getColumnDimension('P')->setWidth(15);
  $doc->getActiveSheet()->getColumnDimension('Q')->setWidth(15);
  $doc->getActiveSheet()->getColumnDimension('R')->setWidth(15);
  $doc->getActiveSheet()->getColumnDimension('S')->setWidth(15);
  $doc->getActiveSheet()->getColumnDimension('T')->setWidth(15);
  $doc->getActiveSheet()->getColumnDimension('U')->setWidth(30);
  $doc->getActiveSheet()->getColumnDimension('V')->setWidth(20);
  $doc->getActiveSheet()->getColumnDimension('W')->setWidth(20);
  $doc->getActiveSheet()->getColumnDimension('X')->setWidth(20);
  $doc->getActiveSheet()->getColumnDimension('Y')->setWidth(20);
  $doc->getActiveSheet()->getColumnDimension('Z')->setWidth(20);
  $doc->getActiveSheet()->getColumnDimension('AA')->setWidth(20);
  $doc->getActiveSheet()->getColumnDimension('AB')->setWidth(20);

$style = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    )
);

$styleAllBorders = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);

$doc->getDefaultStyle()->applyFromArray($style);
$doc->getActiveSheet()->getStyle('1:1')->getFont()->setBold(true);


  $doc->getActiveSheet()->getStyle('A2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('B2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('C2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('D2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('E2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('F2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('G2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('H2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('I2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('J2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('K2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('L2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('M2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('N2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('O2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('P2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('Q2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('R2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('S2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('T2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('U2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('V2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('W2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('X2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('Y2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('Z2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('AA2')->getAlignment()->setWrapText(true);
  $doc->getActiveSheet()->getStyle('AB2')->getAlignment()->setWrapText(true);


$doc->getActiveSheet()->getStyle('A1:AB2')->applyFromArray($styleAllBorders);

function cellColor($cells,$color){
    global $doc;

    $doc->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => $color
        )
    ));
}

$nr = 3;
while($doc->getActiveSheet()->getCell('B'.$nr)->getValue())
{
	$cblRcbl = explode('/',$doc->getActiveSheet()->getCell('C'.$nr)->getValue());
	// print_r($cblRcbl);
	// die();
	if($cblRcbl[0]<=$cblRcbl[1])
		cellColor('C'.$nr, '37ff37');
	else
		cellColor('C'.$nr, 'ff6161');
	
	if($doc->getActiveSheet()->getCell('D'.$nr)->getValue() == '-100'){
		cellColor('D'.$nr, 'ff6161');
		$doc->getActiveSheet()->setCellValue('D'.$nr,'NA');
	}else{
		cellColor('D'.$nr, '37ff37');
	}
	// else if($doc->getActiveSheet()->getCell('AC'.$nr)->getValue() == '-101')
	// 	$doc->getActiveSheet()->setCellValue('D'.$nr,'NA');
	// else if($doc->getActiveSheet()->getCell('AC'.$nr)->getValue() == 'GREEN'){
	// 	cellColor('D'.$nr, '37ff37');
	// 	//$doc->getActiveSheet()->setCellValue('D'.$nr,$doc->getActiveSheet()->getCell('D'.$nr)->getValue().'%');
	// }
	// else if($doc->getActiveSheet()->getCell('AC'.$nr)->getValue() == 'AMBER'){
	// 	cellColor('D'.$nr, 'ffbf00');
	// 	//$doc->getActiveSheet()->setCellValue('D'.$nr,$doc->getActiveSheet()->getCell('D'.$nr)->getValue().'%');
	// }
	// else if($doc->getActiveSheet()->getCell('AC'.$nr)->getValue() == 'RED'){
	// 	cellColor('D'.$nr, 'ff6161');
	// 	//$doc->getActiveSheet()->setCellValue('D'.$nr,$doc->getActiveSheet()->getCell('D'.$nr)->getValue().'%');
	// 	if($doc->getActiveSheet()->getCell('D'.$nr)->getValue()==''){
	// 		$doc->getActiveSheet()->setCellValue('D'.$nr,$doc->getActiveSheet()->getCell('D'.$nr)->getValue());
	// 	}else{
	// 		//$doc->getActiveSheet()->setCellValue('D'.$nr,$doc->getActiveSheet()->getCell('D'.$nr)->getValue().'%');
	// 	}
	// }
	// else {
	// 	cellColor('D'.$nr, 'ff6161');
	// 	if($doc->getActiveSheet()->getCell('D'.$nr)->getValue()==''){
	// 		$doc->getActiveSheet()->setCellValue('D'.$nr,$doc->getActiveSheet()->getCell('D'.$nr)->getValue());
	// 	}else{
	// 		//$doc->getActiveSheet()->setCellValue('D'.$nr,$doc->getActiveSheet()->getCell('D'.$nr)->getValue().'%');
	// 	}
	// }

	if($doc->getActiveSheet()->getCell('E'.$nr)->getValue() == '-100' || $doc->getActiveSheet()->getCell('E'.$nr)->getValue() == '-101'){
		cellColor('E'.$nr, 'ff6161');
		$doc->getActiveSheet()->setCellValue('E'.$nr,'NA');
	}
	else if($doc->getActiveSheet()->getCell('E'.$nr)->getValue() >=90 && $doc->getActiveSheet()->getCell('E'.$nr)->getValue() <=100){
		cellColor('E'.$nr, '37ff37');
		$doc->getActiveSheet()->setCellValue('E'.$nr,$doc->getActiveSheet()->getCell('E'.$nr)->getValue().'%');
	}
	else if($doc->getActiveSheet()->getCell('E'.$nr)->getValue() >=80 && $doc->getActiveSheet()->getCell('E'.$nr)->getValue() <90){
		cellColor('E'.$nr, 'ffbf00');
		$doc->getActiveSheet()->setCellValue('E'.$nr,$doc->getActiveSheet()->getCell('E'.$nr)->getValue().'%');
	}
	else{ 
		cellColor('E'.$nr, 'ff6161');
		$doc->getActiveSheet()->setCellValue('E'.$nr,$doc->getActiveSheet()->getCell('E'.$nr)->getValue().'%');
	}

	if($doc->getActiveSheet()->getCell('F'.$nr)->getValue() == '-100'){
		$doc->getActiveSheet()->setCellValue('F'.$nr,'0');
		cellColor('F'.$nr, 'ff6161');
	}
	else if($doc->getActiveSheet()->getCell('AB'.$nr)->getValue() == 'GREEN')
		cellColor('F'.$nr, '37ff37');
	else if($doc->getActiveSheet()->getCell('AB'.$nr)->getValue() == 'AMBER')
		cellColor('F'.$nr, 'ffbf00');
	else if($doc->getActiveSheet()->getCell('AB'.$nr)->getValue() == 'RED')
		cellColor('F'.$nr, 'ff6161');

	if($doc->getActiveSheet()->getCell('G'.$nr)->getValue() == '-100'){
		$doc->getActiveSheet()->setCellValue('G'.$nr,'0');
		cellColor('G'.$nr, 'ff6161');
	}
	else if($doc->getActiveSheet()->getCell('AD'.$nr)->getValue() == 'GREEN')
		cellColor('G'.$nr, '37ff37');
	else if($doc->getActiveSheet()->getCell('AD'.$nr)->getValue() == 'AMBER')
		cellColor('G'.$nr, 'ffbf00');
	else if($doc->getActiveSheet()->getCell('AD'.$nr)->getValue() == 'RED')
		cellColor('G'.$nr, 'ff6161');

	if($doc->getActiveSheet()->getCell('H'.$nr)->getValue() || $doc->getActiveSheet()->getCell('H'.$nr)->getValue() != '')
		cellColor('H'.$nr, '37ff37');
	else
		cellColor('H'.$nr, 'ff6161');

	if($doc->getActiveSheet()->getCell('I'.$nr)->getValue() || $doc->getActiveSheet()->getCell('I'.$nr)->getValue() != '')
		cellColor('I'.$nr, '37ff37');
	else
		cellColor('I'.$nr, 'ff6161');

	if($doc->getActiveSheet()->getCell('J'.$nr)->getValue()<=110 && $doc->getActiveSheet()->getCell('J'.$nr)->getValue()>=90){
		cellColor('J'.$nr, '37ff37');
		$doc->getActiveSheet()->setCellValue('J'.$nr,$doc->getActiveSheet()->getCell('J'.$nr)->getValue().'%');
	}
	else if( ($doc->getActiveSheet()->getCell('J'.$nr)->getValue()<90 && $doc->getActiveSheet()->getCell('J'.$nr)->getValue()>=80) || ($doc->getActiveSheet()->getCell('J'.$nr)->getValue()<121 && $doc->getActiveSheet()->getCell('J'.$nr)->getValue()>110)){
		cellColor('J'.$nr, 'ffbf00');
		$doc->getActiveSheet()->setCellValue('J'.$nr,$doc->getActiveSheet()->getCell('J'.$nr)->getValue().'%');
	}
	else if($doc->getActiveSheet()->getCell('J'.$nr)->getValue()>=121 || $doc->getActiveSheet()->getCell('J'.$nr)->getValue()<80){
		cellColor('J'.$nr, 'ff6161');
		$doc->getActiveSheet()->setCellValue('J'.$nr,$doc->getActiveSheet()->getCell('J'.$nr)->getValue().'%');
	}
	/*else
		cellColor('J'.$nr, 'ff6161');
*/
	if($doc->getActiveSheet()->getCell('L'.$nr)->getValue() == '-100'){
		//cellColor('L'.$nr, 'ff6161');
		$doc->getActiveSheet()->setCellValue('L'.$nr,'NA');
		cellColor('L'.$nr, 'ff6161');
	}
	else if($doc->getActiveSheet()->getCell('AE'.$nr)->getValue() == 'GREEN'){
		cellColor('L'.$nr, '37ff37');
		$doc->getActiveSheet()->setCellValue('L'.$nr,$doc->getActiveSheet()->getCell('L'.$nr)->getValue());
	}
	else if($doc->getActiveSheet()->getCell('AE'.$nr)->getValue() == 'AMBER'){
		cellColor('L'.$nr, 'ffbf00');
		$doc->getActiveSheet()->setCellValue('L'.$nr,$doc->getActiveSheet()->getCell('L'.$nr)->getValue());
	}
	else if($doc->getActiveSheet()->getCell('AE'.$nr)->getValue() == 'RED'){
		cellColor('L'.$nr, 'ff6161');
		$doc->getActiveSheet()->setCellValue('L'.$nr,$doc->getActiveSheet()->getCell('L'.$nr)->getValue());
	}
	else cellColor('L'.$nr, 'ff6161');


	// if($doc->getActiveSheet()->getCell('M'.$nr)->getValue() == '-100'){
	// 	$doc->getActiveSheet()->setCellValue('M'.$nr,'0');
	// 	cellColor('M'.$nr, 'ff6161');
	// }
	// else if($doc->getActiveSheet()->getCell('AF'.$nr)->getValue() == 'GREEN')
	// 	cellColor('M'.$nr, '37ff37');
	// else if($doc->getActiveSheet()->getCell('AF'.$nr)->getValue() == 'AMBER')
	// 	cellColor('M'.$nr, 'ffbf00');
	// else if($doc->getActiveSheet()->getCell('AF'.$nr)->getValue() == 'RED')
	// 	cellColor('M'.$nr, 'ff6161');
	// else cellColor('M'.$nr, 'ff6161');

	if($doc->getActiveSheet()->getCell('M'.$nr)->getValue() == 'Y')
		cellColor('M'.$nr, '37ff37');
	else if($doc->getActiveSheet()->getCell('M'.$nr)->getValue() == 'N')
		cellColor('M'.$nr, 'ff6161');
	else if($doc->getActiveSheet()->getCell('M'.$nr)->getValue() == '-100'){
		cellColor('M'.$nr, 'ff6161');
		$doc->getActiveSheet()->setCellValue('M'.$nr,'NA');
	}
	else cellColor('M'.$nr, 'ff6161');

	if($doc->getActiveSheet()->getCell('N'.$nr)->getValue() == '-100' || $doc->getActiveSheet()->getCell('N'.$nr)->getValue() == '-101'){
		cellColor('N'.$nr, 'ff6161');
		$doc->getActiveSheet()->setCellValue('N'.$nr,'NA');
	}
	else if($doc->getActiveSheet()->getCell('N'.$nr)->getValue() >=90 && $doc->getActiveSheet()->getCell('N'.$nr)->getValue() <=100){
		cellColor('N'.$nr, '37ff37');
		$doc->getActiveSheet()->setCellValue('N'.$nr,$doc->getActiveSheet()->getCell('N'.$nr)->getValue().'%');
	}
	else if($doc->getActiveSheet()->getCell('N'.$nr)->getValue() >=80 && $doc->getActiveSheet()->getCell('N'.$nr)->getValue() <90){
		cellColor('N'.$nr, 'ffbf00');
		$doc->getActiveSheet()->setCellValue('N'.$nr,$doc->getActiveSheet()->getCell('N'.$nr)->getValue().'%');
	}
	else if($doc->getActiveSheet()->getCell('N'.$nr)->getValue() <80 || $doc->getActiveSheet()->getCell('N'.$nr)->getValue() >121){ 
		cellColor('N'.$nr, 'ff6161');
		$doc->getActiveSheet()->setCellValue('N'.$nr,$doc->getActiveSheet()->getCell('N'.$nr)->getValue().'%');
	}else{
		$doc->getActiveSheet()->setCellValue('N'.$nr,$doc->getActiveSheet()->getCell('N'.$nr)->getValue().'%');
	}


	if($doc->getActiveSheet()->getCell('P'.$nr)->getValue() == 'Y')
		cellColor('P'.$nr, '37ff37');
	else if($doc->getActiveSheet()->getCell('P'.$nr)->getValue() == 'N')
		cellColor('P'.$nr, 'ff6161');
	else if($doc->getActiveSheet()->getCell('P'.$nr)->getValue() == '-100'){
		cellColor('P'.$nr, 'ff6161');
		$doc->getActiveSheet()->setCellValue('P'.$nr,'NA');
	}
	else cellColor('P'.$nr, 'ff6161');

	if($doc->getActiveSheet()->getCell('Q'.$nr)->getValue() == 'Y')
		cellColor('Q'.$nr, '37ff37');
	else if($doc->getActiveSheet()->getCell('Q'.$nr)->getValue() == 'N')
		cellColor('Q'.$nr, 'ff6161');
	else if($doc->getActiveSheet()->getCell('Q'.$nr)->getValue() == 'NA')
		cellColor('Q'.$nr, 'ff6161');

	if($doc->getActiveSheet()->getCell('R'.$nr)->getValue() == '-100')
		$doc->getActiveSheet()->setCellValue('R'.$nr,'NA');
		//cellColor('R'.$nr, '37ff37');
	else if($doc->getActiveSheet()->getCell('R'.$nr)->getValue() == '100'){
		cellColor('R'.$nr, '37ff37');
		$doc->getActiveSheet()->setCellValue('R'.$nr,$doc->getActiveSheet()->getCell('R'.$nr)->getValue().'%');
	}
	else if($doc->getActiveSheet()->getCell('R'.$nr)->getValue() == '0'){
		cellColor('R'.$nr, 'ff6161');
		$doc->getActiveSheet()->setCellValue('R'.$nr,$doc->getActiveSheet()->getCell('R'.$nr)->getValue().'%');
	}

	// if($doc->getActiveSheet()->getCell('R'.$nr)->getValue() == 'NA' || !$doc->getActiveSheet()->getCell('R'.$nr)->getValue())
	// 	if($doc->getActiveSheet()->getCell('R'.$nr)->getValue()==0){
	// 		cellColor('R'.$nr, 'ff6161');
	// 	}else{
	// 		$doc->getActiveSheet()->setCellValue('S'.$nr,'NA');
	// 	}
	// else if($doc->getActiveSheet()->getCell('AH'.$nr)->getValue() == 'GREEN')
	// 	cellColor('S'.$nr, '37ff37');
	// else if($doc->getActiveSheet()->getCell('AH'.$nr)->getValue() == 'AMBER')
	// 	cellColor('S'.$nr, 'ffbf00');
	// else if($doc->getActiveSheet()->getCell('AH'.$nr)->getValue() == 'RED')
	// 	cellColor('S'.$nr, 'ff6161');
	// else $doc->getActiveSheet()->setCellValue('S'.$nr,'NA');
	// //else cellColor('S'.$nr, 'ff6161');
if($doc->getActiveSheet()->getCell('S'.$nr)->getValue() == '-100' || $doc->getActiveSheet()->getCell('S'.$nr)->getValue() == '-101'){
		//cellColor('S'.$nr, 'ff6161');
		$doc->getActiveSheet()->setCellValue('S'.$nr,'NA');
	}
	else if($doc->getActiveSheet()->getCell('S'.$nr)->getValue() >=8.2){
		cellColor('S'.$nr, '37ff37');
	}
	else if($doc->getActiveSheet()->getCell('S'.$nr)->getValue() >=7.38 && $doc->getActiveSheet()->getCell('S'.$nr)->getValue() <8.2){
		cellColor('S'.$nr, 'ffbf00');
	}
	else if($doc->getActiveSheet()->getCell('S'.$nr)->getValue() <7.38){ 
		cellColor('S'.$nr, 'ff6161');
	}

	if($doc->getActiveSheet()->getCell('T'.$nr)->getValue() == '-100'){
		$doc->getActiveSheet()->setCellValue('T'.$nr,'NA');
		//cellColor('O'.$nr, 'ff6161');
	}
	else if($doc->getActiveSheet()->getCell('AI'.$nr)->getValue() == 'GREEN')
		cellColor('T'.$nr, '37ff37');
	else if($doc->getActiveSheet()->getCell('AI'.$nr)->getValue() == 'AMBER')
		cellColor('T'.$nr, 'ffbf00');
	else if($doc->getActiveSheet()->getCell('AI'.$nr)->getValue() == 'RED')
		cellColor('T'.$nr, 'ff6161');
	else cellColor('T'.$nr, 'ff6161');

	$doc->getActiveSheet()->setCellValue('AB'.$nr,'');
	$doc->getActiveSheet()->setCellValue('AC'.$nr,'');
	$doc->getActiveSheet()->setCellValue('AD'.$nr,'');
	$doc->getActiveSheet()->setCellValue('AE'.$nr,'');
	$doc->getActiveSheet()->setCellValue('AF'.$nr,'');
	$doc->getActiveSheet()->setCellValue('AG'.$nr,'');
	$doc->getActiveSheet()->setCellValue('AH'.$nr,'');
	$doc->getActiveSheet()->setCellValue('AI'.$nr,'');
	$doc->getActiveSheet()->setCellValue('AJ'.$nr,'');
	$nr++;
}

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Export Project Data.xls"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$writer = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

$writer->save('php://output');
	//include '../../_libs/php/Utilities/_exportSQL.php';
	////die($sql);
	exportSQL($sql, 'Export Project Dashboard', 'Sheet1');
	
}else if($view == 'Raw Data View'){
	include 'markets/getRawDataMarketFunction.php';

	$row = array('#',
			'Country Of Customer',
			'Customer Name',
			'Project Name',
			'Cost Baseline Value in K Euro',
			'Revised CBL',
			'Estimated Cost at Completion E',
			'Total Cost (K Euro) charged Actual from F&C report base on WBS/Svo/GIC]',
			'Planned Cost (Till (n-1) End)',
			//'PCA % (Actual)- Formula',
			'CBL Consume Cum(n-1)',
			'PCA% (EAC) - Formula',
			'Project Planned Start Date (Month)',
			'Project Actual Start Date (Month)',
			'Project Planned End Date (Month)',
			'Estimated Project Completion Date',
			//'Project Planned End Date (Duration in days)',
			//'Estimated Project Completion Date (Duration in days)',
			'Schedule Adherence -EAC (%)- Formula',
			'Planned Sites till P'.((int)$month-1).'.(NI/NPO)/Planned Milestones till P'.((int)$month-1).' (SI)',
			'Actual Sites till P'.((int)$month-1).' (NI/NPO)/Actual Milestones till P'.((int)$month-1).' (SI)',
			'Total Milestones',
			'Revised Milestones',
			'Project Completion -Actual %- Formula',
			'Business Line (excel Care)',
			'No. of Changes Requests Aproved',
			'Competence Adherence (Against SSD) ',
			'Total Work Packages on which GDC has to work',
			'Work Packages on which GDC has actually worked',
			'Remote Service Catalog Compliance Formula',
			'Scope Creep',
			//'PTA Sign-off (based on BC approval & SOW/WLA agreement)',
			'PTA Sign-off (actual Document in PMO Onboarding)',
			'Solution Document Available (Y/N)',
			'Virtual Zero Delivery Index (n-1)',
			'Customer Team Survey(CTS)',
			'WBS/Project Code as per fNok FTE report or Lawson tool',
			'Service Order (SvO) Number as per FTE report or Lawson for fAlu',
			'GIC Code',
			'GDM Name',
			'Project Manager',
			'Delivery Center',
			'Business Case Available Y/N',
			'Project Duration Baseline in M',
			'PMO',
			'Remarks',
			'TOP 32 PROJECTS',
			'Former ALU/NOKIA',
			'MARKET',
			'YEAR MONTH'
	);
	//die($sql);

	$arr=getRawDataMerketFunction($ym,$region,'','','export');
function cellColor($cells,$color){
    global $doc;

    $doc->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => $color
        )
    ));
}
$doc = new PHPExcel();
$doc->setActiveSheetIndex(0);
for ($i=0; $i < count($arr); $i++) { 
	foreach ($arr[$i] as $key => $value) {
		if($value=='-100'){
			$value='NA';
		}else if($value!='NA' && $value!='' && ($key=='COST_PCA_ACTUAL' || $key=='COST_PCA_EAC' || $key=='TIME_SCHEDULE' || $key=='TIME_PROJECT' || $key=='SCOPE_REMOTE') ){
			$value=$value.' %';
		}
		$arr[$i][$key]=$value;
	}
}
//die();
/*print_r($arr);
die();*/
for ($i=0; $i < count($arr); $i++) { 
	# code...
	$arr2[$i]['COST_BASELINE-COLOR']=$arr[$i]['COST_BASELINE-COLOR'];
	unset($arr[$i]['COST_BASELINE-COLOR']);
	$arr2[$i]['REVISED_CBL-COLOR']=$arr[$i]['REVISED_CBL-COLOR'];
	unset($arr[$i]['REVISED_CBL-COLOR']);
	$arr2[$i]['ESTIMATED_COST-COLOR']=$arr[$i]['ESTIMATED_COST-COLOR'];
	unset($arr[$i]['ESTIMATED_COST-COLOR']);
	$arr2[$i]['PROJECT_START_DATE-COLOR']=$arr[$i]['PROJECT_START_DATE-COLOR'];
	unset($arr[$i]['PROJECT_START_DATE-COLOR']);
	$arr2[$i]['ACTUAL_START_DATE-COLOR']=$arr[$i]['ACTUAL_START_DATE-COLOR'];
	unset($arr[$i]['ACTUAL_START_DATE-COLOR']);
	$arr2[$i]['PROJECT_END_DATE-COLOR']=$arr[$i]['PROJECT_END_DATE-COLOR'];
	unset($arr[$i]['PROJECT_END_DATE-COLOR']);
	$arr2[$i]['ESTIMATED_COMPLETITION_DATE-CO']=$arr[$i]['ESTIMATED_COMPLETITION_DATE-CO'];
	unset($arr[$i]['ESTIMATED_COMPLETITION_DATE-CO']);
	$arr2[$i]['TIME_PROJECT-COLOR']=$arr[$i]['TIME_PROJECT-COLOR'];
	unset($arr[$i]['TIME_PROJECT-COLOR']);
	$arr2[$i]['COST_PCA_ACTUAL-COLOR']=$arr[$i]['COST_PCA_ACTUAL-COLOR'];
	unset($arr[$i]['COST_PCA_ACTUAL-COLOR']);
	$arr2[$i]['COST_PCA_EAC-COLOR']=$arr[$i]['COST_PCA_EAC-COLOR'];
	unset($arr[$i]['COST_PCA_EAC-COLOR']);
	$arr2[$i]['SCOPE_REMOTE-COLOR']=$arr[$i]['SCOPE_REMOTE-COLOR'];
	unset($arr[$i]['SCOPE_REMOTE-COLOR']);
	$arr2[$i]['TIME_SCHEDULE-COLOR']=$arr[$i]['TIME_SCHEDULE-COLOR'];
	unset($arr[$i]['TIME_SCHEDULE-COLOR']);

}
// print_r($arr);
// die();
// $rows = $doc->getActiveSheet()->fromArray(array_keys($arr[0]),NULL,'A2');
$doc->getActiveSheet()->fromArray($row, null, 'A1');
$doc->getActiveSheet()->fromArray($arr, null, 'A2');



for ($i=0; $i < count($arr2); $i++) { 
	if ($arr2[$i]['COST_BASELINE-COLOR']=='GREEN'){
		cellColor('E'.($i+2), '37ff37');
	}else if ($arr2[$i]['COST_BASELINE-COLOR']=='AMBER'){
		cellColor('E'.($i+2), 'ffbf00');
	}else if ($arr2[$i]['COST_BASELINE-COLOR']=='RED'){
		cellColor('E'.($i+2), 'ff6161');
	}
	if ($arr2[$i]['REVISED_CBL-COLOR']=='GREEN'){
		cellColor('F'.($i+2), '37ff37');
	}else if ($arr2[$i]['REVISED_CBL-COLOR']=='AMBER'){
		cellColor('F'.($i+2), 'ffbf00');
	}else if ($arr2[$i]['REVISED_CBL-COLOR']=='RED'){
		cellColor('F'.($i+2), 'ff6161');
	}
	if ($arr2[$i]['ESTIMATED_COST-COLOR']=='GREEN'){
		cellColor('G'.($i+2), '37ff37');
	}else if ($arr2[$i]['ESTIMATED_COST-COLOR']=='AMBER'){
		cellColor('G'.($i+2), 'ffbf00');
	}else if ($arr2[$i]['ESTIMATED_COST-COLOR']=='RED'){
		cellColor('G'.($i+2), 'ff6161');
	}
	if ($arr2[$i]['COST_PCA_ACTUAL-COLOR']=='GREEN'){
		cellColor('J'.($i+2), '37ff37');
	}else if ($arr2[$i]['COST_PCA_ACTUAL-COLOR']=='AMBER'){
		cellColor('J'.($i+2), 'ffbf00');
	}else if ($arr2[$i]['COST_PCA_ACTUAL-COLOR']=='RED'){
		cellColor('J'.($i+2), 'ff6161');
	}
	if ($arr2[$i]['COST_PCA_EAC-COLOR']=='GREEN'){
		cellColor('K'.($i+2), '37ff37');
	}else if ($arr2[$i]['COST_PCA_EAC-COLOR']=='AMBER'){
		cellColor('K'.($i+2), 'ffbf00');
	}else if ($arr2[$i]['COST_PCA_EAC-COLOR']=='RED'){
		cellColor('K'.($i+2), 'ff6161');
	}
	if ($arr2[$i]['PROJECT_START_DATE-COLOR']=='GREEN'){
		cellColor('L'.($i+2), '37ff37');
	}else if ($arr2[$i]['PROJECT_START_DATE-COLOR']=='AMBER'){
		cellColor('L'.($i+2), 'ffbf00');
	}else if ($arr2[$i]['PROJECT_START_DATE-COLOR']=='RED'){
		cellColor('L'.($i+2), 'ff6161');
	}
	if ($arr2[$i]['ACTUAL_START_DATE-COLOR']=='GREEN'){
		cellColor('M'.($i+2), '37ff37');
	}else if ($arr2[$i]['ACTUAL_START_DATE-COLOR']=='AMBER'){
		cellColor('M'.($i+2), 'ffbf00');
	}else if ($arr2[$i]['ACTUAL_START_DATE-COLOR']=='RED'){
		cellColor('M'.($i+2), 'ff6161');
	}
	if ($arr2[$i]['PROJECT_END_DATE-COLOR']=='GREEN'){
		cellColor('N'.($i+2), '37ff37');
	}else if ($arr2[$i]['PROJECT_END_DATE-COLOR']=='AMBER'){
		cellColor('N'.($i+2), 'ffbf00');
	}else if ($arr2[$i]['PROJECT_END_DATE-COLOR']=='RED'){
		cellColor('N'.($i+2), 'ff6161');
	}
	if ($arr2[$i]['ESTIMATED_COMPLETITION_DATE-CO']=='GREEN'){
		cellColor('O'.($i+2), '37ff37');
	}else if ($arr2[$i]['ESTIMATED_COMPLETITION_DATE-CO']=='AMBER'){
		cellColor('O'.($i+2), 'ffbf00');
	}else if ($arr2[$i]['ESTIMATED_COMPLETITION_DATE-CO']=='RED'){
		cellColor('O'.($i+2), 'ff6161');
	}
	if ($arr2[$i]['TIME_SCHEDULE-COLOR']=='GREEN'){
		cellColor('P'.($i+2), '37ff37');
	}else if ($arr2[$i]['TIME_SCHEDULE-COLOR']=='AMBER'){
		cellColor('P'.($i+2), 'ffbf00');
	}else if ($arr2[$i]['TIME_SCHEDULE-COLOR']=='RED'){
		cellColor('P'.($i+2), 'ff6161');
	}
	if ($arr2[$i]['TIME_PROJECT-COLOR']=='GREEN'){
		cellColor('U'.($i+2), '37ff37');
	}else if ($arr2[$i]['TIME_PROJECT-COLOR']=='AMBER'){
		cellColor('U'.($i+2), 'ffbf00');
	}else if ($arr2[$i]['TIME_PROJECT-COLOR']=='RED'){
		cellColor('U'.($i+2), 'ff6161');
	}
	if ($arr2[$i]['SCOPE_REMOTE-COLOR']=='GREEN'){
		cellColor('AA'.($i+2), '37ff37');
	}else if ($arr2[$i]['SCOPE_REMOTE-COLOR']=='AMBER'){
		cellColor('AA'.($i+2), 'ffbf00');
	}else if ($arr2[$i]['SCOPE_REMOTE-COLOR']=='RED'){
		cellColor('AA'.($i+2), 'ff6161');
	}
	if($doc->getActiveSheet()->getCell('W'.($i+2))->getValue() == '0' || $doc->getActiveSheet()->getCell('W'.($i+2))->getValue() == ''){
		cellColor('W'.($i+2), 'ff6161');
	}else{
		cellColor('W'.($i+2), '37ff37');
	}
	if($doc->getActiveSheet()->getCell('X'.($i+2))->getValue() == 'N' || $doc->getActiveSheet()->getCell('X'.($i+2))->getValue() == ''){
		cellColor('X'.($i+2), 'ff6161');
	}else{
		cellColor('X'.($i+2), '37ff37');
	}
	if($doc->getActiveSheet()->getCell('AC'.($i+2))->getValue() == 'N' || $doc->getActiveSheet()->getCell('AC'.($i+2))->getValue() == ''){
		cellColor('AC'.($i+2), 'ff6161');
	}else{
		cellColor('AC'.($i+2), '37ff37');
	}
	if($doc->getActiveSheet()->getCell('AD'.($i+2))->getValue() == 'N' || $doc->getActiveSheet()->getCell('AD'.($i+2))->getValue() == ''){
		cellColor('AD'.($i+2), 'ff6161');
	}else{
		cellColor('AD'.($i+2), '37ff37');
	}
	if($doc->getActiveSheet()->getCell('AE'.($i+2))->getValue() == '0'){
		cellColor('AE'.($i+2), 'ff6161');
	}else if($doc->getActiveSheet()->getCell('AE'.($i+2))->getValue() != 'NA'){
		cellColor('AE'.($i+2), '37ff37');
	}
	if($doc->getActiveSheet()->getCell('AF'.($i+2))->getValue() != 'NA'){
		cellColor('AF'.($i+2), '37ff37');
	}

}  
foreach(range('A','AR') as $columnID) {

    $doc->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);

}
$style = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    )
);

$doc->getDefaultStyle()->applyFromArray($style);
$doc->getActiveSheet()->getStyle('1:1')->getFont()->setBold(true);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Export Raw Data.xls"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$writer = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

$writer->save('php://output');


}

exit; //done.. exiting!
?>