

$( document ).ready(function() {
	//Loads the interface into the menu view
    loadMenuComponents ();
});

function loadMenuComponents () {
	console.log("Loading Menu Components...");

	//Sets the expand / retract button margin to menus width
	var menuWidth = $("#leftMarketsMenu").width();
	$("#btnMenuCollapse").css("margin-left", menuWidth);

}

//Function that checks and handles the collapse / expand event
function collapseMenu (obj) {
	var menuWidth = $("#leftMarketsMenu").width();

	//75 is the width of the menu
	if (menuWidth > 75) {
		setCollapsedElements ();
	} else {
		setExpandedElements ();
	}
}

//Sets the elements when collapse event triggers
function setCollapsedElements () {
	//Sets the css part of the menu and items for collapsion
	$(".fa-chevron-left").addClass("fa-chevron-right");
	$(".fa-chevron-left").removeClass("fa-chevron-left");
	$("#leftMarketsMenu").css("width", "75px");
	$(".btnMarket").css({"text-align": "center"});
	$(".btnFunction").css({"text-align": "center"});
	$("#btnMenuCollapse").css({"margin-left": "75px", "border-radius":"0px 20px 20px 0", "background-color": "transparent"});

	//Handles the transition and display of elements
	$("#btnMenuCollapse").css({"transition": "margin-left .8s"});
	$("#leftMarketsMenu").css({"transition": "width .6s"});
	$(".btnMarket").css({"display": "block"});
	$(".btnFunction").css({"display": "block"});

	setCollapsedElementsText();
}

//Function that clears the elements text for the additional text components of the menu elements 
function setCollapsedElementsText () {
	//Markets additional text
	$("#ALL").text("");
	$("#MEA").text("");
	$("#EUR").text("");
	$("#IND").text("");
	$("#GCHN").text("");
	$("#APJ").text("");
	$("#LAT").text("");
	$("#NAM").text("");

	//Functions additional text
	$("#addUpdate").text("");
	$("#upload").text("");
	$("#customerManagemenet").text("");
	$("#reportManager").text("");
}

//Sets the elements when expand event triggers
function setExpandedElements () {
	//Sets the css part of the menu and items for expansion
	$(".fa-chevron-right").addClass("fa-chevron-left");
	$(".fa-chevron-right").removeClass("fa-chevron-right");
	$("#leftMarketsMenu").css("width", "200px");
	$(".btnMarket").css({"text-align": "left"});
	$(".btnFunction").css({"text-align": "left"});
	$("#btnMenuCollapse").css({"margin-left": "180px", "border-radius":"20px 0 0 20px", "background-color": "white"});

	//Handles the transition and display of elements
	$("#btnMenuCollapse").css({"transition": "none"});
	$("#leftMarketsMenu").css({"transition": "none"});
	$(".btnMarket").css({"display": "flex"});
	$(".btnFunction").css({"display": "flex"});

	//Sets the additional text for the menu elements 
	setExpandedElementsText ();
}

//Function that sets the elements text for the additional text components of the menu elements 
function setExpandedElementsText () {
	//Markets additional text
	$("#ALL").text("Consolidated View");
	$("#MEA").text("Middle East and Africa");
	$("#EUR").text("Europe");
	$("#IND").text("India");
	$("#GCHN").text("Great China");
	$("#APJ").text("Asia Pacific Japan");
	$("#LAT").text("Latin America");
	$("#NAM").text("North America");

	//Functions additional text
	$("#addUpdate").text("Add or Update Projects");
	$("#upload").text("Upload Projects");
	$("#customerManagemenet").text("User Management");
	$("#reportManager").text("Report Manager");
}

function changeMarket(market) {
	var clickedMarket = $(market).ignore("span").text();
	// console.log($(market));
	if (selectedMarket == clickedMarket) {
		return;
	}

	switch (clickedMarket) {
		case "ALL": selectedMarket = "ALL";break;
		case "MEA": selectedMarket = "MEA";break;
		case "EUR": selectedMarket = "EUR";break;
		case "IND": selectedMarket = "IND";break;
		case "GCHN": selectedMarket = "GCHN";break;
		case "APJ": selectedMarket = "APJ";break;
		case "LAT": selectedMarket = "LAT";break;
		case "NAM": selectedMarket = "NAM";break;
	}

	changeSelectedMarketClass(selectedMarket);
	filterChange ();
	// console.log(selectedMarket);
}

function changeSelectedMarketClass(market) {
	$(".selectedMarket").removeClass("selectedMarket");
	$("#" + market).parent().addClass("selectedMarket");

}

$.fn.ignore = function(sel){
  return this.clone().find(sel||">*").remove().end();
};