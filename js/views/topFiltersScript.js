
$( document ).ready(function() {
	//Sets the logged user text and loads the interface
    loadFilterComponents ();//Function that loads the components into the main view
});

//Sets the components of the Filter View
function loadFilterComponents () {
	console.log("Loading Filters View Components...");

	//Resize at the start of the app, and resize components on window resize event
	checkWidthAndResize ();
}

//Setter for YearMonth
function setYearMonth(ym) {
	selectedYearMonth = $("#cbYearMonth").find(":selected").text();
	// console.log(selectedYearMonth);
	filterChange ();
}

//Setter for GDM Filter
function setGDM(gdm) {
	selectedGDM = $("#cbGDM").find(":selected").text();
	// console.log(selectedGDM);
	filterChange ();
}

//Setter for Data Type
//UNUSED
function setDataType (dataType) {

	switch ($(dataType).attr('id')) {
		case "projectView": selectedDataType = "project";break;
		case "executiveView": selectedDataType = "executive";break;
		case "rawDataView": selectedDataType = "rawdata";break;
	}

	//Changes the design of the selected data type
	$(".selectedDataType").removeClass("selectedDataType");
	$("#" + selectedDataType + "View").addClass("selectedDataType");

	filterChange ();
}
