
//Function that is called every component change event
function filterChange () {
	// getSelectedFilters ();
	setTitleText ();
	loadData();
}

function checkWidthAndResize () {
	var pageWidth = $( document ).width();
	// console.log(pageWidth);

	//Changes the text of the data type radio buttons at certains widths
	if (pageWidth < 900) {
		$("#projectView").text("Proj");
		$("#executiveView").text("Exec");
		$("#rawDataView").text("Raw");
	} else {
		$("#projectView").text("Project");
		$("#executiveView").text("Executive");
		$("#rawDataView").text("Raw Data");
	}

	if (pageWidth < 700) {
		$("#synopsisText").text("PLG");
	} else {
		$("#synopsisText").text("Project Level Governance");
	}
}

function loadData() {
	$.ajax({
	  	url: "php/getRawData.php",
	  	data: {yearMonth: selectedYearMonth},
	  	dataType: 'json' //Used to return data as objects not as strings
	}).done(function(response) {
	  	// console.log("Raw Data: %s", JSON.stringify(response));
	  	data = response;
	  	console.log(response);
  		// setOncallElements (user);
	});
}