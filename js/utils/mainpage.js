//Variables to store globaly the filter values
var selectedMarket = "ALL";
var selectedDataType = "project";
var selectedYearMonth = "2019-02";
var selectedGDM = "Test Name 1";

$( document ).ready(function() {
	//Sets the logged user text and loads the interface
    $("#loggedUserText").text(loggedUser.email);
    checkWidthAndResize ();
	$( window ).resize(function() {
		checkWidthAndResize ();
	});
    filterChange ();
    loadComponents ();//Function that loads the components into the main view
});

function loadComponents () {
	$("#leftMarketsMenu").load("html/leftMenu.html");
	$("#topFilters").load("html/topFilters.html");
	$("#content").load("html/mainContent.html");
}

//Function that is an alternative to current method of getting the filter values
//Current method sets the selected values on each event of the component
//This function gets them one time collecting data from each component
//CURRENTLY UNUSED
function getSelectedFilters () {
	selectedMarket = $(".selectedMarket").text();
	selectedYearMonth =  $("#cbYearMonth").find(":selected").text();
	selectedGDM = $("#cbGDM").find(":selected").text();
	selectedDataType = $(".selectedDataType").text();
}

function setTitleText () {
	// console.log(selectedDataType);
	// console.log(selectedYearMonth);
	// console.log(selectedGDM);
	// console.log(selectedMarket);

	$("#viewTitle").text(selectedDataType.substr(0,1).toUpperCase() + selectedDataType.substr(1) + " View" + " - " + selectedMarket + " - " + getMonthFromYM(selectedYearMonth));
}

function getMonthFromYM (ym) {
	return "P"+ym.substr(-2);
}