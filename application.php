<?php
	session_start();
	include "../_libs/php/Utilities/General.php";

	$_SESSION["userEmail"] = (isset($_SERVER['REMOTE_USER']) == 1) ? $_SERVER['REMOTE_USER'] : 'urechiatu.raul.ext@nokia.com';

	checkUser2Group( (isset($_SERVER['REMOTE_USER']) == 1) ? $_SERVER['REMOTE_USER'] : 'local'
		,(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"
		,'GSD_GOV'
		,$conn
		,0
		,'app.php'
		,"http://localhost/gsd_gov/application.php");

?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<!-- BOOTSTRAP & Jquery LIBS -->

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


	<!-- GET THE LOGGED USER -->

		<script>
			var loggedUser = {};
			loggedUser['email'] = <?php echo "'" . $_SESSION["userEmail"] . "'"?>;
			var name = loggedUser['email'].substr(0, loggedUser['email'].indexOf("@")).split('.').join(" ");
			loggedUser['name'] = name.charAt(0).toUpperCase() + name.substr(1);

			console.log(loggedUser);
		</script>

	<!-- CUSTOM LIBS -->
		<!-- This solves a console error regarding favicon being not found -->
		<link rel="shortcut icon" href="#" />
		<link rel="stylesheet" type="text/css" href='css/style.css'>
		<link rel="stylesheet" type="text/css" href='css/menuStyle.css'>
		<link rel="stylesheet" type="text/css" href='css/topFiltersStyle.css'>


		<title>GSD Governance</title>

		<!-- This removes the EXTJS css overwriting the whole design of the app but still use the css for the elements -->
		<script>
			Ext = {
			        scopeCss: true
			};
		</script>

		<script type='text/javascript' src='js/views/mainpage.js'></script>
		<script type='text/javascript' src='js/utils/globalFunctions.js'></script>
	</head>
	<body>
		<!-- Top title bar section -->
		<div id="topInfoBar">
			<span id="nokiaText">NOKIA</span>
			<span id="synopsisText">Project Level Governance</span>
			<span id="viewTitle"></span>
			
			<div id="loggedUserContainer">
				<span class="welcomeText">Welcome</span> <span id="loggedUserText"></span><span class="welcomeText">!</span>
			</div>
		</div>

		<div id="contentContainer">
			<div id="leftMarketsMenu"></div>
			<div id="topFilters"></div>
			<div id="content"></div>
		</div>

	</body>
</html>
